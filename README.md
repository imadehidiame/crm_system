# crm_system
A simple CRM system built with Laravel and the usual suspects of web development; HTML, CSS, Javascript, Bootstrap. Your company is registered and on registration, links for your Admins, Customer Managers and Customers to log in are sent via email based. Access levels are in a hierarchy of Super Administrators, Administrators, Customer Managers and Customers.
