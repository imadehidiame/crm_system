<?php

namespace App\Listeners;

use App\Events\UserLogin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\DatabaseQueryController;
class UpdateLoginStatus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $data;
    public function __construct(DatabaseQueryController $data)
    {
        $this->data = $data;
    }

    /**
     * Handle the event.
     *
     * @param  UserLogin  $event
     * @return void
     */
    public function handle(UserLogin $event)
    {
        $config = $event->config;

        /**
         *  $config['update_values'] = array('');
                $config['table_name'] = array('sessions');
                $config['where']=array('');
                $config['query_array']=array('');

                $config['insert_values']=array(['user_id','ip_address','logged_in']);
                $config['query_array']=array([$this->req->all()['username'],$_SERVER['REMOTE_ADDR'],1]);
                $config['query_method']=array('insert');
         * 
         */


        
        
        foreach($config as $key => $value):
            if($value['query_method']=="insert"){
                $configg['table_name'] = $value['table_name'];
                $configg['insert_values']=$value['insert_values'];
                $configg['query_array']=$value['query_array'];
                $this->data->insert_data($configg);
            }else{
                $configg['table_name'] = $value['table_name'];
                $configg['where']=$value['where'];
                $configg['query_array']=$value['query_array'];
                $configg['update_values']=$value['update_values'];
                $this->data->update_data($configg);
            }
        endforeach;
       // $this->data->insert_data($configg);
     //$configg=null;
     
    }
}
