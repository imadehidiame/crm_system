<?php

namespace App\Listeners;

use App\Events\UserLog;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\DatabaseQueryController;
class InsertIpAddress

{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $data;
    public function __construct(DatabaseQueryController $data)
    {
        $this->data = $data;
    }

    /**
     * Handle the event.
     *
     * @param  UserLog  $event
     * @return void
     */
    public function handle(UserLog $event)
    {
    $config = $event->config;

/*    foreach($config as $key=> $value):
    $configg['insert_values']=array('username','ip_add');
     $configg['query_array']=array($key,$value);
     $configg['table_name']='test_table';
     $this->data->insert_data($configg);
     //$configg=null;
     endforeach;*/

     foreach($config as $key => $value):
        if($value['query_method']=="insert"){
            $configg['table_name'] = $value['table_name'];
            $configg['insert_values']=$value['insert_values'];
            $configg['query_array']=$value['query_array'];
            $this->data->insert_data($configg);
        }else{
            $configg['table_name'] = $value['table_name'];
            $configg['where']=$value['where'];
            $configg['query_array']=$value['query_array'];
            $configg['update_values'] = $value['update_values'];
            $this->data->update_data($configg);
        }
    endforeach;

    }
}
