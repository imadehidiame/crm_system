<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\UtilityController;
use Illuminate\Http\Request;

class CreateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules(Request $request,UtilityController $utility)
    {
        session(['tab_normal'=>false]);
        //session(['tab_normal'=>true]);
        //$utility->s_flash_empty($request,$request->all());
        $utility->s_flash($request,$request->all());
     $full_name_array=explode(" ",$request->all()['full_name']);   
     if(count($full_name_array)<2){
         $config['full_name_error']="Invalid format for full name. Full name must contain a first name and last name";
        $utility->s_flash($request,$config);
     }
        return [
            'full_name'=>'required',
            'username'=>'required|min:5',
            'email'=>'email'
        ];
    }
    public function messages(){
        return [
            'full_name.required'=>'Enter full name of Customer',
            'email.email'=>'Enter a valid email address',
            'username.min'=>'Username must contain at least 5 characters'
        ];
    }
}
