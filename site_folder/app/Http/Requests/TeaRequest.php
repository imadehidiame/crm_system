<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class TeaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $requests)
    {
        $this->s_flash($requests,$requests->all());
        return [
            'timo'=>'required|email',
            'timo1'=>'required',
            'timo2'=>'required|digits_between:2,10',
            'timo3'=>'required|confirmed',
            'timo3_confirmation'=>'required'
        ];
    } 
    public function messages(){
        return [
            'timo.required'=>'Please fill in the timo field',
            'timo.email'=>'Make sure a valid email address is entered for timo',
            'timo1.required'=>'Enter a value for timo1',
            'timo3.confirmed'=>'Password mismatch'
        ];
    }
    //custom method for flashing post value back to form after redirect in the event of a validation error
    private function s_flash($request,$data){
        $keys=array_keys($data);
        foreach($keys as $key):
            if($key == "_token"){
                continue;
            }
            if(is_array($data[$key])){
                $arr_keys = array_keys($data[$key]);
                foreach($arr_keys as $k):
                    $request->session()->flash(strval($key).strval($k),$data[$key][$k]);        
                endforeach;
            }else{
                $request->session()->flash($key,$data[$key]);
            }
        endforeach;
    }
}
