<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\UtilityController;
class SignUpCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request,UtilityController $utility)
    {
        //var_dump($request->all());
        //return;
        $utility->s_flash($request,$request->all());
        $u = $request->path();
        $url = explode("/",$u);

        $error['err_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
        <div class="err_div">
            <div class="err_details text-center featurette-H font-weight-bold"> Please correct registration errors </div>
        </div>
    </div>';
    //$error['err_div'] = $url[count($url)-1];
        $utility->s_flash($request,$error);
       // $checked['check']=" checked";
       // $ch = $request->all()['acc'];
        //if(!empty($ch)){
          //  $utility->s_flash($request,$checked);
        //}
        return [
            'company'=>'required',
            'registration'=>'required',
            'website'=>'required|url',
            'webmail'=>'required|email',
            'email'=>'required|email|different:webmail',
            'average'=>'required|digits_between:1,100',
            'phone'=>array(
                'required'
            )//,
            //'acc'=>'required|accepted'
        ];
    }
    public function messages(){
        return [
            'company.required'=>'Enter the name of your company',
            'website.required'=>'Enter your company\'s website',
            'website.url'=>'Invalid website address entered',
            'registration'=>"Enter your company's registration number",
            'webmail.required'=>'Enter your web mail address',
            'email.required'=>'Enter an alternate email address we can reach your company with',
            'webmail.email'=>'Enter a valid web mail address',
            'email.email'=>'Enter a valid email address',
            'email.different'=>'Ensure the alternate email supplied is different from your web mail address',
            'average.required'=>'Enter the average number of staffs in your company',
            'average.digits_between'=>'Enter the average number of staffs in numbers',
            'phone.required'=>"Enter your company's head offices' phone number",
            'acc.required'=>"Agree to the terms and conditions stipulated by NUTSHELL",
            'registration.required'=>"Enter your company's registration number"
        ];
    }
    
}
