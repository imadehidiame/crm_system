<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\UtilityController;
use Illuminate\Http\Request;


class CustomerManagerUpdateLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(UtilityController $utility, Request $request)
    {
        $utility->s_flash($request,$request->all());
        return [
            'passwordd'=>'required|min:5',
            'confirm_passwordd'=>'same:passwordd'
        ];
    }
    public function messages(){
        return [
            'passwordd.required'=>'Please enter your new password',
            'confirm_passwordd.same'=>'Do ensure this value matches with the password field'
        ];
    }
    
}
