<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\UtilityController;
use Illuminate\Http\Request;


class CustomerManagerInsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request,UtilityController $utility)
    {

        $utility->s_flash($request,$request->all());
        if(!$utility->validate_number_patterns($request->all()['phone_number'],'Nigeria')){
            $config['phone_number_error']='Invalid mobile phone number';
            $utility->s_flash($request,$config);
            $config = array();
        }
        


        /*$error['erro_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="err_div">
            <div class="err_details text-center featurette-H font-weight-bold"> Please ensure all errors are corrected </div>
        </div>
    </div>';*/
    //$error['err_div'] = $url[count($url)-1];
   // $utility->s_flash($request,$error);
    
   return[
    'country'=>'required',
    'address'=>'required',
    'phone_number'=>'required',
    'state_of_residence'=>'required'
    ];

    }
     
    public function messages(){
        return[
            'state_of_residence.required'=>'Please enter your current state of residence'
        ];
    }
}