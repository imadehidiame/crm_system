<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;


class UpdateMeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(UtilityController $utility,Request $request,DatabaseQueryController $database)
    {
        $check_customer_manager = $utility->pull_data($database,[session($request->all()['ret_ul'])['data']->url_extension,session($request->all()['ret_ul'])['data']->username,'Customer Manager'],['url_extension','username','role'],'company_users',1);
        if($check_customer_manager){
            $check_meeting = $utility->pull_data($database,[session($request->all()['ret_ul'])['data']->url_extension,$request->all()['meeting_id']],['url_extension','meeting_id'],'meetings',1);
            if($check_meeting)
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     * 
     * @return array
     */
    public function rules(Request $request,UtilityController $utility)
    {
        $utility->s_flash($request,$request->all());
        if($request->all()['date']){
        if(time()>strtotime($request->all()['date'])){
            $config['date_error']="Invalid date. A valid date would be at least 24hours from today";
            $utility->s_flash($request,$config);  
        }
    }
        return [
            
        ];
    }
}
