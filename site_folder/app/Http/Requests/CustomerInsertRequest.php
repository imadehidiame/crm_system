<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\UtilityController;
use App\Http\Controllers\DatabaseQueryController;
use Illuminate\Http\Request;


class CustomerInsertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request,UtilityController $utility,DatabaseQueryController $database)
    {
        if($request->all()['ret_ul']){
            $check_user = $utility->pull_data($database,[session($request->all()['ret_ul'])['data']->username,session($request->all()['ret_ul'])['data']->url_extension,'Customer'],['username','url_extension','role'],'company_users',1);
            if($check_user)
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request,UtilityController $utility)
    {

        $utility->s_flash($request,$request->all());
        if(!$utility->validate_number_patterns($request->all()['phone_number'],'Nigeria')){
            $config['phone_number_error']='Invalid mobile phone number';
            $utility->s_flash($request,$config);
            $config = array();
        }
     
    
   return[
    'country'=>'required',
    'address'=>'required',
    'phone_number'=>'required',
    'state_of_residence'=>'required'
    ];

    }
     
    public function messages(){
        return[
            'state_of_residence.required'=>'Please enter your current state of residence'
        ];
    }
}
