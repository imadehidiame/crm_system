<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\UtilityController;
use App\Http\Controllers\DatabaseQueryController;
use Illuminate\Http\Request;


class CustomerUpdateLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request,UtilityController $utility,DatabaseQueryController $database)
    {
        if($request->all()['ret_ul'] && $request->all()['usee']){
            $check_user = $utility->pull_data($database,[session($request->all()['ret_ul'])['data']->username,session($request->all()['ret_ul'])['data']->url_extension,'Customer'],['username','url_extension','role'],'company_users',1);
            if($check_user)
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(UtilityController $utility, Request $request)
    {
        $utility->s_flash($request,$request->all());
        return [
            'passwordd'=>'required|min:5',
            'confirm_passwordd'=>'same:passwordd'
        ];
    }
    public function messages(){
        return [
            'passwordd.required'=>'Please enter your new password',
            'confirm_passwordd.same'=>'Do ensure this value matches with the password field'
        ];
    }

}
