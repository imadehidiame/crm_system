<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\UtilityController;

class LoginCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request,UtilityController $utility)
    {
        $utility->s_flash($request,$request->all());
        return [
            'username'=>'required',
            'password'=>'required'
        ];
    }
    public function messages(){
        return [
        'username.required'=>'Enter your username',
        'password.required'=>'Enter your password'
        ];
    }
}
