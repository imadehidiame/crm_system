<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\UtilityController;
use Illuminate\Http\Request;
class UpdateLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request,UtilityController $utility)
    {
        $utility->s_flash($request,$request->all());

        $error['err_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="err_div">
            <div class="err_details text-center featurette-H font-weight-bold"> Please ensure all errors are corrected </div>
        </div>
    </div>';
    //$error['err_div'] = $url[count($url)-1];
        $utility->s_flash($request,$error);

        return [
            'passwordd'=>'required|min:5',
            'confirm_passwordd'=>'same:passwordd'
        ];
    }
    public function messages(){
        return [
            'passwordd.min'=>'Password must contain at least 5 characters',
            //'passwordd.confirmed'=>'Please re-type your new password correctly',
            'passwordd.required'=>'Please enter the new password',
            'confirm_passwordd.same'=>'Please re-type your new password correctly'
        ];
    }
}
