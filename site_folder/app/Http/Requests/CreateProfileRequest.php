<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\UtilityController;
use Illuminate\Http\Request;

class CreateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public $utility;
    public function __construct(UtilityController $utility){
        $this->utility = $utility;
    }
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $this->utility->s_flash($request,$request->all());
        if(!$this->utility->validate_number_patterns($request->all()['phone_number'],"Nigeria")){
            $config['phone_number_error'] = "Invalid mobile number entered";
            $this->utility->s_flash($request,$config);
        }
        return[
            'country'=>'required|min:3',
            'address'=>'required',
            'phone_number'=>'required',
            'state_of_residence'=>'required'
        ];
    }
    public function messages(){
        return[
            'state_of_residence.required'=>'Please enter your current state of residence'
        ];
    }
}
