<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\UtilityController;
class CustomerMeetingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(Request $request)
    {
        if($request->session()->has($request->all()['ret_ul'])){
            if(session($request->all()['ret_ul'])['data']->role == 'Customer')
           return true; 
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request,UtilityController $utility)
    {
        $utility->s_flash($request,$request->all());
        if(time()>strtotime($request->all()['date'])){
            $config['date_error']="Invalid date. A valid date would be at least 24hours from today";
            $utility->s_flash($request,$config);  
        }
        return [
            'venue'=>'required',
            'date'=>'required'
        ];
    }
    public function messages(){
        return [
            'venue.required'=>'Enter a meeting venue',
            'date.required'=>'Select a meeting date'
        ];
    }
}
