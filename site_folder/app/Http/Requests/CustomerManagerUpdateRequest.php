<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\UtilityController;
use Illuminate\Http\Request;


class CustomerManagerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request,UtilityController $utility)
    {

        $utility->s_flash($request,$request->all());

        /*$error['erro_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="err_div">
            <div class="err_details text-center featurette-H font-weight-bold"> Please ensure all errors are corrected </div>
        </div>
    </div>';*/
    //$error['err_div'] = $url[count($url)-1];
   // $utility->s_flash($request,$error);
   
        $map = array('email'=>'email');
        $returned_array = $this->return_array($request);
        $array = array();
        
        foreach($returned_array as $key=>$value):
            if(array_key_exists($key,$map)){
                $array[$key]=$map[$key];
            }
        endforeach;
        if(!empty($array)){
            return $array;
        }
        return [

        ];
    }
    private function return_array(Request $request){
        $array = array();
        foreach($request->all() as $key=>$value):
            if($key == "_token" || $key == "ret_ul"){
                continue;
            }
            if(!empty($value)){
                $array[$key] = $value;
            }    
        endforeach;
        return $array;
    }
    public function messages(){
        return[
            'state_of_residence.required'=>'Please enter your current state of residence'
        ];
    }
}