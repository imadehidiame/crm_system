<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 

class DatabaseQueryController extends Controller
{
    
    public function generate_select_query($config){
        /**
         * Compulsory config options
          search_column key -->Table columns to search for, accepts both string and array as value, pass the wildcard "*" character for all columns in the table
        table_name key -----> table name to query
         */
        if(is_string($config['search_columns'])){
            return "select {$config['search_columns']} from {$config['table_name']}";
        }
        if(is_array($config['search_columns'])){
            $search_columns=implode(",",$config['search_columns']);
            return "select $search_columns from {$config['table_name']}";
        }
        return !empty($config['table_name'])?"select * from {$config['table_name']}":false;
        
    }
    private function generate_where($config){
        $query = array();
        if(!isset($config['where'])||empty($config['where'])){
            return "";
        }
        if(is_string($config['where'])){
            $delimeter = !empty($config['delimeter'])?$config['delimeter']:",";
            $query = explode($delimeter,$config['where']);
        }else if(is_array($config['where'])){
            $query = $config['where'];
        }else{
            return "";
        }
        $init = " where {$query[0]} = ?";
        for($count = 1; $count<count($query);$count++):
        $init.= " and {$query[$count]} = ?";
        endfor;
        return $init;
    }
    private function generate_order($config){
        if(!isset($config['order'])){
            return ""; 
        }
        $order_sequence = isset($config['order_sequence'])?$config['order_sequence']:"ASC";
        return " order by {$config['order']} $order_sequence";
    }
    private function generate_limit($config){
        if(!isset($config['limit']) || !is_integer($config['limit']) || $config['limit'] < 1){
            return "";
        }
        return " limit {$config['limit']}";
    }
    private function create_update_values($config){
        //config[update values] --> assoc array of key table column and value column value config[where] table columns
        //config[querry array] ---> value in table column
        if(!isset($config['update_values'])|| !isset($config['table_name']) || !is_array($config['update_values'])){
            return false;
        }
        $count = 0;
        $init = "";
        foreach($config['update_values'] as $table_column=>$column_value):
            if($count == 0){
             $col_val = is_string($column_value)? "'$column_value'" : $column_value;   
             $init.="set $table_column = $col_val";   
            }else{
            $col_val = is_string($column_value)? "'$column_value'" : $column_value;
            $init.=", $table_column = $col_val";   
            }
            $count+=1;
        endforeach;
        return "update {$config['table_name']} ".$init.$this->generate_where($config);
        //$init="update {$config['table_name']} set ";
    }
    
    
    private function create_insert_values($config){
        $insert = array(); 
        if(empty($config['insert_values'])){
            return false;
        }
        if(is_string($config['insert_values'])){
            $delimeter = !empty($config['delimeter'])?$config['delimeter']:",";
            $insert = explode($delimeter,$config['insert_values']);
        }else if(is_array($config['insert_values'])){
            $insert = $config['insert_values'];
        }else{
            return false;
        }

    
        $init_unknown = "(?";
        $init = " ({$insert[0]}";

        for($c=1;$c<count($insert);$c++):
            $init.=", {$insert[$c]}";
            $init_unknown.=", ?";    
        endfor;

        $init.=")";
        $init_unknown.=")";

        return $init." values ".$init_unknown;
    }
    public function delete_data($config){
        if(!isset($config['table_name'])){
            return false;
        }
        if(!isset($config['delete_value'])||empty($config['delete_value'])){
            return DB::delete("delete from {$config['table_name']}");
        }else{
            //delete value is an associative array with key = table column and value is matching value
            $id_delete = array_keys($config['delete_value'])[0];
            $id_value = $config['delete_value'][$id_delete];
            $sql = "delete from {$config['table_name']} where $id_delete = :$id_delete";
            $q = DB::connection()->getPdo()->prepare($sql);
            return $q->execute([":$id_delete"=>$id_value]);
        }
    }
    public function insert_data($config){
        //config['insert_values']-->table columns config['query_array']--->table values
        if(empty($config['table_name'])){
            return false;
        }
        if(empty($config['query_array'])){
            return false;
        }
        $query_array = array();
        if(is_string($config['query_array'])){
            $query_array = explode(",",$config['query_array']);
        }else if(is_array($config['query_array'])){
            $query_array = $config['query_array'];
        }else{
            return false;
        }
        return DB::insert("insert into {$config['table_name']}".$this->create_insert_values($config),$query_array);
    }
    public function select_data($config){
    //config[table_name],config[where]-->table columns, config[query_array]-->column values
        $query_array = array();
        if(empty($config['query_array'])){
            $query_array = null;
        }else if(is_string($config['query_array'])){
            $query_array = explode(",",$config['query_array']);
        }else if(is_array($config['query_array'])){
            $query_array = $config['query_array'];
        }else{
            $query_array = null;
        }
        if(empty($config['where'])){
            $query_array = null; 
        }

        return empty($query_array)?DB::select($this->generate_select_query($config)):DB::select($this->generate_select_query($config).$this->generate_where($config).$this->generate_order($config).$this->generate_limit($config),$query_array);
    }
    public function update_data($config){
        $query_array = array();
        if(empty($config['query_array']) || !isset($config['query_array'])){
            $query_array = null;
        }else if(is_string($config['query_array'])){
            $query_array = explode(",",$config['query_array']);
        }else if(is_array($config['query_array'])){
            $query_array = $config['query_array'];
        }else{
            $query_array = null;
        }
        if(empty($query_array) && !empty($config['where'])){
            return false;
        }
        if(count($query_array) !== count($config['where'])){
            return false;
        }
        if(!empty($query_array) && empty($config['where'])){
            return DB::update($this->create_update_values($config));
        }
        return empty($query_array)?DB::update($this->create_update_values($config)):DB::update($this->create_update_values($config),$query_array);
    }

}


/**
 *                          UPDATE QUERY
                            * $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
 * 
 * 
 * 
 *                          SELECT QUERY
 *                          $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active','url_extension'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active',session('comp_url')];
            $select = $this->database->select_data($configg);
 * 
 * 
 * 
 *                          INSERT QUERY
 * 
 * 
 * 
 *                      $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
 * 
 */                            


 /**
  * 
  session('super admin login set')
  DELETE QUERY
                //$config['delete_value']
                //$config_delete['delete_value']=['user_id'=>]

  */