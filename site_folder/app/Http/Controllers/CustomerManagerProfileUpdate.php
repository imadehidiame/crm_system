<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Http\Requests\CustomerManagerUpdateRequest;
use App\Http\Requests\CustomerManagerInsertRequest;
use App\Http\Requests\CustomerManagerUpdateLoginRequest;
use App\Events\DatabaseQueryEvent;
class CustomerManagerProfileUpdate extends Controller
{
    public $database;
    public $utility;
    public $request;
    public function __construct(Request $request,DatabaseQueryController $database,UtilityController $utility){
        $this->request = $request;
        $this->database = $database;
        $this->utility = $utility;
    }
    public function update_personal(CustomerManagerUpdateRequest $req){

        $comp_adms = [
            'table_name' => 'company_customer_managers',
            'where'=>['username','url_extension'],
            'query_array'=>[session($this->request->all()['ret_ul'])['data']->username,session($this->request->all()['ret_ul'])['data']->url_extension],
            'query_method'=>'update'
        ];
        $comp_use = [
            'table_name' => 'company_users',
            'where'=>['username','url_extension'],
            'query_array'=>[session($this->request->all()['ret_ul'])['data']->username,session($this->request->all()['ret_ul'])['data']->url_extension],
            'query_method'=>'update'
        ];

        $comp_adms = $this->create_send_array($this->request,$comp_adms);
        $comp_use = $this->create_send_array($this->request,$comp_use); 
       $config = array();
        if($comp_use){
            unset($comp_use['update_values']['phone_number']);
            unset($comp_use['update_values']['address']);
            unset($comp_use['update_values']['country']);
            unset($comp_use['update_values']['state_of_residence']);
            if($comp_use['update_values']){
                array_push($config,$comp_use);
            }
        }

        if($comp_adms){
        if($comp_adms['update_values']){
                unset($comp_adms['update_values']['email']);
                array_push($config,$comp_adms);
            }
        }

        if(!$config){
        $error['erro_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
                <div class="err_div">
                    <div class="err_details text-center featurette-H font-weight-bold"> No submitted values to update!!!  </div>
                </div>
            </div>';
        
            $this->utility->s_flash($this->request,$error);      

        }else{
            event(new DatabaseQueryEvent($config));
            $this->utility->s_flash_empty($this->request,$this->request->all());
            $error['erro_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
            <div class="succ_div">
                <div class="succ_details text-center featurette-H font-weight-bold"> Update successful </div>
            </div>
        </div>';    
        $this->utility->s_flash($this->request,$error);
        $this->utility->s_flash_empty($this->request,$this->request->all());
        }
        $ul = $this->request->all()['ret_ul'];
        return redirect()->route('dash_customer_manager',['rdr'=>$ul]);
    }
    public function insert_personal(CustomerManagerInsertRequest $req){
        $ul = $this->request->all()['ret_ul'];
        if(!$this->utility->validate_number_patterns($this->request->all()['phone_number'],'Nigeria')){
            return redirect()->route('dash_customer_manager',['rdr'=>$ul]);
        }
        $this->utility->s_flash_empty($this->request,$this->request->all());
        $insert_array=$this->pull_all_admins($ul,'A new customer manager with username, "'.session($ul)['data']->username.'" just finished completing the profile update process');
        
        $insert_array[]=[
            'table_name'=>'company_customer_managers',
            'insert_values'=>['username','phone_number','address','country','state_of_residence','url_extension'],
            'query_array'=>[session($ul)['data']->username,$this->request->all()['phone_number'],$this->request->all()['address'],$this->request->all()['country'],$this->request->all()['state_of_residence'],session($ul)['data']->url_extension],
            'query_method'=>'insert'
        ];
        
        event(new DatabaseQueryEvent($insert_array));
        //event(new DatabaseQueryEvent($config));
        $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Profile created successfully </div>
        </div>
        </div>';

    $this->utility->s_flash($this->request,$succ);
    return redirect()->route('dash_customer_manager',['rdr'=>$ul]);
    }

    public function update_login(CustomerManagerUpdateLoginRequest $req){
        //echo "you got here";
        $ul = $this->request->all()['ret_ul'];
        $this->utility->s_flash_empty($this->request,$this->request->all());
        $password = password_hash($this->request->all()['passwordd'],PASSWORD_BCRYPT);
        $config[]=[
            'table_name' => 'company_users',
            'update_values'=>['password'=>$password,'pt_password'=>$this->request->all()['passwordd']],
            'where'=>['username'],
            'query_array'=>[$this->request->all()['usee']],
            'query_method'=>'update'
        ];
        event(new DatabaseQueryEvent($config));
        $succ['err_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Password updated successfully </div>
        </div>
        </div>';

    $this->utility->s_flash($this->request,$succ);
    return redirect()->route('dash_customer_manager',['rdr'=>$ul]);
    }
    private function pull_all_admins($session_url,$message){
        $insert_array = array();
        $configg['table_name']="company_users";
        $configg['where']=['role','active_status','company_active','url_extension'];
        $configg['search_columns']="*";
        //$configg['limit']=1;
        $configg['query_array']= ["Administrator",'Active','Active',session($session_url)['data']->url_extension];
        $select = $this->database->select_data($configg);
        $configg = array();
        if($select){
        foreach($select as $sel):
            if($sel->super_admin_flag == 'true'){
                continue;
            }
            $insert_array[]=[
                'table_name'=>'admin_notifications_normal',
                'insert_values'=>['notification_message','message_time','url_extension','admin'],
                'query_array'=>[$message,time(),session($session_url)['data']->url_extension,$sel->username],
                'query_method'=>'insert'
            ];
        endforeach;
        $insert_array[]=[
            'table_name'=>'admin_notifications',
            'insert_values'=>['notification_message','message_time','url_extension'],
            'query_array'=>[$message,time(),session($session_url)['data']->url_extension],
            'query_method'=>'insert'
        ];
        }
        return $insert_array;
    }
    private function check_post_empty($request){
        $config = array();
        foreach($request->all() as $key=>$value):
            if($key == "_token" || $key == 'ret_ul'){
                continue;
            }
            if(!empty($value)){

            }
        endforeach;
    }
    private function create_send_array(Request $request,$array_keyss){
        $empty_array = [];
        foreach($request->all() as $key=>$value):
            if($key != "_token" && $key !='ret_ul' && !empty($value)){
                $empty_array['update_values'][$key] = $value;
            }
        endforeach;
        if(!empty($empty_array)){
            foreach($array_keyss as $key=>$value):
                $empty_array[$key]=$value;
            endforeach;
        }
        return $empty_array;
    }
    private function add_array($original_array,$additional_array){
        if(!empty($additional_array)){
            $original_array[] = $additional_array;
        }
        return $original_array;
    }

}
