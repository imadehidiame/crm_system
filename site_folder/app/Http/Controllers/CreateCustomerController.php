<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Events\DatabaseQueryEvent;
use App\Http\Requests\CreateCustomerRequest;


class CreateCustomerController extends Controller
{
    public function __construct(UtilityController $utility,Request $request,DatabaseQueryController $database){
        $this->database = $database;
        $this->request = $request;
        $this->utility = $utility;
    }
    public function create_account(CreateCustomerRequest $ad_request){
        $check = true; 
        $ul = $this->request->all()['ret_ul'];
        $comp_name = $this->request->all()['ret_comp_name'];
        $comp_user = $this->request->all()['ret_comp_user'];
        if(!$this->check_user_datas($this->database,[$this->request->all()['username'],session($ul)['data']->url_extension],['username','url_extension'],'company_users')){
            $check = false;
            $conf['username_error']='Username already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$this->check_user_datas($this->database,[$this->request->all()['email'],session($ul)['data']->url_extension],['email','url_extension'],'company_users')){
            $check = false;
            $conf['email_error']='Email already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$this->check_user_datas($this->database,[$this->request->all()['email'],session($ul)['data']->url_extension],['webmail','url_extension'],'company_registration')){
            $check = false;
            $conf['email_error']='Email already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$check){
            $conf['tab_normal']=true;
            $this->utility->s_flash($this->request,$conf);
            session(['tab_normal'=>false]);
            return redirect()->route('customer_manager_customer',['rdr'=>$this->request->all()['ret_ul']]);
        }
        $password = str_random(7);
        $password_hash = password_hash($password,PASSWORD_BCRYPT);

        $configg = array();
        $configg['table_name']="company_users";
        $configg['where']=['role','url_extension','active_status'];
        $configg['search_columns']="*";
        //$configg['limit']=1;
        $configg['query_array']= ['Administrator',session($ul)['data']->url_extension,'Active'];

        $selectt = $this->database->select_data($configg);
        $insert_array = array();
        if($selectt){
            foreach($selectt as $admin):
                if($admin->username == $comp_user){
                    continue;
                }
                $insert_array[]=[
                    'table_name'=>'admin_notifications_normal',
                    'insert_values'=>['notification_message','message_time','url_extension','admin'],
                    'query_array'=>['A customer manager with username, "'.session($ul)['data']->username.'" has just completed the registration of a customer.',time(),session($ul)['data']->url_extension,$admin->username],
                    'query_method'=>'insert'
                ];
            endforeach;
        }


            $configg = array();
            $configg['table_name']="package_bundle";
            $configg['where']=['url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($ul)['data']->url_extension];
            $select = $this->database->select_data($configg);

            if($select){
                foreach($select as $sel):
                    //$config = array();
                    $insert_array[] = [
                        'table_name' => 'company_users',
                    'insert_values'=>['username','password','pt_password','full_name','role','email','package_bundle','url_extension','customer_manager_flag'],
                        'query_array'=>[$this->request->all()['username'],$password_hash,$password,$this->request->all()['full_name'],'Customer',$this->request->all()['email'],session($ul)['data']->package_bundle,session($ul)['data']->url_extension,session($ul)['data']->username],
                        'query_method'=>'insert'
                    ];
                    $insert_array[] = [
                        'table_name' => 'package_bundle',
                        'update_values'=>['customer'=>$sel->admin+1],
                        'where'=>['url_hash'],
                        'query_array'=>[session($ul)['data']->url_extension],
                        'query_method'=>'update'
                    ];
                    $insert_array[] = [
                        'table_name' => 'admin_notifications',
                        'insert_values'=>['notification_message','message_time','url_extension'],
                    'query_array'=>['A customer manager with username, "'.session($ul)['data']->username.'" has just completed the registration of a customer.',time(),session($ul)['data']->url_extension],
                            'query_method'=>'insert'
                    ];
                    
                event(new DatabaseQueryEvent($insert_array));

                endforeach;
            }
            $array_data = ['name'=>$this->request->all()['full_name'],'company_name'=>$comp_name,'company_name_url'=>$comp_user,'url_hash'=>session($ul)['data']->url_extension,'username'=>$this->request->all()['username'],'password'=>$password,'role'=>'a Customer','role_url'=>'customers'];
            $this->utility->send_email($this->request->all()['email'],'em_customer',$array_data);
            $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Customer registration successful </div>
        </div>
    </div>';
    $this->utility->s_flash_empty($this->request,$this->request->all());
    $this->utility->s_flash($this->request,$succ);
    return redirect()->route('customer_manager_customer',['rdr'=>$this->request->all()['ret_ul']]);
    }

    public function create_customer_manager_account(CreateAdminRequest $ad_request){
        //usernane full_name role email package_bundle url_extension
     //   echo "you got here";
        $check = true; 
        $ul = $this->request->all()['ret_ul'];
        $comp_name = $this->request->all()['ret_comp_name'];
        $comp_user = $this->request->all()['ret_comp_user'];
        if(!$this->check_user_datas($this->database,[$this->request->all()['username'],session($ul)['data']->url_extension],['username','url_extension'],'company_users')){
            $check = false;
            $conf['username_error']='Username already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$this->check_user_datas($this->database,[$this->request->all()['email'],session($ul)['data']->url_extension],['email','url_extension'],'company_users')){
            $check = false;
            $conf['email_error']='Email already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$this->check_user_datas($this->database,[$this->request->all()['email'],session($ul)['data']->url_extension],['webmail','url_extension'],'company_registration')){
            $check = false;
            $conf['email_error']='Email already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$check){
            $conf['tab_normal']=true;
            $this->utility->s_flash($this->request,$conf);
            session(['tab_normal'=>false]);
            return redirect()->route('adm',['rdr'=>$this->request->all()['ret_ul']]);
        }
        $password = str_random(7);
        $password_hash = password_hash($password,PASSWORD_BCRYPT);
            $configg = array();
            $configg['table_name']="package_bundle";
            $configg['where']=['url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($ul)['data']->url_extension];
            $select = $this->database->select_data($configg);
            if($select){
                foreach($select as $sel):
                    $config = array();
                    $config = array(
                        [
                    'table_name' => 'company_users',
                'insert_values'=>['username','password','pt_password','full_name','role','email','package_bundle','url_extension'],
                    'query_array'=>[$this->request->all()['username'],$password_hash,$password,$this->request->all()['full_name'],'Customer Manager',$this->request->all()['email'],session($ul)['data']->package_bundle,session($ul)['data']->url_extension],
                    'query_method'=>'insert'
                        ],
                        [
                            'table_name' => 'package_bundle',
                            'update_values'=>['customer_manager'=>$sel->customer_manager+1],
                            'where'=>['url_hash'],
                            'query_array'=>[session($ul)['data']->url_extension],
                            'query_method'=>'update'
                        ]
                        );
                        event(new DatabaseQueryEvent($config));

                endforeach;
            }
        /*$config = array(
            [
        'table_name' => 'company_users',
    'insert_values'=>['username','password','pt_password','full_name','role','email','package_bundle','url_extension'],
        'query_array'=>[$this->request->all()['username'],$password_hash,$password,$this->request->all()['full_name'],'Customer Manager',$this->request->all()['email'],session($ul)['data']->package_bundle,session($ul)['data']->url_extension],
        'query_method'=>'insert'
            ]
            );
            event(new DatabaseQueryEvent($config));*/
            $array_data = ['name'=>$this->request->all()['full_name'],'company_name'=>$comp_name,'company_name_url'=>$comp_user,'url_hash'=>session($ul)['data']->url_extension,'username'=>$this->request->all()['username'],'password'=>$password,'role'=>'a Customer Managerial','role_url'=>'customer_managers'];
            $this->utility->send_email($this->request->all()['email'],'em_reg',$array_data);
            $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Customer Manager registration successful </div>
        </div>
    </div>';
    $this->utility->s_flash_empty($this->request,$this->request->all());
    $this->utility->s_flash($this->request,$succ);
    return redirect()->route('admin_customer_man',['rdr'=>$this->request->all()['ret_ul']]);
    }


    private function check_user_data($data,$check_value,$check_column,$table_name){
        $config['query_array'] = [$check_value];
        $config['search_columns'] = "*";
        $config['where']=[$check_column];
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return false;
        }
        return true;
    }
    private function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return false;
        }
        return true;
    } 
}
