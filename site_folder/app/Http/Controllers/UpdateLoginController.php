<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Events\DatabaseQueryEvent;
use App\Http\Requests\UpdateLoginRequest;
use App\Http\Requests\UpdatePersonalRequest;
use App\Http\Requests\CreateProfileRequest;

class UpdateLoginController extends Controller
{
    public $utility;
    public $request;
    public $database;
    public function __construct(UtilityController $utility,Request $request,DatabaseQueryController $database){
        $this->database = $database;
        $this->request = $request;
        $this->utility = $utility; 
    }
    public function create_profile(CreateProfileRequest $login_request){
        $ul = $this->request->all()['ret_ul'];
        if(!$this->utility->validate_number_patterns($this->request->all()['phone_number'],"Nigeria")){
         return redirect()->route('prof',['rdr'=>$ul]); 
        }
        $this->utility->s_flash_empty($this->request,$this->request->all());
        $config = array(
            [
        'table_name' => 'company_administrators',
    'insert_values'=>['username','phone_number','address','country','state_of_residence','url_extension'],
        'query_array'=>[session($ul)['data']->username,$this->request->all()['phone_number'],$this->request->all()['address'],$this->request->all()['country'],$this->request->all()['state_of_residence'],session($ul)['data']->url_extension],
        'query_method'=>'insert'
            ],
            [
                'table_name' => 'admin_notifications',
            'insert_values'=>['notification_message','message_time','url_extension'],
                'query_array'=>['New admin profile with username, '.session($ul)['data']->username.' has been successfully created',time(),session($ul)['data']->url_extension],
                'query_method'=>'insert'
            ]
            );
            event(new DatabaseQueryEvent($config));
            $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Profile created successfully </div>
        </div>
    </div>';

    $this->utility->s_flash($this->request,$succ);
    return redirect()->route('prof',['rdr'=>$ul]);
    }
    public function update_login(UpdateLoginRequest $login_request){
        //echo "you got here";
        //echo $this->request->all()['usee'];
        $ul = $this->request->all()['ret_ul'];
        if(session($ul)['data']->super_admin_flag == 'true'){

            $password = password_hash($this->request->all()['passwordd'],PASSWORD_BCRYPT);
        $comp_reg = [
            'table_name' => 'company_registration',
            'update_values'=>['admin_password'=>$password,'admin_pt_password'=>$this->request->all()['passwordd']],
            'where'=>['admin_username','url_extension'],
            'query_array'=>[$this->request->all()['usee'],session($ul)['data']->url_extension],
            'query_method'=>'update'
        ];
        $comp_use = [
            'table_name' => 'company_users',
            'update_values'=>['password'=>$password,'pt_password'=>$this->request->all()['passwordd']],
            'where'=>['username','url_extension'],
            'query_array'=>[$this->request->all()['usee'],session($ul)['data']->url_extension],
            'query_method'=>'update'
        ];
        $config[]=$comp_reg;
        $config[] = $comp_use;
       /* $config = array(
            $comp_reg,
            $comp_use
            );*/
        event(new DatabaseQueryEvent($config));
        $error['err_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Password updated successfully </div>
        </div>
    </div>';
        $error['passwordd']='';
        $error['confirm_passwordd']='';
    //$error['err_div'] = $url[count($url)-1];
        $this->utility->s_flash($this->request,$error);
        $ul = $this->request->all()['ret_ul'];
        return redirect()->route('prof',['rdr'=>$ul]);
        }else{

        $password = password_hash($this->request->all()['passwordd'],PASSWORD_BCRYPT);
        $comp_use = [
            'table_name' => 'company_users',
            'update_values'=>['password'=>$password,'pt_password'=>$this->request->all()['passwordd']],
            'where'=>['username','url_extension'],
            'query_array'=>[$this->request->all()['usee'],session($ul)['data']->url_extension],
            'query_method'=>'update'
        ];
        //$config[]=$comp_reg;
        $config[] = $comp_use;
       /* $config = array(
            $comp_reg,
            $comp_use
            );*/
        event(new DatabaseQueryEvent($config));
        $error['err_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Password updated successfully </div>
        </div>
    </div>';
    $error['passwordd']='';
    $error['confirm_passwordd']='';
    //$error['err_div'] = $url[count($url)-1];
        $this->utility->s_flash($this->request,$error);
        $ul = $this->request->all()['ret_ul'];
        return redirect()->route('prof',['rdr'=>$ul]);
    }
        
        
}
    public function update_personal(UpdatePersonalRequest $personal_request){
        //echo "here we go";
        $comp_reg = array();
        $comp_use = array();
        $comp_ad = array();

        $comp_regg = [
            'table_name' => 'company_registration',
            'where'=>['admin_username','url_extension'],
            'query_array'=>[session($this->request->all()['ret_ul'])['data']->username,session($this->request->all()['ret_ul'])['data']->url_extension],
            'query_method'=>'update'
        ];
        $comp_usee = [
            'table_name' => 'company_users',
            'where'=>['username','url_extension'],
            'query_array'=>[session($this->request->all()['ret_ul'])['data']->username,session($this->request->all()['ret_ul'])['data']->url_extension],
            'query_method'=>'update'
        ];

        $comp_adms = [
            'table_name' => 'company_administrators',
            'where'=>['username','url_extension'],
            'query_array'=>[session($this->request->all()['ret_ul'])['data']->username,session($this->request->all()['ret_ul'])['data']->url_extension],
            'query_method'=>'update'
        ];

        if(session($this->request->all()['ret_ul'])['data']->super_admin_flag == "true"){
        $comp_reg = $this->create_send_array($this->request,$comp_regg);
        $comp_use = $this->create_send_array($this->request,$comp_usee);
         
       $config = array();
        if($comp_use){
            unset($comp_use['update_values']['phone_number']);
            unset($comp_use['update_values']['webmail']);
            unset($comp_use['update_values']['number_of_staffs']);
            unset($comp_use['update_values']['registration_number']);
            if($comp_use['update_values']){
                array_push($config,$comp_use);
            }
        }
        if($comp_reg){
           // unset($comp_use['update_values']['phone_number']);
            if($comp_reg['update_values']){
                array_push($config,$comp_reg);
            }
        }
        
        
            if(!$config){
                $error['erro_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
                <div class="err_div">
                    <div class="err_details text-center featurette-H font-weight-bold"> No submitted values to update!!!  </div>
                </div>
            </div>';
            //$error['err_div'] = $url[count($url)-1];
                $this->utility->s_flash($this->request,$error);      
            }else{
                event(new DatabaseQueryEvent($config));
                $this->utility->s_flash_empty($this->request,$this->request->all());
                $error['erro_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
                <div class="succ_div">
                    <div class="succ_details text-center featurette-H font-weight-bold"> Update successful </div>
                </div>
            </div>';    
            $this->utility->s_flash($this->request,$error);
            }
            $ul = $this->request->all()['ret_ul'];
            return redirect()->route('prof',['rdr'=>$ul]);
        }else{
            //if(session('profile_check') == true){
        //$comp_reg = $this->create_send_array($this->request,$comp_regg);
        $comp_use = $this->create_send_array($this->request,$comp_usee);
        $comp_adms = $this->create_send_array($this->request,$comp_adms);
         
       $config = array();
        if($comp_use){
            unset($comp_use['update_values']['phone_number']);
            unset($comp_use['update_values']['webmail']);
            unset($comp_use['update_values']['number_of_staffs']);
            unset($comp_use['update_values']['registration_number']);
            if($comp_use['update_values']){
                array_push($config,$comp_use);
            }
        }

        if($comp_adms){
            //unset($comp_use['update_values']['phone_number']);
            unset($comp_use['update_values']['webmail']);
            unset($comp_use['update_values']['email']);
            unset($comp_use['update_values']['number_of_staffs']);
            unset($comp_use['update_values']['registration_number']);
            if($comp_adms['update_values']){
                array_push($config,$comp_adms);
            }
        }

        if(!$config){
            $error['erro_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
            <div class="err_div">
                <div class="err_details text-center featurette-H font-weight-bold"> No submitted values to update!!!  </div>
            </div>
        </div>';
        //$error['err_div'] = $url[count($url)-1];
            $this->utility->s_flash($this->request,$error);      
        }else{
            event(new DatabaseQueryEvent($config));
            $this->utility->s_flash_empty($this->request,$this->request->all());
            $error['erro_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
            <div class="succ_div">
                <div class="succ_details text-center featurette-H font-weight-bold"> Update successful </div>
            </div>
        </div>';    
        $this->utility->s_flash($this->request,$error);
        }
        $ul = $this->request->all()['ret_ul'];
        return redirect()->route('prof',['rdr'=>$ul]);
}
}
    private function create_send_array(Request $request,$array_keyss){
        $empty_array = [];
        foreach($request->all() as $key=>$value):
            if($key != "_token" && $key !='ret_ul' && !empty($value)){
                $empty_array['update_values'][$key] = $value;
            }
        endforeach;
        if(!empty($empty_array)){
            foreach($array_keyss as $key=>$value):
                $empty_array[$key]=$value;
            endforeach;
        }
        return $empty_array;
    }
    private function add_array($original_array,$additional_array){
        if(!empty($additional_array)){
            $original_array[] = $additional_array;
        }
        return $original_array;
    }
    
}
