<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Events\UserLogin;
use App\Events\DatabaseQueryEvent;
class DashboardController extends Controller
{
    public $req;
    public $database;
    public $utility;
    public function __construct(DatabaseQueryController $database,UtilityController $utility){
        $this->database = $database;
        $this->utility = $utility;
/*        $this->middleware(function(Request $request,$next){
            if($request->session()->has('adm_login')){
                //return $next($request);
                //return view('dashboard');
            }else if($request->session()->has('cust_man_login')){
                //return $next($request);
            }else if($request->session()->has('cust_login')){
               // return $next($request);
            }else{
                return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            return $next($request);
        });*/
    }


    public function load_meetingg(Request $request){


        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];

                            $check_super_notifications = $this->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,'unread'],['url_extension','read_flag'],'admin_notifications',"all","DESC","id"); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                   
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;

                                    endforeach;
                                else:
                                    $profile_adm=false;
                                    endif;

                                   

                                    $adm_meeting = $this->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension],['url_extension'],'meetings',"all"); 

                                    return view('admin_meeting',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'adm_meeting'=>$adm_meeting]);

                        }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                            $configg = null;
                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag','admin'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                $adm_meeting = $this->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension],['url_extension'],'meetings',"all","DESC","id"); 

                                return view('admin_meeting',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'adm_meeting'=>$adm_meeting]);
                                    
                        }
                    }else if(session($_GET['rdr'])['data']->role == "Customer Manager"){

                    }else if(session($_GET['rdr'])['data']->role == "Customer"){


                    }

                        

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }


    }


    public function load_invoicee(Request $request){

        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    

                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;

                                    endforeach;
                                else:
                                    $profile_adm=false;
                                    endif;

                                    $adm_invoice = array();
                                    $conf = array();
                                    $conf['table_name']="invoice";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    $adm_invoice = $this->database->select_data($conf);
                    return view('admin_invoice',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'adm_invoice'=>$adm_invoice]);
                                    }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                            $configg = array();
                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag','admin'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                
                                    $adm_invoice = array();
                                    $conf = array();
                                    $conf['table_name']="invoice";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    $adm_invoice = $this->database->select_data($conf);

                                    return view('admin_invoice',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'adm_invoice'=>$adm_invoice]);

                                    
                        }
                    }else if(session($_GET['rdr'])['data']->role == "Customer Manager"){

                    }else if(session($_GET['rdr'])['data']->role == "Customer"){


                    }

                        

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }


    public function load_messagee(Request $request){


        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    




                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;

                                    endforeach;
                                else:
                                    $profile_adm=false;
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;

                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Administrator'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users=$c_use;
                                    //endforeach;
                                    else:
                                    $company_users=0;
                                    endif;


                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Customer Manager'];
                                    $check_company_users_man = $this->database->select_data($conff);
                                    $c_use_man = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users_man):
                                        $count = 0;
                                    foreach($check_company_users_man as $check):
                                        $c_use_man[$count]['username']=$check->username;
                                        $c_use_man[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use_man[$count]['email']=$check->email;
                                        $c_use_man[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users_man=$c_use_man;
                                    //endforeach;
                                    else:
                                    $company_users_man=0;
                                    endif;




                                    return view('admin_message',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'company_users'=>$company_users,'company_users_man'=>$company_users_man]);

                        }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                                    

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;

                            $configg = array();
                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag','admin'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;



                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conf['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Administrator'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users=$c_use;
                                    //endforeach;
                                    else:
                                    $company_users=0;
                                    endif;


                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Customer Manager'];
                                    $check_company_users_man = $this->database->select_data($conff);
                                    $c_use_man = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users_man):
                                        $count = 0;
                                    foreach($check_company_users_man as $check):
                                        $c_use_man[$count]['username']=$check->username;
                                        $c_use_man[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use_man[$count]['email']=$check->email;
                                        $c_use_man[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users_man=$c_use_man;
                                    //endforeach;
                                    else:
                                    $company_users_man=0;
                                    endif;




                                    return view('admin_message',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'company_users'=>$company_users,'company_users_man'=>$company_users_man]);

                                    
                        }
                    }else if(session($_GET['rdr'])['data']->role == "Customer Manager"){

                    }else if(session($_GET['rdr'])['data']->role == "Customer"){


                    }

                        

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }



    }


    public function load_customer_managerr(Request $request){


        
        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    




                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;

                                    endforeach;
                                else:
                                    $profile_adm=false;
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;

                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Administrator'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    $customers_use = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $c_use[$count]['customers']=$this->pull_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','customer_manager_flag'],'company_users');
                                        $count++;
                                        
                                    endforeach;
                                    $company_users=$c_use;
                                    //endforeach;
                                    else:
                                    $company_users=0;
                                    endif;


                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Customer Manager'];
                                    $check_company_users_man = $this->database->select_data($conff);
                                    $c_use_man = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users_man):
                                        $count = 0;
                                    foreach($check_company_users_man as $check):
                                        $c_use_man[$count]['username']=$check->username;
                                        $c_use_man[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_customer_managers')?"Yes":"No";
                                        $c_use_man[$count]['email']=$check->email;
                                        $c_use_man[$count]['full_name']=$check->full_name;
                                        $c_use_man[$count]['active_status']=$check->active_status;

                                        $c_use_man[$count]['customers']=$this->pull_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','customer_manager_flag'],'company_users');
                                        if($c_use_man[$count]['customers']){
                                        $count_customers = 0;
                                        foreach($c_use_man[$count]['customers'] as $customers):
                                            $customers_use[$count][$count_customers]['full_name']=$customers->full_name;
                                            $customers_use[$count][$count_customers]['username']=$customers->username;
                                            $customers_use[$count][$count_customers]['active_status']=$customers->active_status;
                                            $customers_use[$count][$count_customers]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$customers->username],['url_extension','username'],'company_customers')?"Yes":"No";
                                            $count_customers++;
                                        endforeach;    
                                        }

                                        $count++;
                                        
                                    endforeach;
                                    $company_users_man=$c_use_man;
                                    //endforeach;
                                    else:
                                    $company_users_man=0;
                                    endif;




                                    return view('admin_customer_manager',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'company_users'=>$company_users,'company_users_man'=>$company_users_man,'customers'=>$customers_use]);

                        }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                                    

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;

                            $configg = array();
                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag','admin'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;



                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conf['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Administrator'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    $customers_use = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users=$c_use;
                                    //endforeach;
                                    else:
                                    $company_users=0;
                                    endif;


                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Customer Manager'];
                                    $check_company_users_man = $this->database->select_data($conff);
                                    $c_use_man = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users_man):
                                        $count = 0;
                                    foreach($check_company_users_man as $check):
                                        $c_use_man[$count]['username']=$check->username;
                                        $c_use_man[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_customer_managers')?"Yes":"No";
                                        $c_use_man[$count]['email']=$check->email;
                                        $c_use_man[$count]['full_name']=$check->full_name;
                                        $c_use_man[$count]['active_status']=$check->active_status;

                                        $c_use_man[$count]['customers']=$this->pull_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','customer_manager_flag'],'company_users');
                                        if($c_use_man[$count]['customers']){
                                        $count_customers = 0;
                                        foreach($c_use_man[$count]['customers'] as $customers):
                                            $customers_use[$count][$count_customers]['full_name']=$customers->full_name;
                                            $customers_use[$count][$count_customers]['username']=$customers->username;
                                            $customers_use[$count][$count_customers]['active_status']=$customers->active_status;
                                            $customers_use[$count][$count_customers]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$customers->username],['url_extension','username'],'company_customers')?"Yes":"No";
                                            $count_customers++;
                                        endforeach;    
                                        }

                                        $count++;
                                        
                                    endforeach;
                                    $company_users_man=$c_use_man;
                                    //endforeach;
                                    else:
                                    $company_users_man=0;
                                    endif;




                                    return view('admin_customer_manager',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'company_users'=>$company_users,'company_users_man'=>$company_users_man,'customers'=>$customers_use]);

                                    
                        }
                    }else if(session($_GET['rdr'])['data']->role == "Customer Manager"){

                    }else if(session($_GET['rdr'])['data']->role == "Customer"){


                    }

                        

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }




    }



    public function load_administratorr(Request $request){



        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $check_super_notifications = $this->database->select_data($configg); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    




                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;

                                    endforeach;
                                else:
                                    $profile_adm=false;
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;

                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Administrator'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users=$c_use;
                                    //endforeach;
                                    else:
                                    $company_users=0;
                                    endif;




                                    return view('admin_administrator',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'company_users'=>$company_users]);

                        }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                                    

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;


                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    return view('profile',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration]);


                        }
                    }else if(session($_GET['rdr'])['data']->role == "Customer Manager"){

                    }else if(session($_GET['rdr'])['data']->role == "Customer"){


                    }

                        

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }



    }
    public function load_profilee(Request $request){


        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //delete and re-insert session data and load dash board
                //$config['delete_value']
                //$config_delete['delete_value']=['user_id'=>]
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        $prof_nav_link = " active";
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    




                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;

                                    endforeach;
                                else:
                                    $profile_adm=false;
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;



                                    return view('profile',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'prof_nav_link'=>$prof_nav_link]);

                        }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                                    

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;

                            $configg = array();
                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag','admin'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    return view('profile',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'prof_nav_link'=>$prof_nav_link]);


                        }
                    }else{
                       //no access url mistyped or session fixation
                       $request->session()->forget($_GET['rdr']);
                    $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                       <div class="err_div" style="display:block;">
                       <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                       </div>
                       </div>';
                       $this->utility->s_flash($request,$succ);
                       return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                       

                    }

                        

            }else{
            //no access session data not found in database
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access session has expired
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }




    }
    public function load_dashboardd(Request $request){
        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['order_sequence']='DESC';
                            $configg['order']='id';
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    return view('dashboard',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins]);

                        }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                                    

                            $configg=array();
                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag','admin'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $configg['order']="id";
                            $configg['order_sequence']='DESC';
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    return view('dashboard',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins]);


                        }
                    }else if(session($_GET['rdr'])['data']->role == "Customer Manager"){

                    }else if(session($_GET['rdr'])['data']->role == "Customer"){


                    }

                        

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }
    private function get_uac(){
        $u = get_browser('platform',true);
        
        foreach($_SERVER as $key=>$value):
            echo "key is $key and corresponding value is $value <br>";
        endforeach;

    }
    public function load_dashboard(Request $request){
        ///session('adm_login') is a product of company users  5:40am
            if($request->session()->has('adm_login')){
            //if($request->session()->has('cust_login'))
            //$request->session()->forget('cust_login');
            //if($request->session()->has('cust_man_login'))
            //$request->session()->forget('cust_man_login');  
            $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active','url_extension'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active',session('comp_url')];
            $select = $this->database->select_data($configg);
            if($select){
            foreach($select as $sel):
            $pass_hash = $sel->password;
            if($this->verify_pass(session('adm_login')->pt_password,$sel->password)){
            $conf['table_name']="sessions";
            $conf['where']=['user_id'];
            $conf['search_columns']="*";
            $conf['limit']=1;
            $conf['query_array']= [session('adm_login')->username];
            $check_login = $this->database->select_data($conf);
            if($check_login){
            foreach($check_login as $check):
            //compare time of last activity against 12 hours
            if((time()-$check->last_activity)>43200){
                    //if($check->ip_address==$_SERVER['REMOTE_ADDR'])
                        $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
                            event(new UserLogin($config));

                                    //session([sess_id=>['name'=$name]])
                                    session(['adm_login'=>$sel]);
                                    if($sel->super_admin_flag == "true"){
                                    session(['super_adm_logins'=>true]);  
                                    $configg = array();
                                    $configg['table_name']="admin_notifications";
                                    $configg['where']=['url_extension','read_flag'];
                                    $configg['search_columns']="*";
                                    //$configg['limit']=1;
                                    $configg['query_array']= [session('comp_url'),'unread'];
                                    $check_super_notifications = $this->database->select_data($configg); 
                                    $super_notifications = array();
                                    $count = 0;
                                    if($check_super_notifications){
                                    foreach($check_super_notifications as $check):
                                       $super_notifications[$count]['message'] = $check->notification_message;
                                       $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    endforeach;
                                    session(['super_admin_notice'=>$super_notifications]);    
                                    }else{

                                    }

                                    }else{
                                    session(['super_adm_logins'=>false]);                                           
                                    }


                                    //if()

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;

                                    $conf = array();

                                    $confi['table_name']="admin_notifications";
                                    $confi['where']=['username','read_flag'];
                                    $confi['search_columns']="*";
                                    //$conf['limit']=1;
                                    $confi['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($confi);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach; 
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;

                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;



                //}
                //return redirect()->route('dash');
                return view('dashboard');

                    //else for if 12 hours since last login
                    }else{
                    //check ip address match
                        if($check->ip_address == $_SERVER['REMOTE_ADDR']){
                            //if($sel->)
                            $config = array(
                                [
                            'table_name' => 'sessions',
                            'update_values'=>['last_activity'=>time(),'logged_in'=>1],
                            'where'=>['user_id'],
                            'query_array'=>[session('adm_login')->username],
                            'query_method'=>'update'
                                ]
                                );
                                event(new UserLogin($config));
                                    
                                session(['adm_login'=>$sel]);
                                /*if($role == "Customer Manager"){
                                    session(['cust_man_login'=>$sel]);
                                    
                                    
                                }else if($role == "Customer"){
                                    session(['cust_login'=>$sel]);
                                }*///else if($role == "Administrator"){
                                  //  session(['adm_login'=>$sel]);

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                               // }
                               $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                               
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;


                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;
                               

                                return view('dashboard');
                
                        }else{
                        //do not grant access
                        $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                        <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> This user is already logged in on a different IP Address </div>
                        </div>
                    </div>';
            $this->utility->s_flash($request,$succ);
            $request->session()->forget('adm_login');
            
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                        }
                    }
                endforeach;
                //else for checklogin ie if user data exists in sessions table
            }else{
                //else for checklogin ie if user data exists in sessions table
                $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
                    event(new UserLogin($config));
                    /*if($role == "Customer Manager"){
                        session(['cust_man_login'=>$sel]);
    
                    }else if($role == "Customer"){
                        session(['cust_login'=>$sel]);
                    }/*///else if($role == "Administrator"){
                        session(['adm_login'=>$sel]);
                        

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;



                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                    //}
                    //return redirect()->route('dash');
                    return view('dashboard');

                
            }       
                    //else for password verification failure
                    }else{
                    //else for password verification failure
                    $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                    <div class="err_div" style="display:block;">
                        <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                    </div>
                </div>';
        $this->utility->s_flash($request,$succ);
        $request->session()->forget('adm_login');
        
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                }
                break;
            endforeach;
            //else for if username exists in data base
            }else{
                //else for if username exists in data base
                $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                <div class="err_div" style="display:block;">
                    <div class="err_details text-center featurette-H font-weight-bold"> Your services have been suspended. Contact NUTSHELL for more information </div>
                </div>
            </div>';
    $this->utility->s_flash($request,$succ);
    $request->session()->forget('adm_login');
    
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }


            //return view('dashboard');
            //return $next($request);
            //return view('dashboard');
        }else if($request->session()->has('cust_man_login')){
            //return $next($request);
        }else if($request->session()->has('cust_login')){
           // return $next($request);
        }
        
    }




    public function load_profile(Request $request){
        if($request->session()->has('adm_login')){
            if($request->session()->has('cust_login'))
            $request->session()->forget('cust_login');
            if($request->session()->has('cust_man_login'))
            $request->session()->forget('cust_man_login');  
            //$return_page = $this->return_page($this->req->all()['role']);
            //$role = $this->req->all()['role'];


            $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active'];
            $select = $this->database->select_data($configg);
            if($select){
            foreach($select as $sel):
            $pass_hash = $sel->password;
            if($this->verify_pass(session('adm_login')->pt_password,$sel->password)){
            $conf['table_name']="sessions";
            $conf['where']=['user_id'];
            $conf['search_columns']="*";
            $conf['limit']=1;
            $conf['query_array']= [session('adm_login')->username];
            $check_login = $this->database->select_data($conf);
            if($check_login){
                foreach($check_login as $check):
                    //compare time of last activity against 12 hours
                    if((time()-$check->last_activity)>43200){
                        $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
                            event(new UserLogin($config));

                //session('current_usage',$sel);
                /*if(session('adm_login')->role == "Customer Manager"){
                    session(['cust_man_login'=>$sel]);

                }else if(session('adm_login')->role == "Customer"){
                    session(['cust_login'=>$sel]);
                }*///else if(session('adm_login')->role == "Administrator"){
                                    session(['adm_login'=>$sel]);
                                    

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;

                                    $conf = array();
                                    $conff['table_name']="admin_notifications";
                                    $conff['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= ["$sel->username","'unread'"];
                                    $check_notifications = $this->database->select_data($conff);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;


                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;



                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                //}
                //return redirect()->route('dash');
                return view('profile');


                    }else{
                    //check ip address match
                        if($check->ip_address == $_SERVER['REMOTE_ADDR']){
                            $config = array(
                                [
                            'table_name' => 'sessions',
                            'update_values'=>['last_activity'=>time(),'logged_in'=>1],
                            'where'=>['user_id'],
                            'query_array'=>[session('adm_login')->username],
                            'query_method'=>'update'
                                ]
                                );
                                event(new UserLogin($config));
                                    
                                session(['adm_login'=>$sel]);
                                /*if($role == "Customer Manager"){
                                    session(['cust_man_login'=>$sel]);
                                    
                                    
                                }else if($role == "Customer"){
                                    session(['cust_login'=>$sel]);
                                }*///else if($role == "Administrator"){
                                  //  session(['adm_login'=>$sel]);

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                               // }
                               $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                                    $conf = array();

                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;



                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;
                               


                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;


                                return view('profile');
                
                        }else{
                        //do not grant access
                        $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                        <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> This user is already logged in on a different IP Address </div>
                        </div>
                    </div>';
            $this->utility->s_flash($request,$succ);
            $request->session()->forget('adm_login');
            
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                        }
                    }
                endforeach;
            }else{
                
                $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
                    event(new UserLogin($config));
                    /*if($role == "Customer Manager"){
                        session(['cust_man_login'=>$sel]);
    
                    }else if($role == "Customer"){
                        session(['cust_login'=>$sel]);
                    }/*///else if($role == "Administrator"){
                        session(['adm_login'=>$sel]);
                        

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;




                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;



                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;
                    //}
                    //return redirect()->route('dash');
                    return view('profile');

                
            }
                    }else{
                    $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                    <div class="err_div" style="display:block;">
                        <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                    </div>
                </div>';
        $this->utility->s_flash($request,$succ);
        $request->session()->forget('adm_login');
        
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                }
                break;
            endforeach;
            }else{
       
                $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                <div class="err_div" style="display:block;">
                    <div class="err_details text-center featurette-H font-weight-bold"> Your services have been suspended. Contact NUTSHELL for more information </div>
                </div>
            </div>';
    $this->utility->s_flash($request,$succ);
    $request->session()->forget('adm_login');
    
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }


            //return view('dashboard');
            //return $next($request);
            //return view('dashboard');
        }else if($request->session()->has('cust_man_login')){
            //return $next($request);
        }else if($request->session()->has('cust_login')){
           // return $next($request);
        }
        
    }



    public function load_customer_manager(Request $request){
        if($request->session()->has('adm_login')){
            if($request->session()->has('cust_login'))
            $request->session()->forget('cust_login');
            if($request->session()->has('cust_man_login'))
            $request->session()->forget('cust_man_login');  
            //$return_page = $this->return_page($this->req->all()['role']);
            //$role = $this->req->all()['role'];


            $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active'];
            $select = $this->database->select_data($configg);
            if($select){
            foreach($select as $sel):
            $pass_hash = $sel->password;
            if($this->verify_pass(session('adm_login')->pt_password,$sel->password)){
            $conf['table_name']="sessions";
            $conf['where']=['user_id'];
            $conf['search_columns']="*";
            $conf['limit']=1;
            $conf['query_array']= [session('adm_login')->username];
            $check_login = $this->database->select_data($conf);
            if($check_login){
                foreach($check_login as $check):
                    //compare time of last activity against 12 hours
                    if((time()-$check->last_activity)>43200){
                        $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
                            event(new UserLogin($config));

                //session('current_usage',$sel);
                /*if(session('adm_login')->role == "Customer Manager"){
                    session(['cust_man_login'=>$sel]);

                }else if(session('adm_login')->role == "Customer"){
                    session(['cust_login'=>$sel]);
                }*///else if(session('adm_login')->role == "Administrator"){
                                    session(['adm_login'=>$sel]);
                                    

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;


                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;



                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                //}
                //return redirect()->route('dash');
                return view('admin_customer_manager');


                    }else{
                    //check ip address match
                        if($check->ip_address == $_SERVER['REMOTE_ADDR']){
                            $config = array(
                                [
                            'table_name' => 'sessions',
                            'update_values'=>['last_activity'=>time(),'logged_in'=>1],
                            'where'=>['user_id'],
                            'query_array'=>[session('adm_login')->username],
                            'query_method'=>'update'
                                ]
                                );
                                event(new UserLogin($config));
                                    
                                session(['adm_login'=>$sel]);
                                /*if($role == "Customer Manager"){
                                    session(['cust_man_login'=>$sel]);
                                    
                                    
                                }else if($role == "Customer"){
                                    session(['cust_login'=>$sel]);
                                }*///else if($role == "Administrator"){
                                  //  session(['adm_login'=>$sel]);

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                               // }
                               $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                               
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
//                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;



                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;
                               

                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                                    return view('admin_customer_manager');
                
                        }else{
                        //do not grant access
                        $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                        <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> This user is already logged in on a different IP Address </div>
                        </div>
                    </div>';
            $this->utility->s_flash($request,$succ);
            $request->session()->forget('adm_login');
            
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                        }
                    }
                endforeach;
            }else{
                
                $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
                    event(new UserLogin($config));
                    /*if($role == "Customer Manager"){
                        session(['cust_man_login'=>$sel]);
    
                    }else if($role == "Customer"){
                        session(['cust_login'=>$sel]);
                    }/*///else if($role == "Administrator"){
                        session(['adm_login'=>$sel]);
                        

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                  //  $conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;




                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;




                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                    //}
                    //return redirect()->route('dash');
                    return view('admin_customer_manager');

                
            }
                    }else{
                    $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                    <div class="err_div" style="display:block;">
                        <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                    </div>
                </div>';
        $this->utility->s_flash($request,$succ);
        $request->session()->forget('adm_login');
        
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                }
                break;
            endforeach;
            }else{
       
                $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                <div class="err_div" style="display:block;">
                    <div class="err_details text-center featurette-H font-weight-bold"> Your services have been suspended. Contact NUTSHELL for more information </div>
                </div>
            </div>';
    $this->utility->s_flash($request,$succ);
    $request->session()->forget('adm_login');
    
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }


            //return view('dashboard');
            //return $next($request);
            //return view('dashboard');
        }else if($request->session()->has('cust_man_login')){
            //return $next($request);
        }else if($request->session()->has('cust_login')){
           // return $next($request);
        }
        
    }




    public function load_administrator(Request $request){
        if($request->session()->has('adm_login')){
            if($request->session()->has('cust_login'))
            $request->session()->forget('cust_login');
            if($request->session()->has('cust_man_login'))
            $request->session()->forget('cust_man_login');  
            //$return_page = $this->return_page($this->req->all()['role']);
            //$role = $this->req->all()['role'];

            session(['tab_normal'=>true]);
            

            $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active'];
            $select = $this->database->select_data($configg);
            if($select){
            foreach($select as $sel):
            $pass_hash = $sel->password;
            if($this->verify_pass(session('adm_login')->pt_password,$sel->password)){
            $conf['table_name']="sessions";
            $conf['where']=['user_id'];
            $conf['search_columns']="*";
            $conf['limit']=1;
            $conf['query_array']= [session('adm_login')->username];
            $check_login = $this->database->select_data($conf);
            if($check_login){
                foreach($check_login as $check):
                    //compare time of last activity against 12 hours
                    if((time()-$check->last_activity)>43200){
                        $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
                            event(new UserLogin($config));

                //session('current_usage',$sel);
                /*if(session('adm_login')->role == "Customer Manager"){
                    session(['cust_man_login'=>$sel]);

                }else if(session('adm_login')->role == "Customer"){
                    session(['cust_login'=>$sel]);
                }*///else if(session('adm_login')->role == "Administrator"){
                                    session(['adm_login'=>$sel]);
                                    

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;


                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;




                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_data($this->database,$check->username,'username','company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    session(['company_users'=>$c_use]);
                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                //}
                //return redirect()->route('dash');
                return view('admin_administrator');


                    }else{
                    //check ip address match
                        if($check->ip_address == $_SERVER['REMOTE_ADDR']){
                            $config = array(
                                [
                            'table_name' => 'sessions',
                            'update_values'=>['last_activity'=>time(),'logged_in'=>1],
                            'where'=>['user_id'],
                            'query_array'=>[session('adm_login')->username],
                            'query_method'=>'update'
                                ]
                                );
                                event(new UserLogin($config));
                                    
                                session(['adm_login'=>$sel]);
                                /*if($role == "Customer Manager"){
                                    session(['cust_man_login'=>$sel]);
                                    
                                    
                                }else if($role == "Customer"){
                                    session(['cust_login'=>$sel]);
                                }*///else if($role == "Administrator"){
                                  //  session(['adm_login'=>$sel]);

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                               // }
                               $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                               
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
//                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;



                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;


                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_data($this->database,$check->username,'username','company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    session(['company_users'=>$c_use]);
                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;
                               

                                    return view('admin_administrator');
                
                        }else{
                        //do not grant access
                        $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                        <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> This user is already logged in on a different IP Address </div>
                        </div>
                    </div>';
            $this->utility->s_flash($request,$succ);
            $request->session()->forget('adm_login');
            
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                        }
                    }
                endforeach;
            }else{
                
                $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
                    event(new UserLogin($config));
                    /*if($role == "Customer Manager"){
                        session(['cust_man_login'=>$sel]);
    
                    }else if($role == "Customer"){
                        session(['cust_login'=>$sel]);
                    }/*///else if($role == "Administrator"){
                        session(['adm_login'=>$sel]);
                        

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;




                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;


                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_data($this->database,$check->username,'username','company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    session(['company_users'=>$c_use]);
                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                    //}
                    //return redirect()->route('dash');
                    return view('admin_administrator');

                
            }
                    }else{
                    $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                    <div class="err_div" style="display:block;">
                        <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                    </div>
                </div>';
        $this->utility->s_flash($request,$succ);
        $request->session()->forget('adm_login');
        
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                }
                break;
            endforeach;
            }else{
       
                $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                <div class="err_div" style="display:block;">
                    <div class="err_details text-center featurette-H font-weight-bold"> Your services have been suspended. Contact NUTSHELL for more information </div>
                </div>
            </div>';
    $this->utility->s_flash($request,$succ);
    $request->session()->forget('adm_login');
    
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }


            //return view('dashboard');
            //return $next($request);
            //return view('dashboard');
        }else if($request->session()->has('cust_man_login')){
            //return $next($request);
        }else if($request->session()->has('cust_login')){
           // return $next($request);
        }
        
    }

    public function load_message(Request $request){
        if($request->session()->has('adm_login')){
            if($request->session()->has('cust_login'))
            $request->session()->forget('cust_login');
            if($request->session()->has('cust_man_login'))
            $request->session()->forget('cust_man_login');  
            //$return_page = $this->return_page($this->req->all()['role']);
            //$role = $this->req->all()['role'];


            $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active'];
            $select = $this->database->select_data($configg);
            if($select){
            foreach($select as $sel):
            $pass_hash = $sel->password;
            if($this->verify_pass(session('adm_login')->pt_password,$sel->password)){
            $conf['table_name']="sessions";
            $conf['where']=['user_id'];
            $conf['search_columns']="*";
            $conf['limit']=1;
            $conf['query_array']= [session('adm_login')->username];
            $check_login = $this->database->select_data($conf);
            if($check_login){
                foreach($check_login as $check):
                    //compare time of last activity against 12 hours
                    if((time()-$check->last_activity)>43200){
                        $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
                            event(new UserLogin($config));

                //session('current_usage',$sel);
                /*if(session('adm_login')->role == "Customer Manager"){
                    session(['cust_man_login'=>$sel]);

                }else if(session('adm_login')->role == "Customer"){
                    session(['cust_login'=>$sel]);
                }*///else if(session('adm_login')->role == "Administrator"){
                                    session(['adm_login'=>$sel]);
                                    

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;

                                    $conf = array();

                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;


                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;


                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;
                //}
                //return redirect()->route('dash');
                return view('admin_message');


                    }else{
                    //check ip address match
                        if($check->ip_address == $_SERVER['REMOTE_ADDR']){
                            $config = array(
                                [
                            'table_name' => 'sessions',
                            'update_values'=>['last_activity'=>time(),'logged_in'=>1],
                            'where'=>['user_id'],
                            'query_array'=>[session('adm_login')->username],
                            'query_method'=>'update'
                                ]
                                );
                                event(new UserLogin($config));
                                    
                                session(['adm_login'=>$sel]);
                                /*if($role == "Customer Manager"){
                                    session(['cust_man_login'=>$sel]);
                                    
                                    
                                }else if($role == "Customer"){
                                    session(['cust_login'=>$sel]);
                                }*///else if($role == "Administrator"){
                                  //  session(['adm_login'=>$sel]);

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                               // }
                               $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                               
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;



                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;
                               
                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                                    return view('admin_message');
                
                        }else{
                        //do not grant access
                        $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                        <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> This user is already logged in on a different IP Address </div>
                        </div>
                    </div>';
            $this->utility->s_flash($request,$succ);
            $request->session()->forget('adm_login');
            
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                        }
                    }
                endforeach;
            }else{
                
                $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
                    event(new UserLogin($config));
                    /*if($role == "Customer Manager"){
                        session(['cust_man_login'=>$sel]);
    
                    }else if($role == "Customer"){
                        session(['cust_login'=>$sel]);
                    }/*///else if($role == "Administrator"){
                        session(['adm_login'=>$sel]);
                        

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                   // $conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;




                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;



                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;
                    //}
                    //return redirect()->route('dash');
                    return view('admin_message');

                
            }
                    }else{
                    $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                    <div class="err_div" style="display:block;">
                        <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                    </div>
                </div>';
        $this->utility->s_flash($request,$succ);
        $request->session()->forget('adm_login');
        
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                }
                break;
            endforeach;
            }else{
       
                $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                <div class="err_div" style="display:block;">
                    <div class="err_details text-center featurette-H font-weight-bold"> Your services have been suspended. Contact NUTSHELL for more information </div>
                </div>
            </div>';
    $this->utility->s_flash($request,$succ);
    $request->session()->forget('adm_login');
    
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }


            //return view('dashboard');
            //return $next($request);
            //return view('dashboard');
        }else if($request->session()->has('cust_man_login')){
            //return $next($request);
        }else if($request->session()->has('cust_login')){
           // return $next($request);
        }
        
    }


    public function load_invoice(Request $request){
        if($request->session()->has('adm_login')){
            if($request->session()->has('cust_login'))
            $request->session()->forget('cust_login');
            if($request->session()->has('cust_man_login'))
            $request->session()->forget('cust_man_login');  
            //$return_page = $this->return_page($this->req->all()['role']);
            //$role = $this->req->all()['role'];


            $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active'];
            $select = $this->database->select_data($configg);
            if($select){
            foreach($select as $sel):
            $pass_hash = $sel->password;
            if($this->verify_pass(session('adm_login')->pt_password,$sel->password)){
            $conf['table_name']="sessions";
            $conf['where']=['user_id'];
            $conf['search_columns']="*";
            $conf['limit']=1;
            $conf['query_array']= [session('adm_login')->username];
            $check_login = $this->database->select_data($conf);
            if($check_login){
                foreach($check_login as $check):
                    //compare time of last activity against 12 hours
                    if((time()-$check->last_activity)>43200){
                        $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
                            event(new UserLogin($config));

                //session('current_usage',$sel);
                /*if(session('adm_login')->role == "Customer Manager"){
                    session(['cust_man_login'=>$sel]);

                }else if(session('adm_login')->role == "Customer"){
                    session(['cust_login'=>$sel]);
                }*///else if(session('adm_login')->role == "Administrator"){
                                    session(['adm_login'=>$sel]);
                                    

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;


                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;



                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;
                //}
                //return redirect()->route('dash');
                return view('admin_invoice');


                    }else{
                    //check ip address match
                        if($check->ip_address == $_SERVER['REMOTE_ADDR']){
                            $config = array(
                                [
                            'table_name' => 'sessions',
                            'update_values'=>['last_activity'=>time(),'logged_in'=>1],
                            'where'=>['user_id'],
                            'query_array'=>[session('adm_login')->username],
                            'query_method'=>'update'
                                ]
                                );
                                event(new UserLogin($config));
                                    
                                session(['adm_login'=>$sel]);
                                /*if($role == "Customer Manager"){
                                    session(['cust_man_login'=>$sel]);
                                    
                                    
                                }else if($role == "Customer"){
                                    session(['cust_login'=>$sel]);
                                }*///else if($role == "Administrator"){
                                  //  session(['adm_login'=>$sel]);

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                               // }
                               $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                               
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;



                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;
                               

                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    if($check_company_users):
                                    //foreach($check_profile as $profile):
                                        session(['company_users'=>$check_company_users]);

                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                                    return view('admin_invoice');
                
                        }else{
                        //do not grant access
                        $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                        <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> This user is already logged in on a different IP Address </div>
                        </div>
                    </div>';
            $this->utility->s_flash($request,$succ);
            $request->session()->forget('adm_login');
            
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                        }
                    }
                endforeach;
            }else{
                
                $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
                    event(new UserLogin($config));
                    /*if($role == "Customer Manager"){
                        session(['cust_man_login'=>$sel]);
    
                    }else if($role == "Customer"){
                        session(['cust_login'=>$sel]);
                    }/*///else if($role == "Administrator"){
                        session(['adm_login'=>$sel]);
                        

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;




                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;


                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_data($this->database,$check->username,'username','company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    session(['company_users'=>$c_use]);
                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;
                    //}
                    //return redirect()->route('dash');
                    return view('admin_invoice');

                
            }
                    }else{
                    $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                    <div class="err_div" style="display:block;">
                        <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                    </div>
                </div>';
        $this->utility->s_flash($request,$succ);
        $request->session()->forget('adm_login');
        
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                }
                break;
            endforeach;
            }else{
       
                $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                <div class="err_div" style="display:block;">
                    <div class="err_details text-center featurette-H font-weight-bold"> Your services have been suspended. Contact NUTSHELL for more information </div>
                </div>
            </div>';
    $this->utility->s_flash($request,$succ);
    $request->session()->forget('adm_login');
    
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }


            //return view('dashboard');
            //return $next($request);
            //return view('dashboard');
        }else if($request->session()->has('cust_man_login')){
            //return $next($request);
        }else if($request->session()->has('cust_login')){
           // return $next($request);
        }
        
    }



    public function load_meeting(Request $request){
        if($request->session()->has('adm_login')){
            if($request->session()->has('cust_login'))
            $request->session()->forget('cust_login');
            if($request->session()->has('cust_man_login'))
            $request->session()->forget('cust_man_login');  
            //$return_page = $this->return_page($this->req->all()['role']);
            //$role = $this->req->all()['role'];


            $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active'];
            $select = $this->database->select_data($configg);
            if($select){
            foreach($select as $sel):
            $pass_hash = $sel->password;
            if($this->verify_pass(session('adm_login')->pt_password,$sel->password)){
            $conf['table_name']="sessions";
            $conf['where']=['user_id'];
            $conf['search_columns']="*";
            $conf['limit']=1;
            $conf['query_array']= [session('adm_login')->username];
            $check_login = $this->database->select_data($conf);
            if($check_login){
                foreach($check_login as $check):
                    //compare time of last activity against 12 hours
                    if((time()-$check->last_activity)>43200){
                        $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
                            event(new UserLogin($config));

                //session('current_usage',$sel);
                /*if(session('adm_login')->role == "Customer Manager"){
                    session(['cust_man_login'=>$sel]);

                }else if(session('adm_login')->role == "Customer"){
                    session(['cust_login'=>$sel]);
                }*///else if(session('adm_login')->role == "Administrator"){
                                    session(['adm_login'=>$sel]);
                                    

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;


                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;


                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_data($this->database,$check->username,'username','company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    session(['company_users'=>$c_use]);
                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;
                //}
                //return redirect()->route('dash');
                return view('admin_meeting');


                    }else{
                    //check ip address match
                        if($check->ip_address == $_SERVER['REMOTE_ADDR']){
                            $config = array(
                                [
                            'table_name' => 'sessions',
                            'update_values'=>['last_activity'=>time(),'logged_in'=>1],
                            'where'=>['user_id'],
                            'query_array'=>[session('adm_login')->username],
                            'query_method'=>'update'
                                ]
                                );
                                event(new UserLogin($config));
                                    
                                session(['adm_login'=>$sel]);
                                /*if($role == "Customer Manager"){
                                    session(['cust_man_login'=>$sel]);
                                    
                                    
                                }else if($role == "Customer"){
                                    session(['cust_login'=>$sel]);
                                }*///else if($role == "Administrator"){
                                  //  session(['adm_login'=>$sel]);

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                               // }
                               $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                               
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;



                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;
                               

                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_data($this->database,$check->username,'username','company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    session(['company_users'=>$c_use]);
                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                                    return view('admin_meeting');
                
                        }else{
                        //do not grant access
                        $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                        <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> This user is already logged in on a different IP Address </div>
                        </div>
                    </div>';
            $this->utility->s_flash($request,$succ);
            $request->session()->forget('adm_login');
            
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                        }
                    }
                endforeach;
            }else{
                
                $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
                    event(new UserLogin($config));
                    /*if($role == "Customer Manager"){
                        session(['cust_man_login'=>$sel]);
    
                    }else if($role == "Customer"){
                        session(['cust_login'=>$sel]);
                    }/*///else if($role == "Administrator"){
                        session(['adm_login'=>$sel]);
                        

                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
            session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;

                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->username];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        session(['profile_adm'=>$profile,'profile_check'=>true]);

                                    endforeach;
                                else:
                                    session(['profile_check'=>false]);
                                    endif;
                                    $conf = array();
                                    $conf['table_name']="admin_notifications";
                                    $conf['where']=['username','read_flag'];
                                    $conf['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conf['query_array']= [$sel->username,'unread'];
                                    $check_notifications = $this->database->select_data($conf);
                                    if($check_notifications):
                                    //foreach($check_profile as $profile):
                                        session(['notifications_adm'=>$check_notifications]);

                                    //endforeach;
                                    else:
                                    session(['notifications_adm'=>0]);
                                    endif;




                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [$sel->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    session(['company_registration'=>$pack]);
                                    endforeach;


                                    $conff['table_name']="company_users";
                                    //$conf['where']=['username','read_flag'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    //$conf['query_array']= [$sel->username,'unread'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_data($this->database,$check->username,'username','company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    session(['company_users'=>$c_use]);
                                    //endforeach;
                                    else:
                                    session(['company_users'=>0]);
                                    endif;

                                    //if($this->check_user_data($this->database,$check_company_users->username,'username','company_administrators'))

                    //}
                    //return redirect()->route('dash');
                    return view('admin_meeting');

                
            }
                    }else{
                    $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                    <div class="err_div" style="display:block;">
                        <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                    </div>
                </div>';
        $this->utility->s_flash($request,$succ);
        $request->session()->forget('adm_login');
        
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                }
                break;
            endforeach;
            }else{
       
                $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                <div class="err_div" style="display:block;">
                    <div class="err_details text-center featurette-H font-weight-bold"> Your services have been suspended. Contact NUTSHELL for more information </div>
                </div>
            </div>';
    $this->utility->s_flash($request,$succ);
    $request->session()->forget('adm_login');
    
    return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }


            //return view('dashboard');
            //return $next($request);
            //return view('dashboard');
        }else if($request->session()->has('cust_man_login')){
            //return $next($request);
        }else if($request->session()->has('cust_login')){
           // return $next($request);
        }
        
    }


    private function check_pos($role){
        return $role == "customers" || $role == "customer_managers" || $role == "administrators";
     }
     private function verify_pass($password,$pass_hash){
         return password_verify($password,$pass_hash);
     }
     private function check_role($role){
         $role = str_replace("_"," ",$role);
         return ucwords(substr($role,0,strlen($role)-1));
         
     }
     private function return_package_benefits($bundle){
         if(starts_with($bundle,"Enterprise")){
             return ['Super Administrator'=>1,'Administrator'=>'unlimited','Customer Manager'=>'unlimited','Customer'=>'unlimited'];
         }else if(starts_with($bundle,"Professional")){
             return ['Super Administrator'=>1,'Administrator'=>20,'Customer Manager'=>100,'Customer'=>'Unlimited'];
         }else if(starts_with($bundle,"Free")){
             return ['Super Administrator'=>1,'Customer Manager'=>2,'Customer'=>4];
         }
         return null;
     }
     private function e_var($var){
         echo "$var<br>";
     }
     private function return_page($role){
         $role = strtolower($role);
         if($role == "customer manager"){
             return "dashboard_customer_manager";
         }else if($role == "administrator"){
             return "dashboard";
         }else{
             return "dashboard_customer";
         }
     }
     private function check_user_data($data,$check_value,$check_column,$table_name){
        $config['query_array'] = [$check_value];
        $config['search_columns'] = "*";
        $config['where']=[$check_column];
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return true;
        }
        return false;
    }

    private function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return true;
        }
        return false;
    }
    private function pull_user_datas($data,$check_value,$check_column,$table_name){
        $pull_data = null;
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
        $config['table_name']=$table_name;
        //$config['limit']=1;
        return $data->select_data($config)?$data->select_data($config):false;
        
    }
    private function pull_data($data,$query_array,$where,$table_name,$limit="all",$order_sequece="ASC",$order="id"){
        //$pull_data = null;
        $config['query_array'] = $query_array;
        $config['search_columns'] = "*";
        $config['where']=$where;
        $config['order']=$order;
        $config['order_sequence']=$order_sequece;
        $config['limit']=$limit != "all" ? 1 : "";
        $config['table_name']=$table_name;
        //$config['limit']=1;
        return $data->select_data($config)?$data->select_data($config):false;
        
    }
    private function ins_data_all($data_array){
        event(new DatabaseQueryEvent($data_array));
    }

}
/**
 *                          UPDATE QUERY
                            * $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
 * 
 * 
 * 
 *                          SELECT QUERY
 *                          $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active','url_extension'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active',session('comp_url')];
            $select = $this->database->select_data($configg);
 * 
 * 
 * 
 *                          INSERT QUERY
 * 
 * 
 * 
 *                      $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
 * 
 */                            


 /**
  * 
  session('super admin login set')

  DELETE QUERY
                //$config['delete_value']
                //$config_delete['delete_value']=['user_id'=>]

  */