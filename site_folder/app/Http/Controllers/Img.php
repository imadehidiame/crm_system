<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class Img extends Controller
{
    public function show(){
        $id = $_GET['id'];
        $value = URL::asset('svg/advanced_woman.png');
        $value1 = URL::asset('svg/ave.png');
        //return $id == "old"? "<img src='$value'>":"<img src='$value1'>";
        return $id == "old"? URL::asset('svg/advanced_woman.png'):URL::asset('svg/ave.png');
    }
}
