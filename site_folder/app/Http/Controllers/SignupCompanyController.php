<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SignUpCompanyRequest;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Events\DatabaseQueryEvent;
class SignupCompanyController extends Controller
{
    protected $utility;
    protected $reqs;
    protected $database;
    public function __construct(UtilityController $utility,Request $request,DatabaseQueryController $database){
        $this->utility = $utility;
        $this->reqs = $request;
        $this->database = $database;
    }
    public function free(){
        $package = "Free Package";
        $hm = 'block';
        return view('signup',compact('package','hm'));
    }
    public function pro(){
        $package = "Professional Package";
        $hm = 'block';
        return view('signup',compact('package','hm'));
    }
    public function ent(){
        $package = "Enterprise Package";
        $hm = 'block';
        return view('signup',compact('package','hm'));
    }
    public function submit_company_data(SignUpCompanyRequest $req){
        //var_dump($this->reqs->all());
        //return;
        $number = $this->reqs->all()['phone'];
        $package = $this->reqs->all()['p'];
        if(!$this->utility->validate_number_patterns($number,"Nigeria")){
            $error['phone_check']= "Invalid phone number entered";
            $this->utility->s_flash($this->reqs,$error);
            $hm = 'block';
            return view('signup',compact('package','hm'));
        }
        //var_dump($this->reqs->all());
     //config['insert_values']-->table columns config['query_array']--->table values
        $company_url = $this->reqs->all()['company'];
        $company_url = str_replace(" ","_",$company_url);
        $company_url = $this->utility->filter_url_value($company_url);
        $username = $company_url;
        $password = strtolower($this->utility->rand_chars());
        $password_enc = password_hash($password,PASSWORD_BCRYPT);
        $url_hash = password_hash($company_url,PASSWORD_BCRYPT);
        $url_hash = $this->utility->filter_url_value($url_hash);
        $url_hash = strtolower($this->utility->return_hash($url_hash));
        $customer_manager_url = "$company_url/customer_managers/$url_hash";
        $customer_url = "$company_url/customers/$url_hash";
        $admin_url = "$company_url/administrators/$url_hash";

        /*$this->e_var("Company url $company_url");
        $this->e_var("Company username $username");
        $this->e_var("Company password $password");
        $this->e_var("Company url_hash $company_url/customer_manager/$url_hash");
        $this->e_var("Company url_hash $company_url/customer/$url_hash");
        $this->e_var("Company url_hash $company_url/administrator/$url_hash");
        return;*/
        $config = array(
            [
            'insert_values' => ['name','registration_number','website','webmail','email','number_of_staffs','phone_number','package_bundle','url_extension','admin_username','admin_password','admin_pt_password'],
            'query_array'=>[$this->reqs->all()['company'],$this->reqs->all()['registration'],$this->reqs->all()['website'],$this->reqs->all()['webmail'],$this->reqs->all()['email'],$this->reqs->all()['average'],$this->reqs->all()['phone'],$this->reqs->all()['p'],$url_hash,$username,$password_enc,$password],
            'table_name'=>'company_registration',
            'query_method'=>'insert'
            ],
            [
             'insert_values' => ['username','password','pt_password','full_name','role','package_bundle','url_extension','super_admin_flag','email'],
             'query_array'=>[$username,$password_enc,$password,ucwords($this->reqs->all()['company']),'Administrator',$this->reqs->all()['p'],$url_hash,"true",$this->reqs->all()['email']],
             'table_name'=>'company_users',
             'query_method'=>'insert'
            ],
            [
             'insert_values' => ['url_hash','package'],
             'query_array'=>[$url_hash,$this->reqs->all()['p']],
             'table_name'=>'package_bundle',
             'query_method'=>'insert'
            ]
        );
         
        event(new DatabaseQueryEvent($config));

     /*$config['insert_values']=array('name','registration_number','website','webmail','email','number_of_staffs','phone_number','package_bundle','url_extension','admin_username','admin_password','admin_pt_password');
     $config['query_array']=array($this->reqs->all()['company'],$this->reqs->all()['registration'],$this->reqs->all()['website'],$this->reqs->all()['webmail'],$this->reqs->all()['email'],$this->reqs->all()['average'],$this->reqs->all()['phone'],$this->reqs->all()['p'],$url_hash,$username,$password_enc,$password);
     $config['table_name']='company_registration';
     $configg['insert_values']=array('username','password','pt_password','role','package_bundle','url_extension','super_admin_flag');
     $configg['query_array']=array($username,$password_enc,$password,'Administrator',$this->reqs->all()['p'],$url_hash,"Super Administrator");
     $configg['table_name']='company_users';*/
     
     $package_benefits = array();
        
    if($this->reqs->all()['p']=="Free Package"){
        $package_benefits = array('1 Super Administrator','2 customer managers','4 customers','Help center access');
     }else if($this->reqs->all()['p']=="Professional Package"){
        $package_benefits = array('1 Super Administrator','20 Administrators','100 customer managers','Unlimited number of customers','Help center access');
     }else if($this->reqs->all()['p']=="Enterprise Package"){
        $package_benefits = array('1 Super Administrator','Unlimited number of Administrators','Unlimited number of customer managers','No limit to customer registration','Help center access','24/7 chat support with NUTSHELL');
     }
    //$this->database->insert_data($config);
    //if($this->database->insert_data($config)&&$this->database->insert_data($configg)){
        $succ['succ_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Registration Successful. Check your email box for Login Details 
            </div>
        </div>
    </div>';
    //$error['err_div'] = $url[count($url)-1];
        $this->utility->s_flash_empty($this->reqs,$this->reqs->all());
        $this->utility->s_flash($this->reqs,$succ);
        $checked['check']="";
        $this->utility->s_flash($this->reqs,$checked);
        //$ch = $request->all()['acc'];
        //if(!empty($ch)){
      
       // }

       $this->utility->send_email_group([$this->reqs->all()['webmail'],$this->reqs->all()['email']],'em',['company_name'=>$this->reqs->all()['company'],'url_hash'=>$url_hash,'username'=>$username,'password'=>$password,'company_name_url'=>$username,'package'=>$this->reqs->all()['p'],'package_benefits'=>$package_benefits]);
       $hm = 'block';
       return redirect()->route('home');
    /*}else{
        $error['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
        <div class="err_div">
            <div class="err_details text-center featurette-H font-weight-bold"> An error occured </div>
        </div>
    </div>';
    //$this->utility->s_flash_empty($this->reqs,$this->reqs->all());
    $this->utility->s_flash($this->reqs,$error);
    $hm = 'block';
    return view('signup',compact('package','hm'));
    }*/


        
    }
    private function e_var($var){
        echo "$var<br>";
    }
}
