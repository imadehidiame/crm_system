<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController; 
use App\Http\Controllers\UtilityController;
use App\Events\UserLogin;
use App\Events\DatabaseQueryEvent;
class DashboardControllers extends Controller
{
    public $req;
    public $database;
    public $utility;
    public function __construct(DatabaseQueryController $database,UtilityController $utility){
        $this->database = $database;
        $this->utility = $utility;
/*        $this->middleware(function(Request $request,$next){
            if($request->session()->has('adm_login')){
                //return $next($request);
                //return view('dashboard');
            }else if($request->session()->has('cust_man_login')){
                //return $next($request);
            }else if($request->session()->has('cust_login')){
               // return $next($request);
            }else{
                return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            return $next($request);
        });*/
    }
    private function get_uac(){
        $u = get_browser('platform',true);
        
        foreach($_SERVER as $key=>$value):
            echo "key is $key and corresponding value is $value <br>";
        endforeach;

    }
    public function load_dashboardd(Request $request){
        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['username','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    echo "u got here";

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }
        
    


    private function check_pos($role){
        return $role == "customers" || $role == "customer_managers" || $role == "administrators";
     }
     private function verify_pass($password,$pass_hash){
         return password_verify($password,$pass_hash);
     }
     private function check_role($role){
         $role = str_replace("_"," ",$role);
         return ucwords(substr($role,0,strlen($role)-1));
         
     }
     private function return_package_benefits($bundle){
         if(starts_with($bundle,"Enterprise")){
             return ['Super Administrator'=>1,'Administrator'=>'unlimited','Customer Manager'=>'unlimited','Customer'=>'unlimited'];
         }else if(starts_with($bundle,"Professional")){
             return ['Super Administrator'=>1,'Administrator'=>20,'Customer Manager'=>100,'Customer'=>'Unlimited'];
         }else if(starts_with($bundle,"Free")){
             return ['Super Administrator'=>1,'Customer Manager'=>2,'Customer'=>4];
         }
         return null;
     }
     private function e_var($var){
         echo "$var<br>";
     }
     private function return_page($role){
         $role = strtolower($role);
         if($role == "customer manager"){
             return "dashboard_customer_manager";
         }else if($role == "administrator"){
             return "dashboard";
         }else{
             return "dashboard_customer";
         }
     }
     private function check_user_data($data,$check_value,$check_column,$table_name){
        $config['query_array'] = [$check_value];
        $config['search_columns'] = "*";
        $config['where']=[$check_column];
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return true;
        }
        return false;
    }
}
/**
 *                          UPDATE QUERY
                            * $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
 * 
 * 
 * 
 *                          SELECT QUERY
 *                          $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active','url_extension'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active',session('comp_url')];
            $select = $this->database->select_data($configg);
 * 
 * 
 * 
 *                          INSERT QUERY
 * 
 * 
 * 
 *                      $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
 * 
 */                            


 /**
  * 
  session('super admin login set')
  */