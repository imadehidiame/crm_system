<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Http\Requests\LoginCompanyRequest;
use App\Events\UserLogin;
class LoginControllers extends Controller
{
    protected $database;
    protected $utility;
    protected $req;
    static $company_url;
    static $company_id;
    static $company_role;
    public function __construct(DatabaseQueryController $database,UtilityController $utility,Request $req){
            $this->database = $database;
            $this->utility = $utility;
            $this->req = $req;
            $this->middleware(function(Request $request,$next){
            $company_role = $request->company_role;
            //LoginController::$company_role = $company_role;
            //var_dump($company_role);
            $company_url = $request->company_url;
            //LoginController::$company_url = $company_url;
            $company_id = $request->company_id;
           // LoginController::$company_id = $company_id;
            if(!$this->check_pos($company_role)){
                return redirect('/');
            }
            
            /*if($request->company_role != "administrators" || $request->company_role != "customer_managers" || $request->company_role != "customers"){
                //return redirect('/');
                //var_dump($company_role);
            }else{
              //  var_dump($company_role);
            }*/
            
            //config[table_name],config[where]-->table columns, config[query_array]-->column values
            $config['table_name']="company_registration";
            $config['where']=['url_extension'];
            $config['search_columns']="*";
            $config['limit']=1;
            $config['query_array']= [$company_url];
            $select = $this->database->select_data($config);
            if(!$select){
                return redirect('/'); 
            }
            //if($select->admin_username !== $company_id){
                foreach($select as $sel):
                if($sel->admin_username !== $company_id){
                return redirect('/');
                } 
                endforeach;
           // }
           $arr_log_session['comp_id']=$request->company_id;
           $arr_log_session['comp_role']=$request->company_role;
           $arr_log_session['comp_url']=$request->company_url;
           session($arr_log_session);
            return $next($request);
        })->only('login');
    }
    public function login($company_id,$company_role,$company_url){
        //http://127.0.0.1:8000/company/kool_aid_incorporations/customers/lqhhkcp2wiavtua9abzbugmjyunzdqatlodjnr1f5xzsj3815q4y0763650001538925651
        $company_role=$this->check_role($company_role);
        $company_roles = $this->req->company_role;
        $hm = 'none';
        return view('login',compact('company_role','hm','company_roles','company_id','company_url'));

    }
    public function login_company(LoginCompanyRequest $request){
        //var_dump($this->req->all()['role']);
        //echo LoginController::$company_id;
        //return; 
            $r = $this->req->all()['r'];
            $u = $this->req->all()['u'];
            $i = $this->req->all()['i'];
            $hm = 'none';
            //var_dump($r);
            //var_dump($u);
            //var_dump($i);
            //return;
            $return_page = $this->return_page($this->req->all()['role']);
            $role = $this->req->all()['role'];
            $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active','url_extension'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [$this->req->all()['username'],$this->req->all()['role'],'Active','Active',session('comp_url')];
            $select = $this->database->select_data($configg);
            if($select){
            foreach($select as $sel):
            $pass_hash = $sel->password;
            if($this->verify_pass($this->req->all()['password'],$sel->password)){    
                //insert into sessions and grant access
                $session_hash = str_random(30);
                session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','session_hash','url_hash'],
                'query_array'=>[session($session_hash)['data']->username,$_SERVER['REMOTE_ADDR'],time(),$session_hash,session('comp_url')],
                'query_method'=>'insert'
                    ]
                    );
                    event(new UserLogin($config));
                    return redirect()->route('dash',['rdr'=>$session_hash]);
            }else{
            //do not grant access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Invalid login details </div>
            </div>
            </div>';
            $this->utility->s_flash($this->req,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            endforeach;
        }

    }
    private function check_pos($role){
       return $role == "customers" || $role == "customer_managers" || $role == "administrators";
    }
    private function verify_pass($password,$pass_hash){
        return password_verify($password,$pass_hash);
    }
    private function check_role($role){
        $role = str_replace("_"," ",$role);
        return ucwords(substr($role,0,strlen($role)-1));
        
    }
    private function return_package_benefits($bundle){
        if(starts_with($bundle,"Enterprise")){
            return ['Super Administrator'=>1,'Administrator'=>'unlimited','Customer Manager'=>'unlimited','Customer'=>'unlimited'];
        }else if(starts_with($bundle,"Professional")){
            return ['Super Administrator'=>1,'Administrator'=>20,'Customer Manager'=>100,'Customer'=>'Unlimited'];
        }else if(starts_with($bundle,"Free")){
            return ['Super Administrator'=>1,'Customer Manager'=>2,'Customer'=>4];
        }
        return null;
    }
    private function e_var($var){
        echo "$var<br>";
    }
    private function return_page($role){
        $role = strtolower($role);
        if($role == "customer manager"){
            return "dashboard_customer_manager";
        }else if($role == "administrator"){
            return "dashboard";
        }else{
            return "dashboard_customer";
        }
    }
}


                    /**
                     * Check if user is already logged in
                     * //$select * from sessions where user_id = username LIMIT 1
                     * if($select){
                     * //check if time() - last_activity > 86400
                     * $check = time() - last_activity > 86400
                     * if($check){
                     * //log user in and update last activity,ip_address and logged_in
                     * }else{
                     * //check if ip addresses match
                     * $check_ip = $_SERVER['REMOTE_ADDR'] == $select->ip_address
                     * if($check_ip){
                     * //log user in and update last activity and logged_in
                     * }else{
                     * //user is already logged in and must log out first do not grant access
                     * 
                     * }
                     * }else{
                     * //log user in and insert into sessions
                     * }
                     * 
                     * 
                     * */   
