<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Http\Controllers\DatabaseQueryController;
use App\Events\DatabaseQueryEvent;

class UtilityController extends Controller
{
    protected $country_valid;
    protected $database;
    public function __construct(PHPMailer $mail,DatabaseQueryController $database){
        $this->mail = $mail;
        $this->database = $database;
        $this->country_valid = array(
            "United States of America"=>array("/(^\+1)[0-9]{10}$/"),
            "India"=>array("/(^\+91)[0-9]{15}$/"),
            "Nigeria"=>array("/(^0[7-9][0-1])[0-9]{8}$/","/(^234[7-9][0-1])[0-9]{8}$/","/(^2340[7-9][0-1])[0-9]{8}$/","/(^\+234[7-9][0-1])[0-9]{8}$/","/(^\+2340[7-9][0-1])[0-9]{8}$/","/(^[7-9][0-1])[0-9]{8}$/"),
            "Ghana"=>array("/(^2330)[0-9]{9}$/","/(^\+233)[0-9]{9}$/","/(^233)[0-9]{9}$/")
        );
    }
    public function clean_value($value){
        $value = htmlspecialchars(strip_tags(trim($value)));
    } 
    public function time_break_down($saved_time){ 
        $time_difference = time() - $saved_time; 
        $time_determinant = "";
        if($time_difference<60){
            $time_determinant = ($time_difference>1)?"seconds":"second";
            return $time_difference." ".$time_determinant;
        }else if($time_difference<3600){
            $rem = intval($time_difference/60);
            $time_determinant = ($rem>1)?" minutes":" minute";
            return $rem.$time_determinant;
            //$this->write_ln($rem.$minutes); 
        }else if($time_difference<86400){
            $rem = intval($time_difference/3600);
            $time_determinant = ($rem>1)?" hours":" hour";
            return $rem.$time_determinant;
        }else if($time_difference<604800){
            $rem = intval($time_difference/86400);
            $time_determinant = ($rem>1)?" days":" day";
            return $rem.$time_determinant;
        }else if($time_difference<2419200){
            $rem = intval($time_difference/604800);
            $time_determinant = ($rem>1)?" weeks":" week";
            return $rem.$time_determinant;
        }else if($time_difference<29030400){
            $rem = intval($time_difference/2419200);
            $time_determinant = ($rem>1)?" months":" month";
            return $rem.$time_determinant;
        }else{
            $rem = intval($time_difference/29030400);
            $time_determinant = ($rem>1)?" years":" year";
            return $rem.$time_determinant;
        }
    }
    public function test_util(){
        echo "Utility at work";
    }
    public function nth_representation($day){
        $day = strval($day);
        for($c = 10;$c<21;$c++){
            if(substr($day,strlen($day)-2)==strval($c)){
                return $day."th";
            }
        }
        $nth = "";
        switch(substr($day,strlen($day)-1)){
            case "1":
            $nth = "st";
            break;
            case "2":
            $nth = "nd";
            break;
            case "3":
            $nth = "rd";
            break;
            default:
            $nth = "th";
        }
        return $day.$nth;

    }
    public function validate_number_patterns($mobile,$country){
        //$count = $this->input->post($country);
        $check=false;
        if(array_key_exists($country,$this->country_valid)){
        for($count = 0; $count<count($this->country_valid[$country]); $count++){
        $check = $check || boolval(preg_match($this->country_valid[$country][$count],$mobile));    
        if($check || boolval($check) == true){ 
            break;
        } 
        }    
        }else{
         $check = true;   
        }
        return $check; 
        
    }
    public function send_email($email,$view_file,$array_data){
        try {
            $this->mail->SMTPDebug = 0;                             
           $this->mail->isSMTP();
           $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true 
            )
        );                       
                  
           $this->mail->Host = 'smtp.gmail.com';                   
           $this->mail->SMTPAuth = true;                           
           $this->mail->Username = env('EM_USE');               
           $this->mail->Password = env('EM_PASS');                
           $this->mail->SMTPSecure = 'tls';                        
           $this->mail->Port = 587;                                
           $this->mail->setFrom(env('EM_USE'), 'NUTSHELL');
           $this->mail->addAddress($email);
           $this->mail->isHTML(true);                      
           $this->mail->Subject = "Hello there";
           $this->mail->Body = view($view_file,$array_data)->render() ;
            $this->mail->AltBody = "Hi there";
            $this->mail->send();
           // echo "sent successfully";
        } catch (Exception $e) {
            //echo 'Message could not be sent. Mailer Error: ', $this->mail->ErrorInfo;
        }


    }


    public function send_email_group($email_array,$view_file,$array_data){
        try {
            $this->mail->SMTPDebug = 0;                             
           $this->mail->isSMTP();
           $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true 
            )
        );                       
                  
           $this->mail->Host = 'smtp.gmail.com';                   
           $this->mail->SMTPAuth = true;                           
           $this->mail->Username = env('EM_USE');               
           $this->mail->Password = env('EM_PASS');                
           $this->mail->SMTPSecure = 'tls';                        
           $this->mail->Port = 587;                                
           $this->mail->setFrom(env('EM_USE'), 'NUTSHELL');

           foreach($email_array as $email):
            $this->mail->addAddress($email);
           $this->mail->isHTML(true);                      
           $this->mail->Subject = "Hello there";
           $this->mail->Body = view($view_file,$array_data)->render() ;
            $this->mail->AltBody = "Hi there";
            $this->mail->send();
           endforeach;

           
           // echo "sent successfully";
        } catch (Exception $e) {
            //echo 'Message could not be sent. Mailer Error: ', $this->mail->ErrorInfo;
        }


    }

    public function validate_number_offset_country($string,$country){
        $returnString = "";
        //08055860720 +234
        if(empty($string) || empty($country)){
            return false;
        }
        if(!$this->validate_number_patterns($string,$country)){
            return "invalid";
        }

        if($country === "Nigeria"){
            //$string = "23508055860720";
            $str = array();
            for($count = 1; $count <=10; $count++){
                $str[] = substr($string,(strlen($string)-$count),1);
            }
            $final_str = "";
            for($count = count($str)-1;$count>=0;$count--){
                $final_str.=$str[$count];
            }
            $returnString = $final_str;
            
        }else{
        $returnString= $string;
        }
        return $returnString;
    }
    public function create_flash_array($key,$value){
        $init = array();
        if(is_array($key)&&is_array($value)){
            for($count = 0; $count<count($key);$count++):
               // $init[$key[]]
            endfor;
        }
    }
    public function break_url($request){
        $path = $request->path();
        $url = explode("/",$path);
        return $url[count($url)-1];
    }
    public function s_flash($request,$data){
        $keys=array_keys($data);
        foreach($keys as $key):
            if($key == "_token"){
                continue;
            }
            if(is_array($data[$key])){
                $arr_keys = array_keys($data[$key]);
                foreach($arr_keys as $k):
                    $request->session()->flash(strval($key).strval($k),$data[$key][$k]);        
                endforeach;
            }else{
                $request->session()->flash($key,$data[$key]);
            }
        endforeach;
    }
    public function s_flash_empty($request,$data){
        $keys=array_keys($data);
        foreach($keys as $key):
            if($key == "_token"){ 
                continue;
            }
            if(is_array($data[$key])){
                $arr_keys = array_keys($data[$key]);
                foreach($arr_keys as $k):
                    $request->session()->flash(strval($key).strval($k),'');        
                endforeach;
            }else{
                $request->session()->flash($key,'');
            }
        endforeach;
    }
    public function filter_url_value($string){
        $string = strtolower($string);
        $invalid_characters=[',','.','/','\\','^','&','%',' ','>','<','-','+','?','`','@','[',']','{','}','|','(',')','*','#','~','!','?'];
        $return_string = "";
        foreach($invalid_characters as $invalid):
            $return_string = str_replace($invalid,"",$string);
        endforeach;
        return $return_string;
    }
    public function rand_chars() {
        $stringAlpha='';
        $stringNum='';
        $alpha='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num='0123456789';
        $maxAlpha=  strlen($alpha)-1;
        $maxNum=  strlen($num)-1;
        for($test=0;$test<14;$test++){
        $stringNum.=$num[mt_rand(0, $maxNum)];    
        }
        for($test=0;$test<6;$test++){
        $stringAlpha.=$alpha[mt_rand(0, $maxAlpha)];    
        $stringAlpha.=$num[mt_rand(0, $maxNum)];    
        }
        return $stringAlpha;
    }
    public function return_hash($value){
        $value = str_replace(' ','',password_hash($value,PASSWORD_BCRYPT).microtime());
        $value = str_replace('/','',$value);
        $value = str_replace('\\','',$value); 
        $value = str_replace('.','',$value);
        $value = str_replace(',','',$value); 
        $value = str_replace('$2y$10$','',$value);
        return strtoupper($value);
    }
    
    private function return_month(){
    return array("January","February","March","April","May","June","July","August","September","October","November","December");
    }
    public function current_time_definition($time){
        if(!is_int($time)){
            //throw new IllegalArgumentException();
            return false;
        }
        $arr = localtime($time,true);
        $month_array = $this->return_month();
        $return_array['year']=$arr["tm_year"]+1900;
        $return_array['month']=$month_array[$arr["tm_mon"]];
        $return_array['dayth']=$this->nth_representation($arr["tm_mday"]);
        $return_array['day']=$arr["tm_mday"];
        $return_array['hour']=$arr["tm_hour"];
        $return_array['minute']=$arr["tm_min"];
        $return_array['second']=$arr["tm_sec"];

        return $return_array;
}
public function pull_data($data,$query_array,$where,$table_name,$limit="all",$order_sequence="ASC",$order="id"){
    //$pull_data = null;
    $config['query_array'] = $query_array;
    $config['search_columns'] = "*";
    $config['where']=$where;
    $config['order']=$order;
    $config['order_sequence']=$order_sequence;
    $config['limit']=$limit != "all" ? 1 : "";
    $config['table_name']=$table_name;
    //$config['limit']=1;
    return $data->select_data($config)?$data->select_data($config):false;
    
}
public function insert_data_all($data_array){
    event(new DatabaseQueryEvent($data_array));
}

}
