<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Events\UserLogin;
use App\Events\DatabaseQueryEvent;

class DashboardCustomer extends Controller
{
    public $req;
    public $database;
    public $utility;
    public function __construct(DatabaseQueryController $database,UtilityController $utility){
        $this->database = $database;
        $this->utility = $utility;
    }
    public function load_dashboardd(Request $request){ 
        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $select = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')],['user_id','session_hash','url_hash'],'sessions',1);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Customer"){
                        $profile_check = false;
                        
                        $recheck_user = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username],['url_extension','username'],'company_users',1);
                        $customer_manager = array();
                        if($recheck_user){
                         foreach($recheck_user as $user):   
                         session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                         $customer=$user;
                        endforeach;
                        }
                        $display_assets = 'block';


                                    $profile_customer = array();
                            $check_profile = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension],['username','url_extension'],'company_customers',1);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_customer=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';

                                    //$profilefalse]);
                                    endif;
                            
                            $check_super_notifications = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username],['url_extension','read_flag','customer'],'customer_notifications','all');
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    //echo "great";
                                    return view('profile_customer',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'profile_customer'=>$profile_customer,'customer'=>$customer]);
                }else{
            //no accesss url alteration or session fixation attack
            $request->session()->forget($_GET['rdr']);
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);

        }
            }else{
            //no access session data not found in database
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access session has expired
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }
    public function load_invoice(Request $request){

        
        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Customer"){
                        $profile_check = false;
                        $configg = array();
                        //$super_admin_check = true;
                        
                        $recheck_user = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username],['url_extension','username'],'company_users',1); 
                        $customer = array();
                        if($recheck_user){
                         foreach($recheck_user as $user):   
                         session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                         $customer=$user;
                        endforeach;
                        }
                        $display_assets = 'block';



                                    $check_profile = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension],['username','url_extension'],'company_customers',1);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_customer=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    return redirect()->route('dash_customer',['rdr'=>$_GET['rdr']]);
                                    //$profilefalse]);
                                    endif;
                            $configg = array();
                            $configg['table_name']="customer_notifications";
                            $configg['where']=['url_extension','read_flag','customer'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }


                            $check_invoice = null;
                            $configg = array();
                            $configg['table_name']="invoice";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            //$check_super_notifications = $this->database->select_data($configg); 
                            if($this->database->select_data($configg)){
                                $check_invoice = $this->database->select_data($configg);
                            }

                            return view('customer_invoice',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'profile_customer'=>$profile_customer,'check_invoice'=>$check_invoice]);               



                        }else{
                            //no accesss url alteration or session fixation attack
                            $request->session()->forget($_GET['rdr']);
                            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                            <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                            </div>
                            </div>';
                            $this->utility->s_flash($request,$succ);
                            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                
                        }
                            }else{
                            //no access session data not found in database
                            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                            <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                            </div>
                            </div>';
                            $this->utility->s_flash($request,$succ);
                            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                            }
                            
                        }else{
                            //no access session has expired
                            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                            <div class="err_div" style="display:block;">
                            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
                            </div>
                            </div>';
                            $this->utility->s_flash($request,$succ);
                            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
                        }

    }

    public function load_meeting(Request $request){

        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Customer"){
                        $profile_check = false;
                        $configg = array();
                        //$super_admin_check = true;
                        
                        $recheck_user = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username],['url_extension','username'],'company_users',1); 
                        $customer = array();
                        if($recheck_user){
                         foreach($recheck_user as $user):   
                         session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                         $customer=$user;
                        endforeach;
                        }
                        $display_assets = 'block';



                                    $check_profile = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension],['username','url_extension'],'company_customers',1);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_customer=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    return redirect()->route('dash_customer',['rdr'=>$_GET['rdr']]);
                                    //$profilefalse]);
                                    endif;
                            $configg = array();
                            $configg['table_name']="customer_notifications";
                            $configg['where']=['url_extension','read_flag','customer'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }



                            
                            
                            $configg = array();
                            $configg['table_name']="meetings";
                            $configg['where']=['url_extension','customer'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $check_meetings = $this->database->select_data($configg);
                            $customer_meetings = null;
                            //$customer_information_array = null;
                            $count=0;
                            if($check_meetings){
                                foreach($check_meetings as $meeting):
                                    //$customer_array_data=$this->check_pull_data($this->database,[$meeting->customer],['username'],'company_users',1);
                                    //foreach($customer_array_data as $cust):
                                      //  $customer_meetings[$count]['full_name']=$cust->full_name;    
                                    //endforeach;
                                    $customer_meetings[$count]['username']=$meeting->customer_manager;
                                    $customer_meetings[$count]['meeting_status']=$meeting->meeting_status;
                                    $customer_meetings[$count]['venue']=$meeting->venue;
                                    $customer_meetings[$count]['date']=$meeting->meeting_date;
                                    $customer_meetings[$count]['meeting_id']=$meeting->meeting_id;
                                    $customer_meetings[$count]['colour'] = $meeting->meeting_status=="Accept"?"success":"danger";
                                    $customer_meetings[$count]['host']=$meeting->host;
                                    $count++;
                                endforeach;
                            } 
                            $button_editable = true;
                            if($check_meetings){
                                if(strtotime($customer_meetings[$count-1]['date'])>time()){
                                    $button_editable=false;
                                }
                            }
             return view('customer_meeting',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'profile_customer'=>$profile_customer,'customer_meetings'=>$customer_meetings,'button_editable'=>$button_editable]);
                }else{
            //no accesss url alteration or session fixation attack
            $request->session()->forget($_GET['rdr']);
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);

        }
            }else{
            //no access session data not found in database
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access session has expired
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }
}
