<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Events\UserLogin;
use App\Events\DatabaseQueryEvent;
class DashboardCustomerManager extends Controller
{
    public $req;
    public $database;
    public $utility;
    public function __construct(DatabaseQueryController $database,UtilityController $utility){
        $this->database = $database;
        $this->utility = $utility;
/*        $this->middleware(function(Request $request,$next){
            if($request->session()->has('adm_login')){
                //return $next($request);
                //return view('dashboard');
            }else if($request->session()->has('cust_man_login')){
                //return $next($request);
            }else if($request->session()->has('cust_login')){
               // return $next($request);
            }else{
                return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            return $next($request);
        });*/
    }



    public function load_invoicee(Request $request){

        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Customer Manager"){
                        $profile_check = false;
                        $configg = array();
                        //$super_admin_check = true;
                        $configg['table_name']="company_users";
                        $configg['where']=['url_extension','username'];
                        $configg['search_columns']="*";
                        $configg['limit']=1;
                        $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                        $recheck_user = $this->database->select_data($configg); 
                        $customer_manager = array();
                        if($recheck_user){
                         foreach($recheck_user as $user):   
                         session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                         $customer_manager=$user;
                        endforeach;
                        }
                        $display_assets = 'block';


                                    $profile_customer_manager = array();
                                    $conf = array();
                                    $conf['table_name']="company_customer_managers";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_customer_manager=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    return redirect()->route('dash_customer_manager',['rdr'=>$_GET['rdr']]);
                                    //$profilefalse]);
                                    endif;
                            $configg = array();
                            $configg['table_name']="customer_manager_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }

                            $check_invoice = null;
                            $configg = array();
                            $configg['table_name']="invoice";
                            $configg['where']=['url_extension','customer_manager_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            //$check_super_notifications = $this->database->select_data($configg); 
                            if($this->database->select_data($configg)){
                                $check_invoice = $this->database->select_data($configg);
                            }
 
             return view('customer_manager_invoice',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'check_invoice'=>$check_invoice]);
                }else{
            //no accesss url alteration or session fixation attack
            $request->session()->forget($_GET['rdr']);
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);

        }
            }else{
            //no access session data not found in database
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access session has expired
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }



    public function load_meeting(Request $request){

        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Customer Manager"){
                        $profile_check = false;
                        $configg = array();
                        //$super_admin_check = true;
                        $configg['table_name']="company_users";
                        $configg['where']=['url_extension','username'];
                        $configg['search_columns']="*";
                        $configg['limit']=1;
                        $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                        $recheck_user = $this->database->select_data($configg); 
                        $customer_manager = array();
                        if($recheck_user){
                         foreach($recheck_user as $user):   
                         session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                         $customer_manager=$user;
                        endforeach;
                        }
                        $display_assets = 'block';


                                    $profile_customer_manager = array();
                                    $conf = array();
                                    $conf['table_name']="company_customer_managers";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_customer_manager=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    return redirect()->route('dash_customer_manager',['rdr'=>$_GET['rdr']]);
                                    //$profilefalse]);
                                    endif;
                            $configg = array();
                            $configg['table_name']="customer_manager_notifications";
                            $configg['where']=['url_extension','read_flag','customer_manager'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }



                            
                            
                            $configg = array();
                            $configg['table_name']="meetings";
                            $configg['where']=['url_extension','customer_manager'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $check_meetings = $this->database->select_data($configg);
                            $customer_meetings = null;
                            //$customer_information_array = null;
                            $count=0;
                            if($check_meetings){
                                foreach($check_meetings as $meeting):
                                    $customer_array_data=$this->check_pull_data($this->database,[$meeting->customer],['username'],'company_users',1);
                                    foreach($customer_array_data as $cust):
                                        $customer_meetings[$count]['full_name']=$cust->full_name;    
                                    endforeach;
                                    $customer_meetings[$count]['username']=$meeting->customer;
                                    $customer_meetings[$count]['meeting_status']=$meeting->meeting_status;
                                    $customer_meetings[$count]['venue']=$meeting->venue;
                                    $customer_meetings[$count]['date']=$meeting->meeting_date;
                                    $customer_meetings[$count]['meeting_id']=$meeting->meeting_id;
                                    $customer_meetings[$count]['colour'] = $meeting->meeting_status=="Accept"?"success":"danger";
                                    $count++;
                                endforeach;
                            } 
                            $pull_customers = $this->check_pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username],['url_extension','customer_manager_flag'],'company_users','all');
             return view('customer_manager_meeting',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'profile_customer_manager'=>$profile_customer_manager,'customer_meetings'=>$customer_meetings,'pull_customers'=>$pull_customers]);
                }else{
            //no accesss url alteration or session fixation attack
            $request->session()->forget($_GET['rdr']);
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);

        }
            }else{
            //no access session data not found in database
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access session has expired
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }

    public function load_messagee(Request $request){


        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Customer Manager"){
                        $profile_check = false;
                        $configg = array();
                        //$super_admin_check = true;
                        $configg['table_name']="company_users";
                        $configg['where']=['url_extension','username'];
                        $configg['search_columns']="*";
                        $configg['limit']=1;
                        $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                        $recheck_user = $this->database->select_data($configg); 
                        $customer_manager = array();
                        if($recheck_user){
                         foreach($recheck_user as $user):   
                         session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                         $customer_manager=$user;
                        endforeach;
                        }
                        $display_assets = 'block';


                                    $profile_customer_manager = array();
                                    $conf = array();
                                    $conf['table_name']="company_customer_managers";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_customer_manager=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    return redirect()->route('dash_customer_manager',['rdr'=>$_GET['rdr']]);
                                    //$profilefalse]);
                                    endif;
                            $configg = array();
                            $configg['table_name']="customer_manager_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }


 
             return view('customer_manager_message',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check]);
                }else{
            //no accesss url alteration or session fixation attack
            $request->session()->forget($_GET['rdr']);
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);

        }
            }else{
            //no access session data not found in database
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access session has expired
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }


    public function load_customer_managerr(Request $request){


        
        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    




                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;

                                    endforeach;
                                else:
                                    $profile_adm=false;
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;

                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Administrator'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users=$c_use;
                                    //endforeach;
                                    else:
                                    $company_users=0;
                                    endif;


                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Customer Manager'];
                                    $check_company_users_man = $this->database->select_data($conff);
                                    $c_use_man = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users_man):
                                        $count = 0;
                                    foreach($check_company_users_man as $check):
                                        $c_use_man[$count]['username']=$check->username;
                                        $c_use_man[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use_man[$count]['email']=$check->email;
                                        $c_use_man[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users_man=$c_use_man;
                                    //endforeach;
                                    else:
                                    $company_users_man=0;
                                    endif;




                                    return view('admin_customer_manager',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'company_users'=>$company_users,'company_users_man'=>$company_users_man]);

                        }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                                    

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;


                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;



                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conf['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Administrator'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users=$c_use;
                                    //endforeach;
                                    else:
                                    $company_users=0;
                                    endif;


                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Customer Manager'];
                                    $check_company_users_man = $this->database->select_data($conff);
                                    $c_use_man = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users_man):
                                        $count = 0;
                                    foreach($check_company_users_man as $check):
                                        $c_use_man[$count]['username']=$check->username;
                                        $c_use_man[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use_man[$count]['email']=$check->email;
                                        $c_use_man[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users_man=$c_use_man;
                                    //endforeach;
                                    else:
                                    $company_users_man=0;
                                    endif;




                                    return view('admin_customer_manager',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'company_users'=>$company_users,'company_users_man'=>$company_users_man]);

                                    
                        }
                    }else if(session($_GET['rdr'])['data']->role == "Customer Manager"){

                    }else if(session($_GET['rdr'])['data']->role == "Customer"){


                    }

                        

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }




    }



    public function load_administratorr(Request $request){



        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    




                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;

                                    endforeach;
                                else:
                                    $profile_adm=false;
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;

                                    $conff = array();
                                    $conff['table_name']="company_users";
                                    $conff['where']=['url_extension','role'];
                                    $conff['search_columns']="*";
                                    //$conf['limit']=1;
                                    $conff['query_array']= [session($_GET['rdr'])['data']->url_extension,'Administrator'];
                                    $check_company_users = $this->database->select_data($conff);
                                    $c_use = array();
                                    /**
                                     * 
                                     * function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
                                     */
                                    if($check_company_users):
                                        $count = 0;
                                    foreach($check_company_users as $check):
                                        $c_use[$count]['username']=$check->username;
                                        $c_use[$count]['profile_created_status']=$this->check_user_datas($this->database,[session($_GET['rdr'])['data']->url_extension,$check->username],['url_extension','username'],'company_administrators')?"Yes":"No";
                                        $c_use[$count]['email']=$check->email;
                                        $c_use[$count]['active_status']=$check->active_status;
                                        $count++;
                                        
                                    endforeach;
                                    $company_users=$c_use;
                                    //endforeach;
                                    else:
                                    $company_users=0;
                                    endif;




                                    return view('admin_administrator',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'company_users'=>$company_users]);

                        }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                                    

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;


                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    return view('profile',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration]);


                        }
                    }else if(session($_GET['rdr'])['data']->role == "Customer Manager"){

                    }else if(session($_GET['rdr'])['data']->role == "Customer"){


                    }

                        

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }



    }
    public function load_profilee(Request $request){


        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Administrator"){
                        $prof_nav_link = " active";
                        //session([$session_hash=>['data'=>$sel,'hash'=>$session_hash]]);
                            $configg = array();
                            //$super_admin_check = true;
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','username'];
                            $configg['search_columns']="*";
                            $configg['limit']=1;
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                            $recheck_user = $this->database->select_data($configg); 
                            if($recheck_user){
                             foreach($recheck_user as $user):   
                             session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                            endforeach;
                            }
                            $display_assets = 'block';
                            $display_admins = 'none';
                            $super_notifications = array();
                            $pack = array();
                            $package_benefits = array();
                            $profile_check = false;
                            $super_admin_check = false;
                        if(session($_GET['rdr'])['data']->super_admin_flag == 'true'){
                            $configg = array();
                            $display_admins = 'block';
                            $super_admin_check = true;
                            $configg['table_name']="admin_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            $profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    




                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;

                                    endforeach;
                                else:
                                    $profile_adm=false;
                                    endif;

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;



                                    return view('profile',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'prof_nav_link'=>$prof_nav_link]);

                        }else{
                            //echo "Admin";
                                    $profile_adm = array();
                                    $conf = array();
                                    $conf['table_name']="company_administrators";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_adm=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    //$profilefalse]);
                                    endif;
                                    

                                    $conf = array();
                                    $conf['table_name']="company_registration";
                                    $conf['where']=['url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $pack):
                                    $company_registration=$pack;
                                    endforeach;


                            $configg['table_name']="admin_notifications_normal";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    $conf = array();
                                    $conf['table_name']="package_bundle";
                                    $conf['where']=['url_hash'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                    foreach($this->database->select_data($conf) as $packk):
                                        $pack = $packk;
                                        $package_benefits = $this->return_package_benefits($packk->package);
                //session(['package_bundle'=>$pack,'package_benefits'=>$this->return_package_benefits($pack->package)]);
                                    endforeach;
                                    return view('profile',['super_notifications'=>$super_notifications,'package_benefits'=>$package_benefits,'pack'=>$pack,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'super_admin_check'=>$super_admin_check,'display_admins'=>$display_admins,'profile_adm'=>$profile_adm,'company_registration'=>$company_registration,'prof_nav_link'=>$prof_nav_link]);


                        }
                    }else if(session($_GET['rdr'])['data']->role == "Customer Manager"){
                       


                    }else if(session($_GET['rdr'])['data']->role == "Customer"){
                        

                    }

                        

            }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }




    }
    public function load_dashboardd(Request $request){ 
        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Customer Manager"){
                        $profile_check = false;
                        $configg = array();
                        //$super_admin_check = true;
                        $configg['table_name']="company_users";
                        $configg['where']=['url_extension','username'];
                        $configg['search_columns']="*";
                        $configg['limit']=1;
                        $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                        $recheck_user = $this->database->select_data($configg); 
                        $customer_manager = array();
                        if($recheck_user){
                         foreach($recheck_user as $user):   
                         session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                         $customer_manager=$user;
                        endforeach;
                        }
                        $display_assets = 'block';


                                    $profile_customer_manager = array();
                                    $conf = array();
                                    $conf['table_name']="company_customer_managers";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_customer_manager=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';

                                    //$profilefalse]);
                                    endif;
                            $configg = array();
                            $configg['table_name']="customer_manager_notifications";
                            $configg['where']=['url_extension','read_flag','customer_manager'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }
                                    //echo "great";
                                    return view('profile_customer_manager',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'profile_customer_manager'=>$profile_customer_manager,'customer_manager'=>$customer_manager]);
                }else{
            //no accesss url alteration or session fixation attack
            $request->session()->forget($_GET['rdr']);
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);

        }
            }else{
            //no access session data not found in database
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access session has expired
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }







    public function load_customer(Request $request){ 
        if(!isset($_GET['rdr'])){
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }
        if($request->session()->has($_GET['rdr'])){
            $configg['table_name']="sessions";
            $configg['where']=['user_id','session_hash','url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url',' ')];
            $select = $this->database->select_data($configg);
            if($select){
                //update session and load dash board
                $config = array();
                $config = array(
                    [
                        'table_name' => 'sessions',
                        'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                        'where'=>['user_id','session_hash','url_hash'],
                        'query_array'=>[session($_GET['rdr'])['data']->username,$_GET['rdr'],session('comp_url')],
                        'query_method'=>'update'
                    ]
                    );

                    /**
                     * in production check ip adddress and do further validations
                     */
                    event(new DatabaseQueryEvent($config));
                    if(session($_GET['rdr'])['data']->role == "Customer Manager"){
                        $profile_check = false;
                        $configg = array();
                        //$super_admin_check = true;
                        $configg['table_name']="company_users";
                        $configg['where']=['url_extension','username'];
                        $configg['search_columns']="*";
                        $configg['limit']=1;
                        $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username];
                        $recheck_user = $this->database->select_data($configg); 
                        $customer_manager = array();
                        if($recheck_user){
                         foreach($recheck_user as $user):   
                         session([$_GET['rdr']=>['data'=>$user,'hash'=>$_GET['rdr']]]);  
                         $customer_manager=$user;
                        endforeach;
                        }
                        $display_assets = 'block';


                                    $profile_customer_manager = array();
                                    $conf = array();
                                    $conf['table_name']="company_customer_managers";
                                    $conf['where']=['username','url_extension'];
                                    $conf['search_columns']="*";
                                    $conf['limit']=1;
                                    $conf['query_array']= [session($_GET['rdr'])['data']->username,session($_GET['rdr'])['data']->url_extension];
                                    $check_profile = $this->database->select_data($conf);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_customer_manager=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    return redirect()->route('dash_customer_manager',['rdr'=>$_GET['rdr']]);
                                    //$profilefalse]);
                                    endif;
                            $configg = array();
                            $configg['table_name']="customer_manager_notifications";
                            $configg['where']=['url_extension','read_flag'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,'unread'];
                            $check_super_notifications = $this->database->select_data($configg); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }



                                $configg = array();
                                $configg['table_name']="company_registration";
                                $configg['where']=['url_extension'];
                                $configg['search_columns']="*";
                                $configg['query_array']= [session($_GET['rdr'])['data']->url_extension];
                                $check_company = $this->database->select_data($configg);
                                $ch_company = array();
                                if($check_company){
                                    foreach($check_company as $check):
                                        $ch_company = $check;
                                    endforeach;
                                }
                            
                            $configg = array();
                            $configg['table_name']="company_users";
                            $configg['where']=['url_extension','customer_manager_flag','role'];
                            $configg['search_columns']="*";
                            $configg['query_array']= [session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username,'Customer'];
                            $check_customers = $this->database->select_data($configg);
                            $customer_array = array();
                            $count=0;
                            if($check_customers){
                                foreach($check_customers as $customer):
                                    $customer_array[$count]['full_name']=$customer->full_name;
                                    $customer_array[$count]['username']=$customer->username;
                                    $customer_array[$count]['email']=$customer->email;
                                    $customer_array[$count]['active_status']=$customer->active_status;
                                    $customer_array[$count]['profile_created'] = $this->check_reloaded($this->database,[$customer->username],['username'],'company_customers')?'Yes':'No';
                                    $count++;
                                endforeach;
                            } 
             return view('customer_manager_customer',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'profile_customer_manager'=>$profile_customer_manager,'customer_manager'=>$customer_manager,'customer_array'=>$customer_array,'check_company'=>$ch_company]);
                }else{
            //no accesss url alteration or session fixation attack
            $request->session()->forget($_GET['rdr']);
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);

        }
            }else{
            //no access session data not found in database
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
            }
            
        }else{
            //no access session has expired
            $succ['errr_div']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> Please log in to continue </div>
            </div>
            </div>';
            $this->utility->s_flash($request,$succ);
            return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
        }

    }





    private function get_uac(){
        $u = get_browser('platform',true);
        
        foreach($_SERVER as $key=>$value):
            echo "key is $key and corresponding value is $value <br>";
        endforeach;

    }
    private function check_pos($role){
        return $role == "customers" || $role == "customer_managers" || $role == "administrators";
     }
     private function verify_pass($password,$pass_hash){
         return password_verify($password,$pass_hash);
     }
     private function check_role($role){
         $role = str_replace("_"," ",$role);
         return ucwords(substr($role,0,strlen($role)-1));
         
     }
     private function return_package_benefits($bundle){
         if(starts_with($bundle,"Enterprise")){
             return ['Super Administrator'=>1,'Administrator'=>'unlimited','Customer Manager'=>'unlimited','Customer'=>'unlimited'];
         }else if(starts_with($bundle,"Professional")){
             return ['Super Administrator'=>1,'Administrator'=>20,'Customer Manager'=>100,'Customer'=>'Unlimited'];
         }else if(starts_with($bundle,"Free")){
             return ['Super Administrator'=>1,'Customer Manager'=>2,'Customer'=>4];
         }
         return null;
     }
     private function e_var($var){
         echo "$var<br>";
     }
     private function return_page($role){
         $role = strtolower($role);
         if($role == "customer manager"){
             return "dashboard_customer_manager";
         }else if($role == "administrator"){
             return "dashboard";
         }else{
             return "dashboard_customer";
         }
     }
     private function check_user_data($data,$check_value,$check_column,$table_name){
        $config['query_array'] = [$check_value];
        $config['search_columns'] = "*";
        $config['where']=[$check_column];
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return true;
        }
        return false;
    }

    private function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return true;
        }
        return false;
    }
    private function check_reloaded($data,$query_array,$where,$table_name){
        $config['query_array'] = $query_array;
        $config['search_columns'] = "*";
        $config['where']=$where;
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return true;
        }
        return false;
    }
    private function check_pull_data($data,$query_array,$where,$table_name,$limit="all"){
        $config['query_array'] = $query_array;
        $config['search_columns'] = "*";
        $config['where']=$where;
        $config['table_name']=$table_name;
        $config['limit']=$limit=="all"?"":1;
        return $data->select_data($config)?$data->select_data($config):false;
        
    }
}
/**
 *                          UPDATE QUERY
                            * $config = array(
                            [
                                'table_name' => 'sessions',
                                'update_values'=>['last_activity'=>time(),'ip_address'=>$_SERVER['REMOTE_ADDR'],'logged_in'=>1],
                                'where'=>['user_id'],
                                'query_array'=>[session('adm_login')->username],
                                'query_method'=>'update'
                            ]
                            );
 * 
 * 
 * 
 *                          SELECT QUERY
 *                          $configg['table_name']="company_users";
            $configg['where']=['username','role','active_status','company_active','url_extension'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session('adm_login')->username,session('adm_login')->role,'Active','Active',session('comp_url')];
            $select = $this->database->select_data($configg);
 * 
 * 
 * 
 *                          INSERT QUERY
 * 
 * 
 * 
 *                      $config = array(
                    [
                'table_name' => 'sessions',
                'insert_values'=>['user_id','ip_address','last_activity','logged_in'],
                'query_array'=>[session('adm_login')->username,$_SERVER['REMOTE_ADDR'],time(),1],
                'query_method'=>'insert'
                    ]
                    );
 * 
 */                            


 /**
  * 
  session('super admin login set')
  */