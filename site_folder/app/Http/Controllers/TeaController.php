<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\TeaRequest;
use App\Http\Controllers\DatabaseQueryController;
class TeaController extends Controller
{
    protected $data;
    public function __construct(DatabaseQueryController $data){
        $this->middleware(function(Request $request,$next){
            if($request->age > 1989){
                return redirect('/');
            }
            return $next($request);
        })->only('index');
        $this->data = $data;
    }
    //
    public function index(Request $request,$age){
        
        //$dob = array('dob'=>$age,'name'=>'Leo');
        $name = 'Leo';
        $full_url = $request->fullUrl();
        $half_url = $request->path();
        $age1 = 2055;
        //$request->session()->flush();
        //$age = empty(session('age_check')) session('age_check')
        if(!empty(session('age_check'))){
            $age = session('age_check');
        }
        if(!empty(session('age_check1'))){
            $age1 = session('age_check1');
        }
        
        

        $config['table_name'] = "admins";
        $config['insert_values']=['username','password','pt_password','email','mobile_number','address','updated_at'];
        $config['query_array'] = ["freetone",'victory',"victory",'img@gm.com','0903930300',"Nigeria",time()];
        $this->data->insert_data($config);
        $config['delete_value'] = ["id"=>2];
        $config['search_columns'] = "*";
        $config['where'] = ['username'];
        $config['query_array'] = null;
        $config['update_values'] = ['username'=>"Leonardo",'updated_at'=>time()];

        $num = $this->data->update_data($config);

        $rows = !empty($num)?$num:0;
        //$config['where'] = null;
        $users = $this->data->select_data($config);
        $sess_age = !empty(session('age_check')) ? session('age_check') : 2010;
        return view('tea',compact('name','age','sess_age','full_url','half_url','age1','users','rows'));
    }
    public function posting(TeaRequest $request){
       // $request->session()->get('age_check',function(){
         //   return $_POST['age'];
        //});
        //$all = $requests->all();
        //$validator = Validator::make($all,['timo'=>'required','timo1'=>'required'],['timo.required'=>'Please specify the timo field']);
        //$this->s_flash($requests,$all);
    //    $validated = $request->validated();
        /*$request->validate(
            ['timo'=>'required|email','timo1'=>'required']
        );*/
        
        //print_r(array_keys($all));
        
        var_dump($all);
        //session(['age_check'=>$all['age'][0]]);
        //session(['age_check1'=>$all['age'][1]]);
        //$request->session()->flash('age0',$all['age'][0]);
        //$request->session()->flash('age1',$all['age'][1]);
        //$_SESSION['age_check']=$_POST['age'];
        //print_r($all);
        //return redirect()->route('tee',['age'=>1988]);
        //var_dump(session('age_check'));
        //var_dump($_POST['age']);
    }
    private function s_flash($request,$data){
        $keys=array_keys($data);
        foreach($keys as $key):
            if($key == "_token"){
                continue;
            }
            if(is_array($data[$key])){
                $arr_keys = array_keys($data[$key]);
                foreach($arr_keys as $k):
                    $request->session()->flash(strval($key).strval($k),$data[$key][$k]);        
                endforeach;
            }else{
                $request->session()->flash($key,$data[$key]);
            }
        endforeach;
    }
}
