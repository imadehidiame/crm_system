<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Events\DatabaseQueryEvent;
use App\Http\Requests\CreateAdminRequest;
use App\Http\Requests\CreateCustomerManagerRequest;

class CreateAdminController extends Controller
{
    public function __construct(UtilityController $utility,Request $request,DatabaseQueryController $database){
        $this->database = $database;
        $this->request = $request;
        $this->utility = $utility;
    }
    public function create_account(CreateAdminRequest $ad_request){
        //usernane full_name role email package_bundle url_extension
     //   echo "you got here";
        $check = true; 
        $ul = $this->request->all()['ret_ul'];
        $comp_name = $this->request->all()['ret_comp_name'];
        $comp_user = $this->request->all()['ret_comp_user'];
        if(!$this->check_user_datas($this->database,[$this->request->all()['username'],session($ul)['data']->url_extension],['username','url_extension'],'company_users')){
            $check = false;
            $conf['username_error']='Username already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$this->check_user_datas($this->database,[$this->request->all()['email'],session($ul)['data']->url_extension],['email','url_extension'],'company_users')){
            $check = false;
            $conf['email_error']='Email already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$this->check_user_datas($this->database,[$this->request->all()['email'],session($ul)['data']->url_extension],['webmail','url_extension'],'company_registration')){
            $check = false;
            $conf['email_error']='Email already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$check){
            $conf['tab_normal']=true;
            $this->utility->s_flash($this->request,$conf);
            session(['tab_normal'=>false]);
            return redirect()->route('adm',['rdr'=>$this->request->all()['ret_ul']]);
        }
        $password = str_random(7);
        $password_hash = password_hash($password,PASSWORD_BCRYPT);
            $configg = array();
            $configg['table_name']="package_bundle";
            $configg['where']=['url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($ul)['data']->url_extension];
            $select = $this->database->select_data($configg);
            if($select){
                foreach($select as $sel):
                    $config = array();
                    $config = array(
                        [
                    'table_name' => 'company_users',
                'insert_values'=>['username','password','pt_password','full_name','role','email','package_bundle','url_extension'],
                    'query_array'=>[$this->request->all()['username'],$password_hash,$password,$this->request->all()['full_name'],'Administrator',$this->request->all()['email'],session($ul)['data']->package_bundle,session($ul)['data']->url_extension],
                    'query_method'=>'insert'
                        ],
                        [
                            'table_name' => 'package_bundle',
                            'update_values'=>['admin'=>$sel->admin+1],
                            'where'=>['url_hash'],
                            'query_array'=>[session($ul)['data']->url_extension],
                            'query_method'=>'update'
                        ]
                        );
                        event(new DatabaseQueryEvent($config));

                endforeach;
            }
            $array_data = ['name'=>$this->request->all()['full_name'],'company_name'=>$comp_name,'company_name_url'=>$comp_user,'url_hash'=>session($ul)['data']->url_extension,'username'=>$this->request->all()['username'],'password'=>$password,'role'=>'an Administrative','role_url'=>'administrators'];
            $this->utility->send_email($this->request->all()['email'],'em_reg',$array_data);
            $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Admin registration successful </div>
        </div>
    </div>';
    $this->utility->s_flash_empty($this->request,$this->request->all());
    $this->utility->s_flash($this->request,$succ);
    return redirect()->route('adm',['rdr'=>$this->request->all()['ret_ul']]);
    }

    public function create_customer_manager_account(CreateAdminRequest $ad_request){
        //usernane full_name role email package_bundle url_extension
     //   echo "you got here";
        $check = true; 
        $ul = $this->request->all()['ret_ul'];
        $comp_name = $this->request->all()['ret_comp_name'];
        $comp_user = $this->request->all()['ret_comp_user'];
        if(!$this->check_user_datas($this->database,[$this->request->all()['username'],session($ul)['data']->url_extension],['username','url_extension'],'company_users')){
            $check = false;
            $conf['username_error']='Username already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$this->check_user_datas($this->database,[$this->request->all()['email'],session($ul)['data']->url_extension],['email','url_extension'],'company_users')){
            $check = false;
            $conf['email_error']='Email already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$this->check_user_datas($this->database,[$this->request->all()['email'],session($ul)['data']->url_extension],['webmail','url_extension'],'company_registration')){
            $check = false;
            $conf['email_error']='Email already exists';
            $this->utility->s_flash($this->request,$conf);
        }
        if(!$check){
            $conf['tab_normal']=true;
            $this->utility->s_flash($this->request,$conf);
            session(['tab_normal'=>false]);
            return redirect()->route('adm',['rdr'=>$this->request->all()['ret_ul']]);
        }
        $password = str_random(7);
        $password_hash = password_hash($password,PASSWORD_BCRYPT);
            $configg = array();
            $configg['table_name']="package_bundle";
            $configg['where']=['url_hash'];
            $configg['search_columns']="*";
            $configg['limit']=1;
            $configg['query_array']= [session($ul)['data']->url_extension];
            $select = $this->database->select_data($configg);
            if($select){
                foreach($select as $sel):
                    $config = array();
                    $config = array(
                        [
                    'table_name' => 'company_users',
                'insert_values'=>['username','password','pt_password','full_name','role','email','package_bundle','url_extension'],
                    'query_array'=>[$this->request->all()['username'],$password_hash,$password,$this->request->all()['full_name'],'Customer Manager',$this->request->all()['email'],session($ul)['data']->package_bundle,session($ul)['data']->url_extension],
                    'query_method'=>'insert'
                        ],
                        [
                            'table_name' => 'package_bundle',
                            'update_values'=>['customer_manager'=>$sel->customer_manager+1],
                            'where'=>['url_hash'],
                            'query_array'=>[session($ul)['data']->url_extension],
                            'query_method'=>'update'
                        ]
                        );
                        event(new DatabaseQueryEvent($config));

                endforeach;
            }
        /*$config = array(
            [
        'table_name' => 'company_users',
    'insert_values'=>['username','password','pt_password','full_name','role','email','package_bundle','url_extension'],
        'query_array'=>[$this->request->all()['username'],$password_hash,$password,$this->request->all()['full_name'],'Customer Manager',$this->request->all()['email'],session($ul)['data']->package_bundle,session($ul)['data']->url_extension],
        'query_method'=>'insert'
            ]
            );
            event(new DatabaseQueryEvent($config));*/
            $array_data = ['name'=>$this->request->all()['full_name'],'company_name'=>$comp_name,'company_name_url'=>$comp_user,'url_hash'=>session($ul)['data']->url_extension,'username'=>$this->request->all()['username'],'password'=>$password,'role'=>'a Customer Managerial','role_url'=>'customer_managers'];
            $this->utility->send_email($this->request->all()['email'],'em_reg',$array_data);
            $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Customer Manager registration successful </div>
        </div>
    </div>';
    $this->utility->s_flash_empty($this->request,$this->request->all());
    $this->utility->s_flash($this->request,$succ);
    return redirect()->route('admin_customer_man',['rdr'=>$this->request->all()['ret_ul']]);
    }


    private function check_user_data($data,$check_value,$check_column,$table_name){
        $config['query_array'] = [$check_value];
        $config['search_columns'] = "*";
        $config['where']=[$check_column];
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return false;
        }
        return true;
    }
    private function check_user_datas($data,$check_value,$check_column,$table_name){
        $config['query_array'] = $check_value;
        $config['search_columns'] = "*";
        $config['where']=$check_column;
        $config['table_name']=$table_name;
        $config['limit']=1;
        if($data->select_data($config)){
            return false;
        }
        return true;
    }
}
