<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseQueryController;
use App\Http\Controllers\UtilityController;
use App\Http\Requests\CustomerManagerMeetingRequest;
use App\Http\Requests\UpdateMeetingRequest;
use App\Events\DatabaseQueryEvent;

class CustomerManagerMeeting extends Controller
{    
    protected $database;
    protected $utility;
    protected $request;
    public function __construct(DatabaseQueryController $database,UtilityController $utility,Request $request){
        $this->database = $database;
        $this->utility = $utility;
        $this->request = $request;
    }
    public function create_meeting(CustomerManagerMeetingRequest $request){
        //var_dump($this->request->all());
        
        $sub_time = strtotime($this->request->all()['date']);
        if(time() > $sub_time){
        //    echo "invalid";
         //   $config['date_error']="Invalid date. A valid date would be at least 24hours from today";
            $this->utility->s_flash($this->request,$config);
            return redirect()->route('customer_manager_meeting',['rdr'=>$this->request->all()['ret_ul']]);
        }
    $customer = $this->request->all()['customer'];
   //var_dump($customer);
   
    //return;
    $meeting_id = str_random(30);
    $customer_name = explode("**",$customer)[1];
    $customer_username = explode("**",$customer)[0];
    //var_dump($customer_name);
    //var_dump ($customer_username);
    //return;
     $admins =  $this->utility->pull_data($this->database,[session($this->request->all()['ret_ul'])['data']->url_extension,'Administrator'],['url_extension','role'],'company_users');
     $config = array();
     if(!empty($admins)){
         foreach($admins as $admin):
            if($admin->super_admin_flag == 'true'){
                continue;
            }
            $config[]=[
                'table_name'=>'admin_notifications_normal',
                'query_array'=>['A customer manager with username, "'.session($this->request->all()['ret_ul'])['data']->username.'" just scheduled a meeting with a customer named, "'.$customer_name.'"',time(),session($this->request->all()['ret_ul'])['data']->url_extension,$admin->username],
                'insert_values'=>['notification_message','message_time','url_extension','admin'],
                'query_method'=>'insert'
            ];
         endforeach;
     }
     $config[] = [
        'table_name'=>'admin_notifications',
        'query_array'=>['A customer manager with username, "'.session($this->request->all()['ret_ul'])['data']->username.'" just scheduled a meeting with a customer named, "'.$customer_name.'"',time(),session($this->request->all()['ret_ul'])['data']->url_extension],
        'insert_values'=>['notification_message','message_time','url_extension'],
        'query_method'=>'insert'
    ];

    $config[] = [
        'table_name'=>'customer_notifications',
        'query_array'=>['Your customer manager just scheduled a meeting with you. Feel free to honour the invitation or re-schedule for another day',time(),session($this->request->all()['ret_ul'])['data']->url_extension,$customer_username],
        'insert_values'=>['notification_message','message_time','url_extension','customer'],
        'query_method'=>'insert'
    ];
    $config[] = [
        'table_name'=>'meetings',
        'query_array'=>[$this->request->all()['date'],$this->request->all()['venue'],$customer_username,session($this->request->all()['ret_ul'])['data']->username,$this->request->all()['host'],session($this->request->all()['ret_ul'])['data']->url_extension,$meeting_id],
        'insert_values'=>['meeting_date','venue','customer','customer_manager','host','url_extension','meeting_id'],
        'query_method'=>'insert'
    ];

    //var_dump($config);
            //$pull_data = null;
        $this->utility->insert_data_all($config);
        $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Your meeting has been successfully scheduled </div>
        </div>
    </div>';
    $this->utility->s_flash_empty($this->request,$this->request->all());
    $this->utility->s_flash($this->request,$succ);
    return redirect()->route('customer_manager_meeting',['rdr'=>$this->request->all()['ret_ul']]);
        

    }
    public function delete_meeting($meeting_id){
        $check_meeting = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username,$meeting_id],['url_extension','customer_manager','meeting_id'],'meetings',1);
        if($check_meeting){
            foreach($check_meeting as $meet):

                


            $admins =  $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,'Administrator'],['url_extension','role'],'company_users');
     $config = array();
     if(!empty($admins)){
         foreach($admins as $admin):
            if($admin->super_admin_flag == 'true'){
                continue;
            }
            $config[]=[
                'table_name'=>'admin_notifications_normal',
                'query_array'=>['A meeting initially scheduled has been cancelled by the host',time(),session($_GET['rdr'])['data']->url_extension,$admin->username],
                'insert_values'=>['notification_message','message_time','url_extension','admin'],
                'query_method'=>'insert'
            ];
         endforeach;
     }
     $config[] = [
        'table_name'=>'admin_notifications',
        'query_array'=>['A meeting initially scheduled has been cancelled by the host',time(),session($_GET['rdr'])['data']->url_extension],
        'insert_values'=>['notification_message','message_time','url_extension'],
        'query_method'=>'insert'
    ];

    $config[] = [
        'table_name'=>'customer_notifications',
        'query_array'=>['A meeting initially scheduled by your customer manager has been cancelled',time(),session($_GET['rdr'])['data']->url_extension,$meet->customer],
        'insert_values'=>['notification_message','message_time','url_extension','customer'],
        'query_method'=>'insert'
    ];

        $this->utility->insert_data_all($config);
            $config=null;
            $config['delete_value']=['meeting_id'=>$meeting_id];
            $config['table_name']='meetings';
            $this->database->delete_data($config);

            $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Meeting information has been cancelled successfully </div>
        </div>
    </div>';
    $this->utility->s_flash($this->request,$succ);    
    return redirect()->route('customer_manager_meeting',['rdr'=>$_GET['rdr']]);

            endforeach;
            //$config=null;
            
        }
        $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
        <div class="err_div" style="display:block;">
        <div class="err_details text-center featurette-H font-weight-bold"> Oooops!! Looks like you have incorrectly typed a URL. We suggest using the buttons to carry out actions </div>
        </div>
        </div>';
    $this->utility->s_flash($this->request,$succ);    
    return redirect()->route('customer_manager_meeting',['rdr'=>$_GET['rdr']]);
    
    }
    public function update_meeting($meeting_id){



                        $profile_check = false;
                        
                        $display_assets = 'block';


                                    $profile_customer_manager = array();
                                    $check_profile = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,session($_GET['rdr'])['data']->username],['url_extension','username'],'company_customer_managers',1);
                                    if($check_profile):
                                        $profile_check = true;
                                    foreach($check_profile as $profile):
                                        $profile_customer_manager=$profile;
                                    endforeach;
                                    else:
                                    $display_assets = 'none';
                                    return redirect()->route('dash_customer_manager',['rdr'=>$_GET['rdr']]);
                                    //$profilefalse]);
                                    endif;
                            
                            $check_super_notifications = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,'unread',session($_GET['rdr'])['data']->username],['url_extension','read_flag','customer_manager'],'customer_manager_notifications','all'); 
                            //$profile_check = true;
                            $count = 0;
                            if($check_super_notifications){
                                foreach($check_super_notifications as $check):
                                    $super_notifications[$count]['message'] = $check->notification_message;
                                    $super_notifications[$count]['time']=$this->utility->time_break_down($check->message_time)." ago";
                                    $count++;
                                 endforeach;        
                                }else{
                                    $super_notifications = 0;
                                }




        $meeting_data = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,$meeting_id],['url_extension','meeting_id'],'meetings',1);
        //var_dump($meeting_data);
        if(empty($meeting_data))
        return redirect()->route('customer_manager_meeting',['rdr'=>$_GET['rdr']]);
        $meeting_update = array();
        foreach($meeting_data as $meeting):
            $meeting_update['meeting_id']=$meeting->meeting_id;
            $meeting_update['username'] = $meeting->customer;
            $meeting_update['date'] = $meeting->meeting_date;
            $meeting_update['venue'] = $meeting->venue;
            $full_name = $this->utility->pull_data($this->database,[session($_GET['rdr'])['data']->url_extension,'Customer',$meeting->customer],['url_extension','role','username'],'company_users',1);
            foreach($full_name as $name):
                $meeting_update['full_name']=$name->full_name;
            endforeach;
        endforeach;
        return view('meeting_update',['super_notifications'=>$super_notifications,'display_assets'=>$display_assets,'profile_check'=>$profile_check,'profile_customer_manager'=>$profile_customer_manager,'meeting_update'=>$meeting_update]);
    }
    public function update(UpdateMeetingRequest $request){
        if($this->request->all()['date']){
        if(time()>strtotime($this->request->all()['date'])){
            //$config['date_error']="Invalid date. A valid date would be at least 24hours from today";
            //$utility->s_flash($request,$config);  
            return redirect()->route('meeting_update',['meeting_id'=>$this->request->all()['meeting_id'],'rdr'=>$this->request->all()['ret_ul']]);
            //echo "invalid";
        }
    }
        $config[]=[
            'table_name'=>'customer_notifications',
            'query_array'=>['A meeting initially scheduled by your customer manager has been updated. You might want to check it out',time(),session($this->request->all()['ret_ul'])['data']->url_extension,$this->request->all()['customer_username']],
            'insert_values'=>['notification_message','message_time','url_extension','customer'],
            'query_method'=>'insert'
        ];
        $update_values=array();
        if($request->all()['date']){
            $update_values['meeting_date'] = $this->request->all()['date'];
        }
        if($request->all()['venue']){
            $update_values['venue'] = $this->request->all()['venue'];
        }
        //$update_values['venue']=$this->request->all()['venue'];
        if(empty($update_values)){
            $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
            <div class="err_div" style="display:block;">
            <div class="err_details text-center featurette-H font-weight-bold"> You have not submitted any data to update current meeting information </div>
            </div>
            </div>';
            $this->utility->s_flash($this->request,$succ);        
            return redirect()->route('meeting_update',['meeting_id'=>$this->request->all()['meeting_id'],'rdr'=>$this->request->all()['ret_ul']]);
        }
        $config[]=[
            'table_name' => 'meetings',
            'update_values'=>$update_values,
            'where'=>['url_extension','meeting_id'],
            'query_array'=>[session($this->request->all()['ret_ul'])['data']->url_extension,$this->request->all()['meeting_id']],
            'query_method'=>'update'
        ];
        $this->utility->insert_data_all($config);
        $succ['erro']='<div class="col-md-12 d-flex justify-content-center mb-3" style="display:block;">
        <div class="succ_div">
            <div class="succ_details text-center featurette-H font-weight-bold"> Meeting information updated successfully </div>
        </div>
    </div>';
    $this->utility->s_flash_empty($this->request,$this->request->all());
    $this->utility->s_flash($this->request,$succ);
    return redirect()->route('customer_manager_meeting',['rdr'=>$this->request->all()['ret_ul']]);

    }
}
