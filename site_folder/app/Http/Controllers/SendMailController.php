<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestEmail;
use Illuminate\Mail\Mailable;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class SendMailController extends Controller
{
    protected $mail;
    public function __construct(PHPMailer $mail){
        $this->mail = $mail;

    }
    public function send($company_name,$url_hash){
        //Mail::to('imadehidiame@gmail.com')->send(new TestEmail());

        try {
            $this->mail->SMTPDebug = 0;                             
           $this->mail->isSMTP();
           $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true 
            )
        );                       
                  // $company_name = 'leonardo';
                  // $url_hash = 'hjdhjdjkksgysys8ghd';
           $this->mail->Host = 'smtp.gmail.com';                   
           $this->mail->SMTPAuth = true;                           
           $this->mail->Username = env('EM_USE');               
           $this->mail->Password = env('EM_PASS');                
           $this->mail->SMTPSecure = 'tls';                        
           $this->mail->Port = 587;                                
           $this->mail->setFrom(env('EM_USE'), 'KOODIMART');
           $this->mail->addAddress("imadehidiame@gmail.com");
           $this->mail->isHTML(true);                      
           $this->mail->Subject = "Hello there";
           $this->mail->Body = view('em',compact('company_name','url_hash'))->render() ;
            $this->mail->AltBody = "Hi there";
            $this->mail->send();
            echo "sent successfully";
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $this->mail->ErrorInfo;
        }


    }
}
