<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home','Homeo@homie')->name('homie');
Route::get('/show_img','Img@show')->name('im');
Route::get('/dis_img',function(){
    return view('show_img');
});
Route::get('/','Homee@home')->name('home');
Route::post('/ajax_test','AjaxController@test');
Route::get('/ajax_get','AjaxController@get_all'); 
Route::get('/tim',function(){
    return var_dump(localtime(time(),true));
});
Route::get('/session',function(){
    //ini_set('session.gc_maxlifetime',43200);
    //return "Session max life time is ".ini_get('session.gc_maxlifetime');
    return view('dashboard');
});
Route::get('/company/{company_id}/{company_role}/{company_url}','LoginController@login')->name('loginc');
//Route::get('/company/{company_id}/{company_role}/{company_url}','LoginController@login')->name('login_two');
Route::get('emt','EmTest@s');
Route::get('test_too','Too@index');
Route::get('/se/{company_name}/{url_hash}','SeController@send');
Route::get('/logout',function(Request $request){
    if($request->session()->has($_GET['rdr'])){
    $request->session()->forget($_GET['rdr']);    
        return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
    }else{
        return redirect()->route('loginc',['company_id'=>session('comp_id',' '),'company_role'=>session('comp_role',' '),'company_url'=>session('comp_url',' ')]);
    }
    //$request->session()->flush();
    //return redirect()->route('home');
})->name('log');
Route::get('/signup', function () {
    return view('signup'); 
})->name('sign_up');
Route::get('/dashboard','DashboardController@load_dashboardd')->name('dash');
Route::get('/dashboard/profile','DashboardController@load_profilee')->name('prof');
Route::get('/dashboard/company_administrators','DashboardController@load_administratorr')->name('adm');
Route::get('/dashboard/admin_messages','DashboardController@load_messagee')->name('adm_message');
Route::get('/dashboard/admin_invoice','DashboardController@load_invoicee')->name('adm_invoice');
Route::get('/dashboard/admin_meetings','DashboardController@load_meetingg')->name('adm_meeting'); 


Route::get('/dashboard/customer_manager','DashboardCustomerManager@load_dashboardd')->name('dash_customer_manager');
Route::get('/dashboard/customer_manager_customers','DashboardCustomerManager@load_customer')->name('customer_manager_customer');
//Route::get('/dashboard/company_administrators','DashboardController@load_administratorr')->name('adm');
Route::get('/dashboard/customer_manager_messages','DashboardCustomerManager@load_messagee')->name('customer_manager_message');
Route::get('/dashboard/customer_manager_invoice','DashboardCustomerManager@load_invoicee')->name('customer_manager_invoice');
Route::get('/dashboard/customer_manager_meetings','DashboardCustomerManager@load_meeting')->name('customer_manager_meeting');



Route::get('/dashboard/customer','DashboardCustomer@load_dashboardd')->name('dash_customer');
//Route::get('/dashboard/company_administrators','DashboardController@load_administratorr')->name('adm');
Route::get('/dashboard/customer_messages','DashboardCustomer@load_messagee')->name('customer_message');
Route::get('/dashboard/customer_invoice','DashboardCustomer@load_invoice')->name('customer_invoice');
Route::get('/dashboard/customer_meetings','DashboardCustomer@load_meeting')->name('customer_meeting');


//Route::get('/customer_manager_meeting_update','CustomerManagerMeeting@update_meeting')->name('customer_manager_meeting');


//Route::get('/dashboard/customer_manager_meetings','DashboardController@load_meetingg')->name('customer_manager_customer');

//customer_manager/insert_personal_details
Route::post('/customer_manager/insert_personal_details','CustomerManagerProfileUpdate@insert_personal');
Route::post('/customer/insert_personal_details','CustomerProfileUpdate@insert_personal');
Route::post('/customer_manager/update_personal_details','CustomerManagerProfileUpdate@update_personal');
Route::post('/customer/update_personal_details','CustomerProfileUpdate@update_personal');
Route::post('/customer_manager/update_login_details','CustomerManagerProfileUpdate@update_login');
///customer/update_login_details
Route::post('/customer/update_login_details','CustomerProfileUpdate@update_login');
Route::post('/create_meeting','CustomerManagerMeeting@create_meeting');
Route::post('/create_meeting_customer','CustomerMeeting@create_meeting');
Route::get('/customer_manager_meeting_update/{meeting_id}','CustomerManagerMeeting@update_meeting')->name('meeting_update');
Route::get('/customer_meeting_update/{meeting_id}','CustomerMeeting@update_meeting')->name('customer_meeting_update');


Route::get('/customer_manager_meeting_cancel/{meeting_id}','CustomerManagerMeeting@delete_meeting')->name('meeting_cancel');
Route::get('/customer_meeting_accept/{meeting_id}','CustomerMeeting@accept_meeting')->name('meeting_accept');


Route::post('/update_meeting_cm','CustomerManagerMeeting@update');
Route::post('/reschedule_meeting','CustomerMeeting@update');
Route::post('/admin/update_login_details','UpdateLoginController@update_login');
Route::post('/admin/update_personal','UpdateLoginController@update_personal');
Route::post('/create_admin_account','CreateAdminController@create_account');
Route::post('/create_customer_manager_account','CreateAdminController@create_customer_manager_account');
Route::post('/create_customer_account','CreateCustomerController@create_account');
Route::post('/create_admin_account','CreateAdminController@create_account');
Route::post('/create_admin_profile','UpdateLoginController@create_profile');
//admin_customer_man
Route::get('/dashboard/customer_managers','DashboardController@load_customer_managerr')->name('admin_customer_man');
Route::get('/chat',function(){
    return view('chat');
});
Route::post('/login','LoginController@login_company');
Route::get('tea/{age}','TeaController@index')->name('tee');
//Route::redirect('welcome','hello/leonard',301);
Route::get('hello/{name}',function($name){
    if(empty($name))
    return "hello Anonymous";
    return "hello $name";
})->name('hello_check');
Route::get('blade',function(){
    return view('child');
});
Route::post('posttea','TeaController@posting');
Route::view('hi','hello',['name'=>'Leonard']);
Route::prefix('admin')->group(function(){
    Route::get('customer_manager',function(){
        return "Welcome customer Manager";
    });
    Route::get('customer_manager_admin',function(){
        return "Welcome customer Manager Administrator";
    })->name('customer_manager_admin');
});

Route::prefix('signup')->group(function(){
    Route::get('free','SignupCompanyController@free')->name('sign_up');
    Route::get('pro','SignupCompanyController@pro')->name('professional');
    Route::get('enterprise','SignupCompanyController@ent')->name('ent');
});
Route::post('register_company','SignupCompanyController@submit_company_data');
Route::get('send_mail','SendMailController@send');
/*Route::fallback(function(){
    return view('welcome');
});*/

/**
 * NOTES
 * Routes static methods
 * 
 *  
 * Interesting route methods
 * name method for route aliases
 * ---------------------------------
 * Route::get('/hello',function(){
 * })->name('alias')
 * //url=>route('alias')
 * 
 * group method with middleware 
 * --------------------------------------------
 * Route::middleware(['first', 'second'])->group(function () {
    Route::get('/', function () {
        // Uses first & second Middleware
    });

    Route::get('user/profile', function () {
        // Uses first & second Middleware
    });
});
 * 
 * prefix static method
 * -------------------------------------------
 * Route::prefix('admin')->group(function () {
    Route::get('users', function () {
        // Matches The "/admin/users" URL
    });
});
 *  
 * domain static method
 * ----------------------------------------------------------
 * Route::domain('{subdomain}.nardey.com')->group(function(){
 * Route::get('users',function($subdomain,$id){
 * //$subdomain can be used in function or controller
 * });
 * });
 * 
 * name static method
 * -----------------------------------------
 * Route::name('admin.')->group(function(){
 *  Route::get('users',function(){
 * //matches url admin.users
 * })->name('users');
 * });
 * 
 * 
 * 
 * route model binding
 * -------------------------------------------------------
 * Route::get('api/users/{user}',function(App\User $user){
 * //where App\User is an eloquent model
 * //override the default ID checker by overriding the getRouteKey() in your Eloquent model that returns your custom checker
 *  return $user->email;
 * });
 * 
 * route fallback method
 * -------------------------------------
 * Route::fallback(function(){
 * 
 * });
 * 
 */