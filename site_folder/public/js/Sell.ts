///   /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/
import { Validate } from './Validate';
declare var $: any;
let validate = new Validate();
var sell_dirty= false;
var sell_dirty1= false;

var minimum_dirty= false;
var minimum_dirty1= false;


var exchange_dirty= false;
var exchange_dirty1= false;


validate.create_jquery_object("amt").keydown(function(event:any){
    var x = event.which || event.keycode;
    var str = String.fromCharCode(x);
    if(/[0-9A-Za-z]/.test(str) && !(x>=0 && x<=31)){
        sell_dirty = true;
    }
    });
    
    validate.create_jquery_object("amt").keypress(function(event:any){
        var x = event.which || event.keycode;
        var str = String.fromCharCode(x);
        if(/[0-9A-Za-z]/.test(str) && !(x>=0 && x<=31)){
            sell_dirty = true;
    }
    });


validate.create_jquery_object("amt").focusout(function(){
    if(sell_dirty===true){
        sell_dirty1=true;
    }
    if(sell_dirty && sell_dirty1){
        var criteria = [
            {
                fieldname:{
                id:"amt",
                number_check: true,
                number_check_error: "Enter a valid number for Amount",
                housing_element:"number_error",
                validation_rules:{
                    valid_required: true,
                    valid_required_message: "Please fill in the Amount"   
                                }
                        }
            }
         ];
         validate.validateAllFields(criteria);
    }
});


validate.create_javascript_object("amt").addEventListener('paste',function(event:any){
            sell_dirty=sell_dirty1=true;
            var str = $(this).val();
            str = String(str);
            str = str.trim();
            var res = '';
            if(str.startsWith("0")){
                str = str.replace("0",'');
            }
            
            str = str.replace(/[^$,\d]/gi,'');

            $(this).val(str);
            validate.place_comma(str,"amt");

            var criteria = [
                {
                    fieldname:{
                    id:"amt",
                    number_check: true,
                    number_check_error: "Enter a valid number for Amount",
                    housing_element:"number_error",
                    validation_rules:{
                        valid_required: true,
                        valid_required_message: "Please fill in the Amount"   
                                    }
                            }
                }
             ];
             validate.validateAllFields(criteria);

});


validate.create_jquery_object("amt").keyup(function(event){
            var str = $(this).val();
            
            str = String(str);
            str = str.trim();
            var res = '';
            if(str.startsWith("0")){
                str = str.replace("0",'');
            }
            
            str = str.replace(/[^$,\d]/gi,'');
            $(this).val(str);
            validate.place_comma(str,"amt");
            if(sell_dirty&&sell_dirty1){
            var criteria = [
                {
                    fieldname:{
                    id:"amt",
                    number_check: true,
                    number_check_error: "Enter a valid number for Amount",
                    housing_element:"number_error",
                    validation_rules:{
                        valid_required: true,
                        valid_required_message: "Please fill in the Amount"   
                                    }
                            }
                }
             ];

             validate.validateAllFields(criteria);
            }
            //if(/^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test($(this).val()))
            //console.log("it is a valid numbers");
            //else
            //console.log("it is invalids");
    });








    validate.create_jquery_object("exchange").keydown(function(event){
        var x = event.which || event.keycode;
        var str = String.fromCharCode(x);
        if(/[0-9A-Za-z]/.test(str) && !(x>=0 && x<=31)){
            exchange_dirty = true;
        }
        });
        
        validate.create_jquery_object("exchange").keypress(function(event){
            var x = event.which || event.keycode;
            var str = String.fromCharCode(x);
            if(/[0-9A-Za-z]/.test(str) && !(x>=0 && x<=31)){
                exchange_dirty = true;
        }
        });
    
    
    validate.create_jquery_object("exchange").focusout(function(){
        if(exchange_dirty===true){
            exchange_dirty1=true;
        }
        if(exchange_dirty && exchange_dirty1){
            var criteria = [
                {
                    fieldname:{
                    id:"exchange",
                    number_check: true,
                    number_check_error: "Enter a valid number for Exchange Rate",
                    housing_element:"exchange_error",
                    validation_rules:{
                        valid_required: true,
                        valid_required_message: "Please fill in the Exchange Rate value"   
                                    }
                            }
                }
             ];
             validate.validateAllFields(criteria);
        }
    });
    
    
    validate.create_javascript_object("exchange").addEventListener('paste',function(event){
                exchange_dirty=exchange_dirty1=true;
                var str = $(this).val();
                str = String(str);
                str = str.trim();
                var res = '';
                if(str.startsWith("0")){
                    str = str.replace("0",'');
                }
                
                str = str.replace(/[^$,\d]/gi,'');
    
                $(this).val(str);
                validate.place_comma(str,"exchange");
    
                var criteria = [
                    {
                        fieldname:{
                        id:"exchange",
                        number_check: true,
                        number_check_error: "Enter a valid number for Exchange Rate",
                        housing_element:"exchange_error",
                        validation_rules:{
                            valid_required: true,
                            valid_required_message: "Please fill in the Exchange Rate value"   
                                        }
                                }
                    }
                 ];
                 validate.validateAllFields(criteria);
    
    });
    
    
    validate.create_jquery_object("exchange").keyup(function(event){
                var str = $(this).val();
                
                str = String(str);
                str = str.trim();
                var res = '';
                if(str.startsWith("0")){
                    str = str.replace("0",'');
                }
                
                str = str.replace(/[^$,\d]/gi,'');
                $(this).val(str);
                validate.place_comma(str,"exchange");
                if(sell_dirty&&sell_dirty1){
                var criteria = [
                    {
                        fieldname:{
                        id:"exchange",
                        number_check: true,
                        number_check_error: "Enter a valid number for Exchange Rate",
                        housing_element:"exchange_error",
                        validation_rules:{
                            valid_required: true,
                            valid_required_message: "Please fill in the Exchange Rate value"   
                                        }
                                }
                    }
                 ];
    
                 validate.validateAllFields(criteria);
                }
                //if(/^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test($(this).val()))
                //console.log("it is a valid numbers");
                //else
                //console.log("it is invalids");
        });





  

    validate.create_jquery_object("minimum").keydown(function(event){
        var x = event.which || event.keycode;
        var str = String.fromCharCode(x);
        if(/[0-9A-Za-z]/.test(str) && !(x>=0 && x<=31)){
            minimum_dirty = true;
        }
        });
        
        validate.create_jquery_object("minimum").keypress(function(event){
            var x = event.which || event.keycode;
            var str = String.fromCharCode(x);
            if(/[0-9A-Za-z]/.test(str) && !(x>=0 && x<=31)){
                minimum_dirty = true;
        }
        });
    
    
    validate.create_jquery_object("minimum").focusout(function(){
        if(minimum_dirty===true){
            minimum_dirty1=true;
        }
        if(minimum_dirty && minimum_dirty1){
            var criteria = [
                {
                    fieldname:{
                    id:"minimum",
                    number_check: true,
                    number_check_error: "Enter a valid number for minimum sale amount or leave blank if your sale is independent of a minimum sale amount",
                    housing_element:"minumum_error",    
                        less_than_equal_to:"amt",
                        less_than_equal_to_error_message:"This value cannot be greater than the Amount value"            
                            }
                }
             ];
             validate.validateAllFields(criteria);
        }
    });
    
    
    validate.create_javascript_object("minimum").addEventListener('paste',function(event){
                minimum_dirty=minimum_dirty1=true;
                var str = $(this).val();
                str = String(str);
                str = str.trim();
                var res = '';
                if(str.startsWith("0")){
                    str = str.replace("0",'');
                }
                
                str = str.replace(/[^$,\d]/gi,'');
    
                $(this).val(str);
                validate.place_comma(str,"minimum");
    
                var criteria = [
                    {
                        fieldname:{
                        id:"minimum",
                        number_check: true,
                        number_check_error: "Enter a valid number for minimum sale amount or leave blank if your sale is independent of a minimum sale amount",
                        housing_element:"minumum_error",    
                            less_than_equal_to:"amt",
                            less_than_equal_to_error_message:"This value cannot be greater than the Amount value"            
                                }
                    }
                 ];
                 validate.validateAllFields(criteria);
    
    });
    
    
    validate.create_jquery_object("minimum").keyup(function(event){
                var str = $(this).val();
                
                str = String(str);
                str = str.trim();
                var res = '';
                if(str.startsWith("0")){
                    str = str.replace("0",'');
                }
                
                str = str.replace(/[^$,\d]/gi,'');
                $(this).val(str);
                validate.place_comma(str,"minimum");
                if(sell_dirty&&sell_dirty1){
                    var criteria = [
                        {
                            fieldname:{
                            id:"minimum",
                            number_check: true,
                            number_check_error: "Enter a valid number for minimum sale amount or leave blank if your sale is independent of a minimum sale amount",
                            housing_element:"minumum_error",    
                                less_than_equal_to:"amt",
                                less_than_equal_to_error_message:"This value cannot be greater than the Amount value"            
                                    }
                        }
                     ];
                     validate.validateAllFields(criteria);
                }
                //if(/^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test($(this).val()))
                //console.log("it is a valid numbers");
                //else
                //console.log("it is invalids");
        });
      