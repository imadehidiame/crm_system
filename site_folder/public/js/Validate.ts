declare var $: any;
export class Validate{
country_reg_exp: any;    
constructor(){
this.country_reg_exp  = {
    "Nigeria": { 
       reg_exp: [/(^0[7-9][0-1])[0-9]{8}$/,/(^234[7-9][0-1])[0-9]{8}$/,/(^2340[7-9][0-1])[0-9]{8}$/,/(^\+234[7-9][0-1])[0-9]{8}$/,/(^\+2340[7-9][0-1])[0-9]{8}$/],
       id_length:10  
    },
    "Ghana": {
       reg_exp: [/(^2330)[0-9]{9}$/,/(^\+233)[0-9]{9}$/,/(^233)[0-9]{9}$/],
       id_length:12
    }, 
    "India": {
        reg_exp: [/(^\+91)[0-9]{15}$/],
        id_length:15
     },
     "United States of America":{
         reg_exp: [/(^\+1)[0-9]{10}$/],
         id_length:11
     }
   };
}
select_comma(numberValue:any):any{
    var stringValue=String(numberValue);
        var stringLength=stringValue.length;
        var check=0;
        if(stringValue.length>=4){
            switch(stringLength%3){
                case 0:{
 check=3;
                }
                break;
                case 1:{
 check=1;
                }
                break;
                case 2:{
 check=2;
                }
                break;
   default:             
  check=-1;
            }
        }else{
        this.remove_commas(numberValue);  
            
        }
    return check;
}
remove_commas(numb:any):any{
    numb=String(numb);
    var numb1=numb.replace(/,/g,'');
    return numb1;
}
place_first_comma(num:any,pos:any):any{
    var num1=String(num);
    var var1:any=num1.substring(0,pos);
    var var2:any=num1.substring(pos+1,num1.length);
    var var3:any=num1.charAt(pos);
    var3=","+var3;
    var num2=var1+var3+var2; 
    var1=null;var2=null;var3=null;
    return num2;
}

place_comma(numb:any,element?:any){
        var decimalAddition="";
        if(element){
            numb = this.create_jquery_object(element).val();
        }
        numb=String(numb);
        numb=this.remove_commas(numb);
        var lent=numb.length;
        if(numb.search(/\./g)!=-1){
        decimalAddition=numb.substring(numb.search(/\./g),numb.length);
        numb=numb.substring(0,numb.search(/\./g));
        lent=numb.length;
        }else{
     //   decimalAddition=".00";
        }
            
        var pos=this.select_comma(numb);
    
        if(lent>=4){
        while((lent-pos)>=3){
        numb=this.place_first_comma(numb,pos);
        pos+=4;
        lent=numb.length;
    }
}           
            if(element)
            this.create_jquery_object(element).val(numb+decimalAddition);
            return numb+decimalAddition;
}

create_jquery_object(id:string): any{
    return $("#"+id);
}
create_jquery_object1(id:string): any{
    return "#"+id;
}
select_country_code(country_object:any,select_value:any): any{
return country_object[select_value]?country_object[select_value]:'+';
}
create_javascript_object(id:string): any{
return document.getElementById(id);    
}

validateStringLength(stringValue:any,stringLength:any){
    return stringValue.trim().length>=stringLength;
}
validateEmail(stringValue:any){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(stringValue.toLowerCase());    
}

validateAllFields(fieldObject:any,errorDivID?:any){

    /**
     * var o = {
    fieldname:{
    id:"name",
    housing_element:"span",    
    validation_rules:{
        valid_length: 4,
        valid_length_message: "optional",
        valid_email: true,
        valid_email_message: "optional",
        valid_required: true,
        valid_required_message: "optional"   
    },
    mobile{
    country_dependent_field:"country"//$("#country").val(),
    validation_reg: /[a-zA-Z]/,    
    validation_reg_length: 10
    },
    equal_to_id: confirm
    }
};
     * 
     */

    var basicCheck = true;    
    errorDivID=this.create_jquery_object1(errorDivID);   
    //console.log("err div is "+errorDivID);    
    for(var i=0;i<fieldObject.length;i++){
    var basicCheckInLoop = true;        
    var errorText ="<ul>";    
    var jqueryObject = this.create_jquery_object1(fieldObject[i].fieldname.id);
    var housingElement = this.create_jquery_object1(fieldObject[i].fieldname.housing_element);
    //console.log(housingElement);    
        
        
    if(fieldObject[i].fieldname.validation_rules.valid_required && fieldObject[i].fieldname.validation_rules.valid_required === true){
    basicCheckInLoop=basicCheckInLoop&&this.validateStringLength($(jqueryObject).val(),1);
    if(!this.validateStringLength($(jqueryObject).val(),1)){
    if(fieldObject[i].fieldname.validation_rules.valid_required_message && this.validateStringLength(fieldObject[i].fieldname.validation_rules.valid_required_message,1)){
    errorText+="<li>"+fieldObject[i].fieldname.validation_rules.valid_required_message+"</li>";    
    }else{
    errorText+="<li>This field is required and cannot be empty</li>";        
    }    
    }    
    }    
        
    if(fieldObject[i].fieldname.validation_rules.valid_length && fieldObject[i].fieldname.validation_rules.valid_length>=1){
    basicCheckInLoop=basicCheckInLoop&&this.validateStringLength($(jqueryObject).val(),fieldObject[i].fieldname.validation_rules.valid_length);
    if(!this.validateStringLength($(jqueryObject).val(),fieldObject[i].fieldname.validation_rules.valid_length)){
    if(fieldObject[i].fieldname.validation_rules.valid_length_message && this.validateStringLength(fieldObject[i].fieldname.validation_rules.valid_length_message,1)){
    if($(jqueryObject).val())    
    errorText+="<li>"+fieldObject[i].fieldname.validation_rules.valid_length_message+"</li>";    
    }else{
    if($(jqueryObject).val())    
    errorText+="<li>This field requires at least "+fieldObject[i].fieldname.validation_rules.valid_length+" character(s)</li>";        
    }    
    }    
    }
    
    if(fieldObject[i].fieldname.validation_rules.valid_email && fieldObject[i].fieldname.validation_rules.valid_email === true){
    basicCheckInLoop=basicCheckInLoop&&this.validateEmail($(jqueryObject).val());
    if(!this.validateEmail($(jqueryObject).val())){
    if(fieldObject[i].fieldname.validation_rules.valid_email_message && this.validateStringLength(fieldObject[i].fieldname.validation_rules.valid_email_message,1)){
    if($(jqueryObject).val())    
    errorText+="<li>"+fieldObject[i].fieldname.validation_rules.valid_email_message+"</li>";    
    }else{
    if($(jqueryObject).val())    
    errorText+="<li>The email provided is not valid</li>";        
    }    
    }    
    }

    
    /**
     * var o = {
    fieldname:{
    id:"name",
    housing_element:"span",    
    validation_rules:{
        valid_length: 4,
        valid_length_message: "optional",
        valid_email: true,
        valid_email_message: "optional",
        valid_required: true,
        valid_required_message: "optional"   
    },
    mobile{
    country_dependent_field:"country",//$("#country").val()
    mobile_error_message:"Mobile number not valid for chosen country"
    },
    equal_to: confirm
    equal_to_error_message:
    }
};
     * 
     */


    if(fieldObject[i].fieldname.equal_to && fieldObject[i].fieldname.equal_to.trim() !== ''){
        basicCheckInLoop=basicCheckInLoop&&this.validate_equal_to($(jqueryObject).val(),$(this.create_jquery_object1(fieldObject[i].fieldname.equal_to.trim())).val());
        if(!this.validate_equal_to($(jqueryObject).val(),$(this.create_jquery_object1(fieldObject[i].fieldname.equal_to.trim())).val())){
        if(fieldObject[i].fieldname.equal_to_error_message && this.validateStringLength(fieldObject[i].fieldname.equal_to_error_message,1)){
        if($(jqueryObject).val())    
        errorText+="<li>"+fieldObject[i].fieldname.equal_to_error_message+"</li>";    
        }else{
        if($(jqueryObject).val())   
        errorText+="<li>Values do not match</li>";        
        }    
        }    
        }



    if(fieldObject[i].fieldname.mobile && fieldObject[i].fieldname.mobile.country_dependent_field && fieldObject[i].fieldname.mobile.country_dependent_field.trim() !== ''&& fieldObject[i].fieldname.mobile.offset_field && fieldObject[i].fieldname.mobile.offset_field.trim() !== ''){
        basicCheckInLoop=basicCheckInLoop&&this.validate_mobile_number(fieldObject[i].fieldname.mobile.country_dependent_field.trim(),fieldObject[i].fieldname.id,fieldObject[i].fieldname.mobile.offset_field);
        if(!this.validate_mobile_number(fieldObject[i].fieldname.mobile.country_dependent_field.trim(),fieldObject[i].fieldname.id,fieldObject[i].fieldname.mobile.offset_field)){
        if(fieldObject[i].fieldname.mobile.mobile_error_message && this.validateStringLength(fieldObject[i].fieldname.mobile.mobile_error_message,1)){
        if($(jqueryObject).val())    
        errorText+="<li>"+fieldObject[i].fieldname.mobile.mobile_error_message+"</li>";    
        }else{
        if($(jqueryObject).val() != '')    
        errorText+="<li>The mobile number is invalid for the chosen country</li>";        
        }    
        }    
        }

    

        if(fieldObject[i].fieldname.number_check && fieldObject[i].fieldname.number_check == true ){
            basicCheckInLoop=basicCheckInLoop&&this.validate_number(fieldObject[i].fieldname.id);
            if(!this.validate_number(fieldObject[i].fieldname.id)){
            if(fieldObject[i].fieldname.number_check_error && this.validateStringLength(fieldObject[i].fieldname.number_check_error,1)){
            if($(jqueryObject).val())    
            errorText+="<li>"+fieldObject[i].fieldname.number_check_error+"</li>";    
            }else{
            if($(jqueryObject).val() != '')    
            errorText+="<li>Please enter a valid number</li>";        
            }    
            }    
            }


            if(fieldObject[i].fieldname.less_than_equal_to && fieldObject[i].fieldname.less_than_equal_to.trim() !== ''){
                basicCheckInLoop=basicCheckInLoop&&this.validate_less_than_or_equal_to($(jqueryObject).val(),$(this.create_jquery_object1(fieldObject[i].fieldname.less_than_equal_to.trim())).val());
                if(!this.validate_less_than_or_equal_to($(jqueryObject).val(),$(this.create_jquery_object1(fieldObject[i].fieldname.equal_to.trim())).val())){
                if(fieldObject[i].fieldname.less_than_equal_to_error_message && this.validateStringLength(fieldObject[i].fieldname.less_than_equal_to_error_message,1)){
                if($(jqueryObject).val())    
                errorText+="<li>"+fieldObject[i].fieldname.equal_to_error_message+"</li>";    
                }else{
                if($(jqueryObject).val())   
                errorText+="<li>This value must be less than or equal to the '</li>"+fieldObject[i].fieldname.less_than_equal_to+"' field";        
                }    
                }    
                }
        
                
        
    
    errorText+="</ul>";    
    if(!basicCheckInLoop){
    $(jqueryObject).css("border-bottom","1px solid red");    
    $(housingElement).html(errorText);    
    }else{
    $(jqueryObject).css("border-bottom","1px solid green");    
    $(housingElement).html("");        
    }    
    basicCheck=basicCheck&&basicCheckInLoop;    
    }    
    if(!basicCheck){
    if(errorDivID){    
    //$(errorDivID).css("color","red");
    $(errorDivID).html("Please ensure all errors are corrected before form submission");    
    }
    }else{
    if(errorDivID){    
    $(errorDivID).html("");    
    }
    }    
    return basicCheck;    
    }

    validate_mobile_number(country:string,number_id:any,offset_id:any):any{
    /*
    var c = {
     "Nigeria": /^[0-9][0-7]/,
     "United States of America": /[0-9]{10}$/,
     "Ghana": /[0-9]{12}$/,
     "India": /[0-9]{15}$/ 

    }
    */        
    let county = this.create_jquery_object(country);
     if(county.val()==="Nigeria"||county.val()==="United States of America"||county.val()==="India"||county.val()==="Ghana"){
            let number_to_validate = this.create_jquery_object(offset_id).val()+""+this.create_jquery_object(number_id).val();
            var test = false;
            for(var i=0;i<this.country_reg_exp[county.val()].reg_exp.length;i++){
                //console.log(number_to_validate);
                test = test || this.country_reg_exp[county.val()].reg_exp[i].test(number_to_validate);
                //console.log(test);
                if(test !== false){
                    break;
                }
            }    
         /**var test = false;
          * for(var i = 0; i<count; i++){
              test = test || count[i].test(number);
              if(test == true){
                break;
              } 
          }
          */
        //console.log("country is "+county.val());
        //console.log(/(^0[7-9][0-1])[0-9]{8}$/.test(this.create_jquery_object(number_id).val())&&this.create_jquery_object(number_id).val().length===11||(/(^[7-9][0-1])[0-9]{8}$/.test(this.create_jquery_object(number_id).val())&&this.create_jquery_object(number_id).val().length===10)); 
        //console.log(this.create_jquery_object(number_id).val().length===11);   
     return test;    
     }else{
         return true;
     } /*  
     if(this.country_reg_exp[county.val()]){
       return this.country_reg_exp[county.val()].reg_exp.test(this.create_jquery_object(number_id).val())&&this.create_jquery_object(number_id).val().length===this.country_reg_exp[county.val()].id_length;
     }
     return "Validation Reg Exp not found";
     */

    }


    validate_number(number_id:any):boolean{
    return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(this.create_jquery_object(number_id).val());
    }

    validate_equal_to(value1:any,value2:any):boolean{
        return value1.trim().length===value2.trim().length&&value1===value2;
    }
    validate_less_than_or_equal_to(value1:any,value2:any):boolean{
        var value11=String(value1).replace(/,/g,'');
        var value22=String(value2).replace(/,/g,'');
        return value11<=value22;
    }

    create_xml(remote_url:any,eleme?:any):any{
            var req = new XMLHttpRequest();   
            req.open("POST",remote_url);
            req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            req.setRequestHeader("Cache-control","no-cache,must-revalidate");
            req.setRequestHeader("Expires","Sat, 26 Jul 1997 05:00:00 GMT");
            if(eleme){
            req.send(eleme); 
            } 
            return req;
            
    }
    
    
}