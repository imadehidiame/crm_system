@extends('layouts.master')
@section('title', 'NUTSHELL')
@section('body_class', '')
@section('footer_display', 'none')
@section('page_css')
html,
body {
  height: 100%;
}

body {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: center;
  align-items: center;
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #f5f5f5;
}

.form-signin {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: auto;
}
.form-signin .checkbox {
  font-weight: 400;
}
.form-signin .form-control {
  position: relative;
  box-sizing: border-box;
  height: auto;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

                .succ_div{
                    width:auto;
                    float:left;                    
                }
                
                .succ_details{
                    width:auto;
                    background:#5aeeb0;
                    color:#048d54;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }

                .err_div{
                    width:auto;
                    float:left;                    
                }
                
                .err_details{
                    width:auto;
                    background:#ff93a2;
                    color:#ff2a46;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }

@endsection

@section('page')
<form class="form-signin mt-5" action="/login" id="sup" method="POST">

@csrf
      <h1 class="h3 mb-1 font-weight-normal text-center">Enter Login Details</h1>
      <div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                                    <div class="succ_div" style="display:none;">
                                        <div class="succ_details text-center featurette-H font-weight-bold"> Registration Successful. Check your email for Login Details 
                                        </div>
                                    </div>
                                </div>
                        @if($errors->any())
                        
                        @endif
                        
                        {!!session('errr_div','')!!}
                                <div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                                    <div class="err_div" style="display:none;">
                                        <div class="err_details text-center featurette-H font-weight-bold"></div>
                                    </div>
                                </div>
                        
        <div class="form-group">
      <label for="username" class="sr-only">Username</label>
      <input type="text" id="username" name="username" class="form-control" form="sup" placeholder="Username" value="{{session('username','')}}">
      <span class="text-danger">{{$errors->first('username')}}</span>
      </div>
      <div class="form-group">
      <label for="password" class="sr-only">Password</label>
      <input type="password" id="password" name="password" form="sup" class="form-control" placeholder="Password" value="{{session('password','')}}">
      <span class="text-danger">{{$errors->first('password')}}</span>
      </div>
      <div class="form-group" style="margin-bottom:150px;">
      <input type="hidden" name="role" value="{{$company_role}}"> 
      <input type="hidden" name="r" value="{{$company_roles}}">
      <input type="hidden" name="u" value="{{$company_url}}">
      <input type="hidden" name="i" value="{{$company_id}}">
      <button class="btn btn-lg btn-primary btn-block" style="" form="sup" type="submit">Login</button>
        </div>
       
      <div class="row mt-5">
                <div class="col-md-12 mt-5">
                <p class="featurette-heading font-weight-normal float-right"><span data-feather="globe"></span> </p>
                    <p class="featurette-heading text-center font-weight-normal">
                        &copy; <?php echo date("Y") ; ?> NUTSHELL Inc. &middot; 
                        <a href="#" style="text-decoration:none;">Privacy</a> &middot; 
                        <a href="#" style="text-decoration:none;">Terms</a>
                    </p>
                </div>
            </div>
            </form>        
    @endsection
    