@extends('layouts.master')
@section('title', 'NUTSHELL')
@section('body_class', '')
@section('footer_display', 'none')
@section('page_css')
html,
body {
  height: 100%;
}

body {
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: center;
  align-items: center;
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #f5f5f5;
}

.form-signin {
  width: 100%;
  max-width: 330px;
  padding: 15px;
  margin: auto;
}
.form-signin .checkbox {
  font-weight: 400;
}
.form-signin .form-control {
  position: relative;
  box-sizing: border-box;
  height: auto;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

                .succ_div{
                    width:auto;
                    float:left;                    
                }
                
                .succ_details{
                    width:auto;
                    background:#5aeeb0;
                    color:#048d54;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }

                .err_div{
                    width:auto;
                    float:left;                    
                }
                
                .err_details{
                    width:auto;
                    background:#ff93a2;
                    color:#ff2a46;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }

@endsection

@section('page')
<form class="form-signin mt-5" action="/register_company" id="sup" method="POST">

@csrf
      <h1 class="h3 mb-1 font-weight-normal text-center">Register Your Company To Get Started</h1>
      <div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                                    <div class="succ_div" style="display:none;">
                                        <div class="succ_details text-center featurette-H font-weight-bold"> Registration Successful. Check your email for Login Details 
                                        </div>
                                    </div>
                                </div>
                        @if($errors->any())
                        {!!session('err_div','')!!}
                        @endif
                        
                        {!!session('errr_div','')!!}
                                <div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                                    <div class="err_div" style="display:none;">
                                        <div class="err_details text-center featurette-H font-weight-bold"> Oopss! Insuficient Balance, Credit Your Account </div>
                                    </div>
                                </div>
                        
        <div class="form-group">
      <label for="inputEmail" class="sr-only">Company Name</label>
      <input type="text" id="inputEmail" name="company" class="form-control" form="sup" placeholder="Company Name" value="{{session('company','')}}">
      <span class="text-danger">{{$errors->first('company')}}</span>
      </div>
      <div class="form-group">
      <label for="inputEmail" class="sr-only">Registration Number</label>
      <input type="text" id="inputEmail" name="registration" form="sup" class="form-control" placeholder="Registration Number" value="{{session('registration','')}}">
      <span class="text-danger">{{$errors->first('registration')}}</span>
      </div>
      <div class="form-group">
      <label for="inputPassword" class="sr-only">Website</label>
      <input type="text" id="inputPassword" name="website" form="sup" class="form-control" placeholder="Website" value="{{session('website','')}}">
      <span class="text-danger">{{$errors->first('website')}}</span>
      </div>
      <div class="form-group">
      <label for="inputPassword" class="sr-only">Webmail Address</label>
      <input type="email" id="inputPassword" name="webmail" form="sup" class="form-control" placeholder="Web mail address" value="{{session('webmail','')}}">
      <span class="text-danger">{{$errors->first('webmail')}}</span>
      </div>

    <div class="form-group">
      <label for="inputPassword" class="sr-only">Alternate Email Address</label>
      <input type="email" id="inputPassword" name="email" form="sup" class="form-control" placeholder="Alternate email address" value="{{session('email','')}}">
      <span class="text-danger">{{$errors->first('email')}}</span>
      </div>

      <div class="form-group">
      <label for="inputPassword" class="sr-only">average</label>
      <input type="text" name="average" class="form-control" form="sup" placeholder="Average number of Staffs" value="{{session('average','')}}">
      <span class="text-danger">{{$errors->first('average')}}</span>
      </div>
      <div class="form-group">
    <label for="inputPassword" class="sr-only">Head office Phone Number</label>
      <input type="text" name="phone" id="inputPassword" form="sup" class="form-control" placeholder="Head office phone number" value="{{session('phone','')}}">
      <span class="text-danger">{{$errors->first('phone')}}</span>
      <span class="text-danger">{{session('phone_check','')}}</span>
      </div> 
    <div class="form-group">
    <label for="inputPassword" class="sr-only">Package Bundle</label>
      <input type="text" name="p" id="inputPassword" form="sup" class="form-control" placeholder="Package Bundle" value="<?php if(!empty($package)) echo $package; ?>" readonly>
      <span>{{$errors->first('p')}}</span>
      </div>
      <div class="checkbox mb-3" style="display:none;">
        <label>
          <input type="checkbox" name="acc" form="sup" value="yes" {{session('check','')}}> I agree to the <a href="{{route('home')}}">Terms & Conditions</a>
        </label>
        <span class="text-danger">{{$errors->first('acc')}}</span>
      </div>
      <button class="btn btn-lg btn-primary btn-block" form="sup" type="submit">Sign up</button>
      <div class="row mt-5">
                <div class="col-md-12 mt-5">
                <p class="featurette-heading font-weight-normal float-right"><span data-feather="globe"></span> </p>
                    <p class="featurette-heading text-center font-weight-normal">
                        &copy; <?php echo date("Y") ; ?> NUTSHELL Inc. &middot; 
                        <a href="#" style="text-decoration:none;">Privacy</a> &middot; 
                        <a href="#" style="text-decoration:none;">Terms</a>
                    </p>
                </div>
            </div>
            </form>    
    @endsection
    