@extends('layouts.master')

@section('title', 'NUTSHELL')
@section('footer_display', 'block')
@section('page_css')
html {
  font-size: 14px;
}
@media (min-width: 768px) {
  html {
    font-size: 16px;
  }
}

.container {
  max-width: 960px;
}

.pricing-header {
  max-width: 700px;
}

.card-deck .card {
  min-width: 220px;
}

.succ_div{
                    width:auto;
                    float:left;                    
                }
                
                .succ_details{
                    width:auto;
                    background:#5aeeb0;
                    color:#048d54;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }

                .err_div{
                    width:auto;
                    float:left;                    
                }
                
                .err_details{
                    width:auto;
                    background:#ff93a2;
                    color:#ff2a46;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }

@endsection 
@section('page')
    

  <div class="pricing-header px-3 py-3 pt-md-5 mt-5 pb-md-4 mx-auto text-center">
  <input type = "hidden" name='my' id="token" value="{{csrf_token()}}">
  <input type = "hidden" id="token1" value="{{$_COOKIE['XSRF-TOKEN']}}">
      <script>
      //function setR(req){
        //meta = document.getElementsByTagName('meta');
        //console.log(meta[2].content);
        //for(var i =0; i< meta.length; i++){
          //if(meta[i].name == 'X-CSRF-TOKEN' || meta[1].name == 'X-XSRF-TOKEN')
        //  req.setRequestHeader(meta[i].name,meta[i].content);
        //}
        //req.setRequestHeader('X-CSRF-TOKEN',$('meta[name="csrf-token"]').attr('content'));
        //req.setRequestHeader('X-XSRF-TOKEN',$('meta[name="xsrf-token"]').attr('content'));
      //}
      //var head = document.getElementsByTagName('head')[0];
     // var meta = document.createElement('meta');
      //meta.name = 'xsrf-token';
      //meta.content = $_COOKIE['X-XSRF-TOKEN'];
      //var token = document.getElementById('token').value;
     // var token1 = document.getElementById('token1').value;
      //meta.name = 'csrf-token';
      //meta.content = token;
      //var token = {{csrf_token()}};
      //meta.content = {{csrf_token()}};
      //head.appendChild(meta);
     //console.log($('meta[name="csrf-token"]').attr('content'));
     //var post = "name=Leo&id=1";
     //var request = new XMLHttpRequest(); // New request
    //request.open("GET", "ajax_get"); // POST to a server-side script
// Send the message, in plain-text, as the request body
//request.setRequestHeader("X-XSRF-TOKEN", $('meta[name="xsrf-token"]').attr('content'));
//request.setRequestHeader('X-CSRF-TOKEN',$('meta[name="csrf-token"]').attr('content'));
//request.setRequestHeader("X-CSRF-TOKEN", token);
//request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
//setR(request);
//request.open()
//request.send();
//request.onreadystatechange = function(){
  //if (request.readyState === 4 && request.status === 200) {
    //console.log(request.responseText);
    //console.log(request.getAllResponseHeaders());
  //}
  
//  }

      </script>
      {!!session('succ_div','')!!}
      <h1 class="display-4">Pricing</h1>
      <p class="lead">Select a plan that best suits your needs. Take full advantage of our services to deliver productive services to your customers on the go with no hassle.</p>
    </div>

    <div class="container">
      <div class="card-deck mb-3 text-center">
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Free</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$0 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
            <li>1 Administrator</li>
              <li>2 Customer Managers</li>
              <li>4 Customers</li>
              <li>Help center access</li>

            </ul>
            <br><br><br>
            <a href="{{route('sign_up')}}"><button type="button" class="btn btn-lg btn-block btn-outline-primary">Sign up for free</button></a>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Pro</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$15 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>20 Administrators</li>
              <li>100 Customer Managers</li>
              <li>Unlimited Number of Customers</li>
              <li>Help center access</li>
              
            </ul>
            <br><br><br>
            <a href="{{route('professional')}}"><button type="button" class="btn btn-lg btn-block btn-primary">Get started</button></a>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Enterprise</h4>
          </div>
          <div class="card-body">
          @php 
          
          @endphp
            <h1 class="card-title pricing-card-title" style="">$29 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Unlimited Number of Administrators</li>
              <li>Unlimited Number of Customer Managers</li>
              <li>No Limit to Customer Registration</li>
              <li>Help center access</li>
              <li>24/7 Chat Support With Nutshell</li>
            </ul>
            <a href="{{route('ent')}}"><button type="button" class="btn btn-lg btn-block btn-primary">Contact us</button></a>
          </div>
        </div>
      </div>

    
    
    
@endsection
</div>
