@extends('layouts.customer_manager_dashboard')
@section('page_style')
body{
                padding:0;
                margin:0;
                font-size: .875rem;
                -webkit-font-smoothing:antialiased;
                text-rendering: optimizeLegibility;
                
            }


            .feather {
                width: 1rem;
                height: 1rem;        
                vertical-align:text-bottom;
            }

            .BTN{
                border-radius:20px;                
            }

            .BTN_bg{
                border-radius:20px;  
                background:#FFF;
            }

            a{
                text-decoration:none;
                color:currentcolor;
            }

            .INPUT{
                border-top-left-radius:15px;
                border-top-right-radius:15px;
            }


            /*========================================================
            ===================SIDEBAR STARTS HERE ====================*/
            /*
            * Sidebar
            */
                
                #sidebar{
                    margin-top:122px;
                    
                }

            .sidebar {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            z-index: 100; /* Behind the navbar */
            padding: 0;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar-sticky {
            position: -webkit-sticky;
            position: sticky;
            top: 48px; /* Height of navbar */
            height: calc(100vh - 48px);
            padding-top: .5rem;
            overflow-x: hidden;
            overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
            background:#FFF;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar .nav-link {
            font-weight: 500;
            color: #333;
            }

            .sidebar .nav-link .feather {
            margin-right: 4px;
            color: #999;
            }

            .sidebar .nav-link.active {
            color: #007bff;
            }

            .sidebar .nav-link:hover .feather,
            .sidebar .nav-link.active .feather {
            color: inherit;
            }

            .sidebar-heading {
            font-size: .75rem;
            text-transform: uppercase;
            }

            /*
            * Utilities
            */

            .border-top { border-top: 1px solid #e5e5e5; }
            .border-bottom { border-bottom: 1px solid #e5e5e5; }
                
            /*==================SIDEBAR ENDS HERE ========================
            ========================================================*/


            /*========================================================
            ===================MAIN STARTS HERE ====================*/
                #main{
                    margin-top:10.125rem;                      
                    padding-right:130px;               
                }
                
                
                /*
                * Cards
                */
                .card-header{
                    height: 0.625rem;
                    padding: 0px;
                }
                
                .card-body p{
                    font-size: 1.125rem;
                    font-weight: 400;
                    margin: 0px;
                }

            /*==================MAIN ENDS HERE ========================
            ========================================================*/
            
            


            /*========================================================
            =================== STARTS HERE ====================*/
                
            /*================== ENDS HERE ========================
            ========================================================*/
                
                #nav{                    		
                    background:#0ab171;                    
                    color:#FFF;  
                } 
                
                
                #nav a{
                    color: currentColor;
                    text-decoration: none;		
                }
                
                #logo{
                    margin: 0.3rem;
                    /*font-size: 1.3rem;
                    font-weight:300;*/
                    color: #111;
                }
                
                
                #badge{
                    position:relative;
                    font-size:14px;		
                    top:-10px;                    
                    left:-15px;
                    background:#ff526f;
                    color:#FFF;
                    border-radius:1.25rem; 
                    border:1px solid #DDD;                              
                }

                
                #user{		
                    	
                }

                #user img{                
                    width:25px;
                    height:25px;
                    margin-right:10px;                
                }
                

                
                #logout{                    
                    margin-left:0.625rem;
                }                
                
                
                .modal#exampleModal{                           
                    margin-left:635px; 
                    margin-top:30px;                    
                    width:450px;
                }

                .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                    position:absolute;
                    margin-top:-11px;
                    margin-left:390px;
                    color:#FFF;                
                }            

                .notificationDetials{ 
                    float:left;                                  
                    border:1px solid #DDD;               
                    border-radius:10px;
                    margin:5px 10px;
                    padding:5px;
                }

                .notificationDetials .pic{                              
                    padding:5px;
                    float:left;                    
                }
                
                .notificationDetials .message{                                       
                    padding:5px;    
                    width:310px;
                    float:left;                                   
                }

                .mark-read{
                    float:right;
                    margin-right:20px;
                }
                
                
                /*================== NAV ENDS HERE ========================
                ========================================================*/

                .navBottom{
                    margin-top:65px;
                    background:#343a40;
                    color:#FFF;
                    position:fixed;
                    width:102%;   
                    z-index:10;    
                    padding:5px 0px;                   
                }


                .topRow{                    
                    padding:10px 0px ;
                    margin-top:-80px;
                    position:fixed;
                    width:74%; 
                    z-index:10;                   
                }

                .topRow_1stcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .topRow_lastcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .filterBy{
                    padding:0px 50px;
                    margin-top:20px;
                }

                .listHeader{
                    border:1px solid #DDD;                                       
                    padding:10px 10px;
                    margin-top:20px;
                    border-top-left-radius:10px;
                    border-top-right-radius:10px;                                  
                }

                .listHeader .feather{
                    width:20px;
                    height:20px;
                }

                .listBody{
                    border-left:1px solid #DDD;
                    border-right:1px solid #DDD;
                    overflow-y:scroll;
                    height:500px;
                    overflow-x:hidden;                                     
                }

                .listBodyContent{
                    padding:10px 0px;                    
                }
                
                .listBody .row.listBodyContent:nth-child(odd){
                    background:#dffff0;
                }

                .listFooter{
                    border:1px solid #DDD;                    
                    padding:10px 10px;                    
                    border-bottom-left-radius:10px;
                    border-bottom-right-radius:10px;                    
                }

                @keyframes feather{
                    0%{
                       transform:rotate(0deg);
                    }                                   
                    
                    100%{
                        transform:rotate(360deg);
                    }
                }
                
                
                .refresh.feather{                    	
                    animation-name: feather;
                    animation-duration: 5s;
                    animation-iteration-count: infinite;
                    animation-timing-function: linear;		
                    animation-direction: forwards;
                }

                .dpic{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:pink;
                    border-radius:100%;
                }

                .dpic2{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:#8af7ff;
                    border-radius:100%;
                }


        
                

                /* Featurettes
                ------------------------- */

                .featurette-divider {
                margin: 5rem 0; /* Space out the Bootstrap <hr> more */
                }

                /* Thin out the marketing headings */
                .featurette-H {
                font-weight: 300;
                line-height: 1;
                letter-spacing: -.05rem;
                }






                /* ======================= @ MEDIA QUARIES ================================= */
                /* ========================================================================= */
                
                /* Small devices (landscape phones, 576px and up) */
                @media (max-width: 480px ) {
                    
                }
                
                /* Medium devices (tablets, 768px and up)*/
                @media (max-width: 768px ) {	
                    body{
                        overflow-x:hidden;
                    }

                    #nav{			
                        max-height: auto;                        
                    }    

                    .dropdown{	
                        margin-top: 1.25rem;
                        margin-left:1.25rem;
                    }
                    
                    .notify{
                                               	
                    }
                    
                    #user{	
                        margin-top: 10px;						
                        margin-left:1.25rem;		
                    }
                    
                    #logout{				
                        margin-top: 10px;						
                        margin-left:1.25rem;	
                    }

                    .modal#exampleModal{                           
                        margin-left:35px; 
                        margin-top:100px;                    
                        width:450px;
                    }

                    .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                        position:absolute;
                        margin-top:-11px;
                        margin-left:20px;
                        color:#FFF;                
                    }            

                    .notificationDetials{ 
                        float:left;                                  
                        border:1px solid #DDD;               
                        border-radius:10px;
                        margin:5px 10px;
                        padding:5px;
                    }

                    .notificationDetials .pic{                              
                        padding:5px;
                        float:left;                    
                    }
                    
                    .notificationDetials .message{                                       
                        padding:5px;    
                        width:310px;
                        float:left;                                   
                    }

                    .mark-read{
                        float:right;
                        margin-right:20px;
                    }

                    .navBottom{                        
                        width:108%;                                             
                    }

                    .searchDiv{
                        display:none;
                    }

                    .topRow{                    
                        padding:10px 0px ;
                        margin-top:-80px;
                        position:fixed;
                        width:74%;   
                        z-index:10;                 
                    }

                    .topRow_1stcol{
                        background:#FFF;
                        border-radius:5px;
                        padding:5px 20px;                        
                    }

                    .topRow_1stcol h3{
                        font-size:20px;
                    }

                    .topRow_lastcol{
                        display:none;
                    }

                    #main{                                            
                        padding-right:10px;               
                    }
                    
                    .filterBy{
                        padding:0px 0px;
                        margin-top:0px;
                        margin-bottom:50px;                        
                    }                 

                    .listHeader{                        
                        background:#2ac489;
                        border:1px solid #2ac489;
                    }   

                    .listBody{                        
                        height:400px;
                    }

                    .listBodyContentLeft{                        
                        float:left;                           
                        width:47%;  
                        padding-left:5px;                  
                    }

                    .listBodyContentLeft div{                                                
                        margin-top:3px;
                    }

                    .listBodyContentRight{                                               
                        float:right;
                        width:53%; 
                        padding-right:5px; 
                        padding-left:0px; 
                    }                                        

                    .buyListingsLG{
                        display:none;
                    }
                    

                    
                }
                
                /* Large devices (desktops, 992px and up) */
                @media (max-width: 992px ) {
                    
                    
                }
                
                /* Extra large devices (large desktops, 1200px and up) */
                @media (max-width: 1200px ) {	}
                                
   
            
@endsection
@section('page_main')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom topRow">
					
                    <div class="col-md-4 topRow_1stcol">
                        <h3 style="font-family: tahoma;">Invoice</h3>
                    </div>
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4 topRow_lastcol">
                        <div class="btn-toolbar mb-3 float-right">
                            <div class="btn-group mr-2">
                                <button class="btn btn-sm btn-outline-dark">Share</button>
                                <button class="btn btn-sm btn-outline-dark">Export</button>
                            </div>
                            
                            <div class="btn btn-sm btn-outline-dark" id="timebutton">
                            <span data-feather="calendar"></span>
                            <?php echo date("D-m-y"); ?>
                            </div>
                        </div>
                    </div>

				</div>
				
				
				


                    <!-- 
                        ==============================
                        BUY LISTINGS FOR "LARGE" DEVICES
                        ==============================
                                                         -->
                    <div class="col-md-10 offset-md-1 d-none d-md-block buyListingsLG">
                        <div class="row mx-2 listHeader">
                            <div class="col-md-12">
                                <h5 class="featurette-H font-weight-normal float-left"><span data-feather="menu"></span> Customer Invoices</h5>
                                <button class="btn btn-sm bg-transparent px-2 BTN float-right" type="button">
                                    <span data-feather="refresh-cw" class="text-white" style="width:16px; height:16px;"><span>
                                </button>
                            </div>
                            <div class="col-md-3 mt-3"><h6> Username</h6></div>
                            <div class="col-md-2 mt-3"><h6> Invoice ID</h6></div>
                            <div class="col-md-2 mt-3"><h6> Date</h6></div>
                            <div class="col-md-2 mt-3"><h6> Status</h6></div>
                            <div class="col-md-3 mt-3"><h6> Total</h6></div> 
                        </div>

                        <div class="mx-2 listBody">     

                            <form action="placebid.php" method="post" id="placebid"></form>

                            
                            @if(!empty($check_invoice)){
                            @foreach($check_invoice as $invoice)

                            <div class="row px-4 listBodyContent">
                                <div class="col-md-3">
                                    <button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic mr-1">{{substr($invoice->username,0,1)}} </div> {{$invoice->username}} </button>
                                </div>
                                <div class="col-md-2 featurette-H font-weight-normal">{{$invoice->invoice_id}}</div>
                                <div class="col-md-2"><span data-feather="dollar-sign"></span> {{$invoice->datee}}</div>
                                <div class="col-md-2"><span data-feather="dollar-sign"></span> {{$invoice->status}}</div>                                
                                <div class="col-md-3">
                                    <h6 class="float-left ml-3 featurette-H font-weight-normal"><img src="images/naira.png" width="28px" height="28px" alt=""> {{$invoice->total_cost}}</h6>
                                    <button class="btn btn-sm btn-outline-dark px-3 BTN float-right" form="placebid" type="submit">Delete</button>
                                </div>
                            </div>
                            @endforeach;
                            @else
                                <div class="row px-4 listBodyContent text-center">
                            <div class="col-md-12"><h5>No invoice data to display</h5></div>
                            </div>
                            @endif
                            
                            
                            
                        </div>

                        <div class="mx-2 listFooter">
                            <div class="col-md-12">
                                <h6 class="featurette-H">Last Updated : Today</h6>
                            </div>
                        </div>

                    </div>



                    <!-- 
                        ==============================
                        BUY LISTINGS FOR "SMALL" DEVICES
                        ==============================
                                                         -->
                    <div class="col-md-10 offset-md-1 d-sm-block d-md-none d-lg-none buyListingsSM">
                        <div class="row  listHeader">
                            <div class="col-md-12">
                                <h5 class="featurette-H font-weight-normal float-left"><span data-feather="menu"></span> Customer Invoices</h5>
                                <button class="btn btn-sm bg-transparent px-2 BTN float-right" type="button">
                                    <span data-feather="refresh-cw" class="text-white" style="width:16px; height:16px;"><span>
                                </button>
                            </div>
                            
                        </div>

                        <div class="row listBody">  
                            <div class="col-md-12">   

                                <form action="placebid.php" method="post" id="placebid"></form>
                                @if(!empty($check_invoice)){
                            @foreach($check_invoice as $invoice)

                                <div class="row listBodyContent">
                                    <div class="col-md-6 listBodyContentLeft">
                                        <div> Username </div>
                                        <div> Invoice ID </div>
                                        <div> Date </div>
                                        <div> Status</div>
                                        <div> Total</div>
                                    </div>

                                    <div class="col-md-6 text-right listBodyContentRight">
                                        <div>
                                            <button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic mr-1">{{substr($invoice->username,0,1)}} </div> {{$invoice->username}} </button>
                                        </div>
                                        <div ><h6>{{$invoice->invoice_id}}</h6></div>
                                        <div><span data-feather="dollar-sign"></span>{{$invoice->datee}}</div>
                                        <div><span data-feather="dollar-sign"></span> {{$invoice->status}}</div>                                
                                        <div>
                                            <img src="images/naira.png" width="28px" height="28px" alt=""> {{$invoice->total_cost}}<br>
                                            <button class="btn btn-sm btn-outline-dark px-3 my-2 BTN" form="placebid" type="submit">Delete</button>
                                        </div>
                                    </div>
                                </div>
                                
                            @endforeach
                            @else
                                <div class="row listBodyContent">
                                    <div class="col-md-12">
                                    <div class="col-md-12"><h5>No invoice data to display</h5></div>
                                    </div>

                                    
                                </div>

                            @endif

                                
                                                        
                            
                            </div>
                        </div>

                        <div class="row listFooter">
                            <div class="col-md-12">
                                <h6 class="featurette-H">Last Updated : Today</h6>
                            </div>
                        </div>

                    </div>
                           
@endsection