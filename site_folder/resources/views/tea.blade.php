<html>
<h2>Hi {{$name}} Your DOB is {{$age}} and Session age is {{$sess_age}}<h2>
<h4>Full URL is {{$full_url}}</h4>
<h4>Half URL is {{$half_url}}</h4>
<h4>Age 1 is {{$age1}}</h4>
@foreach($users as $user)
<div>
<b>Username: </b>{{$user->username}} 
</div>
@endforeach

<div>
<b>Number of rows affected after query:</b>{{$rows}} 
</div>
<form method="POST" id="sub" action="/posttea">
@csrf
</form>

<p><input type="text" name="age[]" placeholder="Age" value="{{session('age0','')}}" form="sub"></p>
<p><input type="text" name="age[]" placeholder="Age 1" value="{{session('age1','')}}" form="sub"></p>
<p><input type="text" name="age[]" placeholder="Age 2" value="{{session('age2','')}}" form="sub"></p>
<p><input type="text" name="age[]" placeholder="Age 3" value="{{session('age3','')}}" form="sub"></p>
<p><input type="text" name="timo" placeholder="Time" value="{{session('timo')}}" form="sub"></p>
<span style="color:red">{{$errors->first('timo')}}</span>
<p><input type="text" name="timo1" placeholder="Time" value="{{session('timo1')}}" form="sub"></p>
<span style="color:red">{{$errors->first('timo1')}}</span>
<p><input type="text" name="timo2" placeholder="Time" value="{{session('timo2')}}" form="sub"></p>
<span style="color:red">{{$errors->first('timo2')}}</span>
<p><input type="text" name="timo3" placeholder="Timo3" value="{{session('timo3')}}" form="sub"></p>
<span style="color:red">{{$errors->first('timo3')}}</span>
<p><input type="text" name="timo3_confirmation" placeholder="Confirm timo3" value="{{session('timo3_confirmation')}}" form="sub"></p>
<span style="color:red">{{$errors->first('timo3_confirmation')}}</span>
<button type="submit" form="sub">Submit</button>  
@if($errors->any())
<?php //var_dump($errors->all()); var_dump($errors->first('timo')); ?>
<div>
<ul style="color:red;">
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
</div>
@endif
</html>