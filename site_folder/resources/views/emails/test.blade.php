@component('mail::message')
# Introduction
Hi there,
@component('mail::panel')
Thanks for your subscription. Feel free to enjoy all our services
@endcomponent

@component('mail::button', ['url' => 'https://koodimartng.com','color'=>'success'])
Check Out
@endcomponent

Thanks,<br>
NUTSHELL
@endcomponent
