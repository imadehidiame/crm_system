@extends('layouts.customer_manager_dashboard')
@section('page_style')
body{
                padding:0;
                margin:0;
                font-size: .875rem;
                -webkit-font-smoothing:antialiased;
                text-rendering: optimizeLegibility;
                
            }

    .succ_div{
                    width:auto;
                    float:left;                    
                }
                
                .succ_details{
                    width:auto;
                    background:#5aeeb0;
                    color:#048d54;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }

                .err_div{
                    width:auto;
                    float:left;                    
                }
                
                .err_details{
                    width:auto;
                    background:#ff93a2;
                    color:#ff2a46;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }


            .feather {
                width: 1rem;
                height: 1rem;        
                vertical-align:text-bottom;
            }

            .BTN{
                border-radius:20px;                
            }

            .BTN_bg{
                border-radius:20px;  
                background:#FFF;
            }

            a{
                text-decoration:none;
                color:currentcolor;
            }

            a.BTN .feather:hover{                
                color:#000;
            }

            .INPUT{
                border-top-left-radius:15px;
                border-top-right-radius:15px;
            }


            /*========================================================
            ===================SIDEBAR STARTS HERE ====================*/
            /*
            * Sidebar
            */
                
                #sidebar{
                    margin-top:122px;
                    
                }

            .sidebar {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            z-index: 100; /* Behind the navbar */
            padding: 0;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar-sticky {
            position: -webkit-sticky;
            position: sticky;
            top: 48px; /* Height of navbar */
            height: calc(100vh - 48px);
            padding-top: .5rem;
            overflow-x: hidden;
            overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
            background:#FFF;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar .nav-link {
            font-weight: 500;
            color: #333;
            }

            .sidebar .nav-link .feather {
            margin-right: 4px;
            color: #999;
            }

            .sidebar .nav-link.active {
            color: #007bff;
            }

            .sidebar .nav-link:hover .feather,
            .sidebar .nav-link.active .feather {
            color: inherit;
            }

            .sidebar-heading {
            font-size: .75rem;
            text-transform: uppercase;
            }

            /*
            * Utilities
            */

            .border-top { border-top: 1px solid #e5e5e5; }
            .border-bottom { border-bottom: 1px solid #e5e5e5; }
                
            /*==================SIDEBAR ENDS HERE ========================
            ========================================================*/


            /*========================================================
            ===================MAIN STARTS HERE ====================*/
                #main{
                    margin-top:10.125rem;                      
                    padding-right:130px;               
                }
                
                
                /*
                * Cards
                */
                .card-header{
                    height: 0.625rem;
                    padding: 0px;
                }
                
                .card-body p{
                    font-size: 1.125rem;
                    font-weight: 400;
                    margin: 0px;
                }

            /*==================MAIN ENDS HERE ========================
            ========================================================*/
            
            


            /*========================================================
            =================== STARTS HERE ====================*/
                
            /*================== ENDS HERE ========================
            ========================================================*/
                
                #nav{                    		
                    background:#0ab171;                    
                    color:#FFF;  
                } 
                
                
                #nav a{
                    color: currentColor;
                    text-decoration: none;		
                }
                
                #logo{
                    margin: 0.3rem;
                    /*font-size: 1.3rem;
                    font-weight:300;*/
                    color: #111;
                }
                
                
                #badge{
                    position:relative;
                    font-size:14px;		
                    top:-10px;                    
                    left:-15px;
                    background:#ff526f;
                    color:#FFF;
                    border-radius:1.25rem; 
                    border:1px solid #DDD;                              
                }

                
                #user{		
                    margin-left:;		
                }

                #user img{                
                    width:25px;
                    height:25px;
                    margin-right:10px;                
                }
                

                
                #logout{                    
                    margin-left:0.625rem;
                }                
                                          
                
                
                /*================== NAV ENDS HERE ========================
                ========================================================*/

                .modal{                           
                    margin-left:635px; 
                    margin-top:30px;                    
                    width:450px;
                }

                .modal .glyphicon.glyphicon-triangle-top{
                    position:absolute;
                    margin-top:-11px;
                    margin-left:390px;
                    color:#FFF;                
                }            

                .notificationDetials{ 
                    float:left;                                  
                    border:1px solid #DDD;               
                    border-radius:10px;
                    margin:5px 10px;
                    padding:5px;
                }

                .notificationDetials .pic{                              
                    padding:5px;
                    float:left;                    
                }
                
                .notificationDetials .message{                                       
                    padding:5px;    
                    width:310px;
                    float:left;                                   
                }

                .mark-read{
                    float:right;
                    margin-right:20px;
                }

                .navBottom{
                    margin-top:65px;
                    background:#343a40;
                    color:#FFF;          
                    position:fixed;
                    width:102%;
                    z-index:10;    
                    padding: 5px 0px;       
                }


                .topRow{                    
                    padding:10px 0px ;
                    margin-top:-80px;
                    position:fixed;
                    width:74%; 
                    z-index:10;                           
                }

                .topRow_1stcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .topRow_lastcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .tabContainer{
                    margin:50px 20px 20px 20px;
                }                   

                .tab-pane{
                    padding:0px 15px;
                }
                
                .listingsHeadT{
                    background:#2ac489;
                    padding:10px 0px;
                    border-top-left-radius:10px;
                    border-top-right-radius:10px;
                    margin-top:30px;
                }  

                .listingsHeadT .feather{
                    width:20px; 
                    height:20px;             
                }

                .listingsHeadB{
                    background:#2ac489;                    
                    border-bottom:1px solid #DDD;                    
                    padding:10px 0px;
                    font-weight:450;                    
                }

                .listingsBody{
                    overflow-y:scroll;
                    padding:10px 0px;
                    height:350px;
                    border-left:1px solid #DDD;
                    border-bottom:1px solid #DDD;
                    border-right:1px solid #DDD;
                }
            
                .listingDetails{                                        
                    padding:10px 0px;
                }

                .listingsBody .row.listingDetails:nth-child(even){
                    background:#dffff0;
                }

                @keyframes feather{
                    0%{
                       transform:rotate(0deg);
                    }                                   
                    
                    100%{
                        transform:rotate(360deg);
                    }
                }
                
                
                .refresh.feather{                    	
                    animation-name: feather;
                    animation-duration: 5s;
                    animation-iteration-count: infinite;
                    animation-timing-function: linear;		
                    animation-direction: forwards;
                }

                .dropdown-menu{                           
                    margin-right:25px; 
                    margin-top:8px;
                    width:400px; 
                    position:absolute;
                    background:#FFF;                 
                }

                .dropdown-menu.edit{                                                                                      
                    width:120px;   
                    padding-top:0px;                    
                    border-radius:5px;
                    border-color:#343a40;  
                    display:static;
                }

                .editHeader{
                    background:#343a40;  
                    padding:10px;
                    color:#FFF;                    
                }

                .pay{
                    padding:5px 10px;
                    margin-left:10px;
                    margin-top:10px;
                    border:1px solid #2ac489;                    
                    border-top-left-radius:10px;
                    border-top-right-radius:10px;
                    width:100px;
                }

                .counterBid{
                    padding:5px 10px;
                    margin-left:10px; 
                    margin-top:1px;                   
                    border:1px solid #2ac489;                    
                    border-radius:3px;                   
                    width:100px;
                }

                .RejectCounterBid{
                    padding:5px 10px;
                    margin-left:10px;
                    margin-top:10px;
                    border:1px solid #2ac489;                    
                    border-top-left-radius:10px;
                    border-top-right-radius:10px;
                    width:100px;
                }

                .cancel{
                    padding:5px 10px;
                    margin-left:10px; 
                    margin-top:1px;                   
                    border:1px solid #2ac489;                    
                    border-bottom-left-radius:10px;
                    border-bottom-right-radius:10px;             
                    width:100px;
                }

                .dropdown-menu.edit input#TotalUnit, #ExchangeRate, #amount{
                    margin-top:5px;                    
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none; 
                    background:none; 
                    color:#000; 
                    font-size:14px;
                    font-weight: 400;
                    line-height: 1;
                    letter-spacing: -.05rem;
                }

                .input-group-text{
                    border-top:none; 
                    border-left:none; 
                    border-right:none;                  
                    background:none; 
                }
                
                .dpic{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:pink;
                    border-radius:100%;
                }

                .dpic2{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:#8af7ff;
                    border-radius:100%;
                }                





                /* Featurettes
                ------------------------- */

                .featurette-divider {
                margin: 5rem 0; /* Space out the Bootstrap <hr> more */
                }

                /* Thin out the marketing headings */
                .featurette-H {
                font-weight: 300;
                line-height: 1;
                letter-spacing: -.05rem;
                }






                /* ======================= @ MEDIA QUARIES ================================= */
                /* ========================================================================= */
                
                /* Small devices (landscape phones, 576px and up) */
                @media (max-width: 480px ) {
                    
                }
                
                /* Medium devices (tablets, 768px and up)*/
                @media (max-width: 768px ) {	
                    body{
                        overflow-x:hidden;
                    }
                    #nav{			
                        max-height: auto;                        
                    }    

                    .dropdown{	
                        margin-top: 1.25rem;
                        margin-left:1.25rem;
                    }
                    
                    .notify{
                                               	
                    }
                    
                    #user{	
                        margin-top: 10px;						
                        margin-left:1.25rem;		
                    }
                    
                    #logout{				
                        margin-top: 10px;						
                        margin-left:1.25rem;	
                    }

                    .modal#exampleModal{                           
                        margin-left:35px; 
                        margin-top:100px;                    
                        width:450px;
                    }

                    .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                        position:absolute;
                        margin-top:-11px;
                        margin-left:20px;
                        color:#FFF;                
                    }            

                    .notificationDetials{ 
                        float:left;                                  
                        border:1px solid #DDD;               
                        border-radius:10px;
                        margin:5px 10px;
                        padding:5px;
                    }

                    .notificationDetials .pic{                              
                        padding:5px;
                        float:left;                    
                    }
                    
                    .notificationDetials .message{                                       
                        padding:5px;    
                        width:310px;
                        float:left;                                   
                    }

                    .mark-read{
                        float:right;
                        margin-right:20px;
                    }

                    .navBottom{                        
                        width:108%;                                             
                    }

                    .searchDiv{
                        display:none;
                    }

                    .topRow{                    
                        padding:10px 0px ;
                        margin-top:-80px;
                        position:fixed;
                        width:74%;   
                        z-index:10;                 
                    }

                    .topRow_1stcol{
                        background:#FFF;
                        border-radius:5px;
                        padding:5px 20px;                        
                    }

                    .topRow_1stcol h3{
                        font-size:20px;
                    }

                    .topRow_lastcol{
                        display:none;
                    }

                    #main{                                            
                        padding-right:10px;               
                    }

                    .buyListingsLG{
                        display:none;
                    }   
                    
                    .tabContainer{
                        margin:0px;                                               
                    }

                    .tab-pane{
                        padding:0px 0px;
                    }

                    .listingsHeadT{
                        border-left:1px solid #DDD;
                        border-top:1px solid #DDD;
                        border-right:1px solid #DDD;
                        padding:10px 0px;
                        border-top-left-radius:10px;
                        border-top-right-radius:10px;
                        
                    }  

                    .listingsHeadT .feather{
                        width:20px; 
                        height:20px;             
                    }

                    .listingsHeadB{
                        border-left:1px solid #DDD;
                        border-bottom:1px solid #DDD;
                        border-right:1px solid #DDD;
                        padding:10px 0px;
                        font-weight:450;                         
                    }

                    .listingsBody{
                        overflow-y:scroll;
                        overflow-x:hidden;
                        padding:10px 0px;
                        height:350px;
                        border-left:1px solid #DDD;
                        border-bottom:1px solid #DDD;
                        border-right:1px solid #DDD;
                        
                    }
                
                    .listingDetails{                                        
                        padding:10px 0px;
                                              
                    }

                    .listBodyContentLeft{
                        width:47%;
                        float:left;
                        padding-left:10px;
                    }

                    .listBodyContentLeft div{                                                
                        float:left;                        
                    }

                    .listBodyContentRight{
                        width:53%;
                        float:right;
                        padding-right:0px;
                    }

                    .editBody{
                        width:100%;
                    }
                    
                    
                    
                }
                
                /* Large devices (desktops, 992px and up) */
                @media (max-width: 992px ) {
                    
                    
                }
                
                /* Extra large devices (large desktops, 1200px and up) */
                @media (max-width: 1200px ) {	}
                
   
            
@endsection
@section('page_main')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom topRow">
					
                    <div class="col-md-4 topRow_1stcol">
                        <h3 style="font-family: tahoma;">Customers</h3>
                    </div>
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4 topRow_lastcol">
                        <div class="btn-toolbar mb-3 float-right">
                            <div class="btn-group mr-2">
                                <button class="btn btn-sm btn-outline-dark">Share</button>
                                <button class="btn btn-sm btn-outline-dark">Export</button>
                            </div>
                            
                            <div class="btn btn-sm btn-outline-dark" id="timebutton">
                            <span data-feather="calendar"></span>
                            <?php echo date("D-m-y"); ?>
                            </div>
                        </div>
                    </div>

				</div>
				
				
                <div class="row tabContainer">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link {{session('tab_normal')==true?'active':''}} featurette-H" style="font-size:18px;" id="personal-tab" data-toggle="tab" href="#personal" role="tab" aria-controls="personal" aria-selected="true">Customers</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link {{session('tab_normal')==true?'':'active'}} featurette-H" style="font-size:18px;" id="people-tab" data-toggle="tab" href="#people" role="tab" aria-controls="people" aria-selected="false">Add New Customer</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade{{session('tab_normal')==true?' show active':''}}" id="personal" role="tabpanel" aria-labelledby="personal-tab">                                
                                <div class="row">
                                    <!-- 
                                        ==============================
                                        MY BID LISTINGS FOR "LARGE" DEVICES
                                        ==============================
                                                                        -->
                                    <div class="col-md-12 d-none d-md-block buyListingsLG pb-3">
                                        <div class="row listingsHeadT bg-dark text-light">
                                            <div class="col-md-12">
                                                <h5 class="featurette-H font-weight-normal float-left"><span data-feather="menu"></span> Customer List</h5>
                                                <button class="btn btn-sm bg-transparent px-2 BTN float-right" type="button">
                                                    <span data-feather="refresh-cw" class="" style="width:16px; height:16px;"><span>
                                                </button>                                                        
                                            </div>
                                        </div>
                                        <div class="row listingsHeadB bg-dark text-white">
                                            <div class="col-md-4"> Name</div>
                                            <div class="col-md-4">Username</div>
                                            <div class="col-md-4">Status</div>
                                            
                                            <!--<div class="col-md-3 text-center">Status</div>-->
                                        </div>
                                    <!-- username, role, has profile, email, action -->
                                        <div class="row listingsBody">
                                            <div class="col-md-12">
                                                @php 
                                // $d = session('adm_login')->super_admin_flag == "true" ? "block" : "none";
                                                @endphp
                                                @if(!empty($customer_array))
                                                @for($count = 0; $count < count($customer_array); $count++)
                                    
                                                <div class="row listingDetails">
                                                    <div class="col-md-4">
                                                        <button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic mr-1">{{strtoupper(substr($customer_array[$count]['full_name'],0,1))}} </div> 
                                                        {{$customer_array[$count]['full_name']}} </button>
                                                    </div>
                                                    
                                                    <div class="col-md-4"></span> {{$customer_array[$count]['username']}}</div>
                                                    
                                                    <div class="col-md-3 text-center">
                                                        <button class="btn btn-sm btn-{{$customer_array[$count]['profile_created']=='Yes'?'success':'danger'}} px-3 BTN disabled" type="button">{{$customer_array[$count]['active_status']}}</button>
                                                        <div class="dropdown float-right">                                                                    
                                                            <button class="btn btn-sm text-danger bg-transparent px-2 BTN" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <span data-feather="chevron-down"></span>
                                                            </button>
                                                            
                                                            <div class="dropdown-menu edit" aria-labelledby="dropdownMenuButton">
                                                                <div class="editHeader px-2">
                                                                    <h6 class="featurette-H font-weight-normal"> Action</h6>
                                                                </div>                                                                        
                                                                <a class="btn btn-sm btn-outline-dark RejectCounterBid" href="counterbid.php" role="button">Delete User</a>   
                                                                <a class="btn btn-sm btn-outline-dark cancel" href="#" role="button" id="cancel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Cancel
                                                                </a>
                                                                <div class="dropdown-menu" aria-labelledby="cancel">
                                                                    <div class="editBody col-md-10 offset-md-1 pt-3">                                                                                                                            
                                                                        <div class="col-md-12 text-center"> Are You Sure?</div>
                                                                        <div class="row m-3">
                                                                            <div class="col-md-12 ">
                                                                                <button class="btn btn-sm btn-dark BTN float-left mx-3 "><span data-feather="check-circle"></span> Yes</button>
                                                                                <button class="btn btn-sm btn-danger BTN float-right mx-3 "><span data-feather="x-circle"></span> No</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>                                                                       
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            @endfor
                                            @else:
                                            <div class='col-md-12 text-center'><h5 class="featurette-H font-weight-normal">No customers registered under you</h5></div>
                                            @endif
                                            </div>
                                        </div>
                                    </div>

@php 
//var_dump(session('company_users'));
@endphp

                                    <!-- 
                                        ==============================
                                        MY BID LISTINGS FOR "SMALL" DEVICES
                                        ==============================
                                                                        -->
                                    <div class="col-md-12 d-sm-block d-md-none d-lg-none buyListingsSM pb-3">
                                        <div class="row listingsHeadT bg-dark text-light">
                                            <div class="col-md-12">
                                                <h5 class="featurette-H font-weight-normal float-left"><span data-feather="menu"></span> Customers</h5>
                                                <button class="btn btn-sm bg-transparent px-2 BTN float-right" type="button">
                                                    <span data-feather="refresh-cw" class="" style="width:16px; height:16px;"><span>
                                                </button>                                                        
                                            </div>
                                        </div>
                                        <div class="row listingsHeadB bg-dark text-light">                                                    
                                        </div>

                                        <div class="row listingsBody">
                                            <div class="col-md-12">
                                                @if(!empty($customer_array))
                                             @for($count = 0; $count < count($customer_array); $count++)
                                                

                                                <div class="row listingDetails">
                                                    <div class="listBodyContentLeft">
                                                        <div>Name</div>
                                                        <div class="mt-2">Username</div>
                                                        <div>Status</div>
                                                    </div>
                                                    
                                                    <div class="listBodyContentRight text-right">
                                                        <div class="col-md-12"><button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic mr-1">{{strtoupper(substr($customer_array[$count]['full_name'],0,1))}}  </div> {{$customer_array[$count]['full_name']}} </button></div>
                                                        <div class="col-md-12">{{$customer_array[$count]['username']}}</div>
                                                        <div class="col-md-12" style="display:none;">
                                                            <button class="btn btn-sm btn-{{$customer_array[$count]['profile_created']=='Yes'?'success':'danger'}} px-3 BTN disabled" type="button">{{$customer_array[$count]['active_status']}}</button>
                                                            <div class="dropdown">                                                                    
                                                                <button class="btn btn-sm text-danger bg-transparent px-3 BTN" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Action <span data-feather="chevron-down"></span>
                                                                </button>
                                                                
                                                                <div class="dropdown-menu edit" aria-labelledby="dropdownMenuButton">
                                                                    <div class="editHeader px-2">
                                                                        <h6 class="featurette-H font-weight-normal"> Action</h6>
                                                                    </div>      
                                                                    <a class="btn btn-sm btn-outline-dark pay" role="button" href="payforbid.php">Delete</a>
                                                                    <a class="btn btn-sm btn-outline-dark counterBid" href="counterbid.php" role="button">Sus</a>                                                                         
                                                                    <a class="btn btn-sm btn-outline-dark cancel" href="#" role="button" id="cancel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                        Cancel
                                                                    </a>
                                                                    <div class="dropdown-menu" aria-labelledby="cancel">
                                                                        <div class="editBody col-md-10 offset-md-1 pt-3">                                                                                                                            
                                                                            <div class="col-md-12 text-center"> Are You Sure?</div>
                                                                            <div class="row">
                                                                                <div class="col-md-12 ">
                                                                                    <button class="btn btn-sm btn-success BTN float-left"><span data-feather="check-circle"></span> Yes</button>
                                                                                    <button class="btn btn-sm btn-danger BTN float-right"><span data-feather="x-circle"></span> No</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>                                                                                                                                              
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                            
                                                </div>
                                            @endfor
                                            @else:
                                            <div class='col-md-12 text-center'><h5 class="featurette-H font-weight-normal">No customers registered under you</h5></div>
                                            @endif

                                                
                                                
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane fade{{session('tab_normal')==true?'':' show active'}}" id="people" role="tabpanel" aria-labelledby="people-tab">                                
                                <div class="row" style="margin-bottom:100px;">

                                    <div class="col-md-6 offset-md-3">

<div class="row d-flex justify-content-center">
    <div class="text-center TH" ><span data-feather="log-in"></span></div>
</div>
{!! session('erro','') !!}
<div class="col-md-12 d-flex justify-content-center mb-3" style="display:none;">
                        <div class="err_div" style="display:none;">
                            <div class="err_details text-center featurette-H font-weight-bold">  </div>
                        </div>
                    </div>


<form action="/create_customer_account" method="POST" class="form-group">              
    @csrf
    <label for="username" class="featurette-heading font-weight-normal">Username</label>                     
    <div class="input-group input-group-sm mb-3">                                
        <div class="input-group-prepend">
            <span class="input-group-text" style="border-top:none; border-left:none; border-right:none; background:none;"><span data-feather="user"></span></span>
        </div>
        <input type="hidden" name="ret_ul" value="{{session($_GET['rdr'])['hash']}}">
        
        <input type="text" name="username" class="form-control" value="{{session('username','')}}" id="username" style="border-top:none; border-left:none; border-right:none; box-shadow:none;" placeholder="Enter Username">
        
    </div>
    <p style="color:red;font-size:14px;">{{$errors->first('username')}}</p>
    <p style="color:red;font-size:14px;">{{$errors->has('username')?'':session('username_error','')}}</p>
    <label for="username" class="featurette-heading font-weight-normal">Full Name</label>                     
    <div class="input-group input-group-sm mb-3">                                
        <div class="input-group-prepend">
            <span class="input-group-text" style="border-top:none; border-left:none; border-right:none; background:none;"><span data-feather="user"></span></span>
        </div>
        <input type="text" name="full_name" class="form-control" id="username" style="border-top:none; border-left:none; border-right:none; box-shadow:none;" placeholder="Enter Full Name" value="{{session('full_name')}}">
    </div>
    <p style="color:red;font-size:14px;">{{$errors->first('full_name')}}</p>
    <p style="color:red;font-size:14px;">{{$errors->has('full_name')?'':session('full_name_error','')}}</p>
    <input type='hidden' name = "ret_comp_name" value="{{$check_company->name}}">
    <input type='hidden' name = "ret_comp_user" value="{{$check_company->admin_username}}">
    <label for="Password" class="featurette-heading font-weight-normal">Email Address</label>
    <div class="input-group input-group-sm mb-3">                                
        <div class="input-group-prepend">
            <span class="input-group-text" style="border-top:none; border-left:none; border-right:none; background:none;"><span data-feather="user"></span></span>
        </div>
        <input type="text" name="email" class="form-control" id="Password" value="{{session('email','')}}" style="border-top:none; border-left:none; border-right:none; box-shadow:none;" placeholder="Enter Email Address">
    </div>
    <p style="color:red;font-size:14px;">{{$errors->first('email')}}</p>
    <p style="color:red;font-size:14px;">{{$errors->has('email')?'':session('email_error','')}}</p>
    

    <button class="btn btn-sm btn-outline-success BTN float-right" type="submit"> 
        <span data-feather="log-in"></span> Create Account
    </button>
</form>
</div>

                                    <!-- 
                                        =======================================================
                                        OTHER USERS BID ON MY ITEM LISTINGS FOR "LARGE" DEVICES
                                        =======================================================
                                
                                
                                
                                
                                                                                                -->
                                
                                        </div>
                                    </div>



                                    <!-- 
                                        =======================================================
                                        OTHER USERS BID ON MY ITEM LISTINGS FOR "SMALL" DEVICES
                                        =======================================================
                                                                                                -->
                                    

                                                
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                           
@endsection