@extends('layouts.customer_manager_dashboard')
@section('page_style')
body{
                padding:0;
                margin:0;
                font-size: .875rem;
                -webkit-font-smoothing:antialiased;
                text-rendering: optimizeLegibility;
                
            }


            .feather {
                width: 1rem;
                height: 1rem;        
                vertical-align:text-bottom;
            }

            .BTN{
                border-radius:20px;                
            }

            .BTN_bg{
                border-radius:20px;  
                background:#FFF;
            }

            a{
                text-decoration:none;
                color:currentcolor;
            }

            .INPUT{
                border-top-left-radius:15px;
                border-top-right-radius:15px;
            }


            /*========================================================
            ===================SIDEBAR STARTS HERE ====================*/
            /*
            * Sidebar
            */
                
                #sidebar{
                    margin-top:122px;
                    
                }

            .sidebar {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            z-index: 100; /* Behind the navbar */
            padding: 0;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar-sticky {
            position: -webkit-sticky;
            position: sticky;
            top: 48px; /* Height of navbar */
            height: calc(100vh - 48px);
            padding-top: .5rem;
            overflow-x: hidden;
            overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
            background:#FFF;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar .nav-link {
            font-weight: 500;
            color: #333;
            }

            .sidebar .nav-link .feather {
            margin-right: 4px;
            color: #999;
            }

            .sidebar .nav-link.active {
            color: #007bff;
            }

            .sidebar .nav-link:hover .feather,
            .sidebar .nav-link.active .feather {
            color: inherit;
            }

            .sidebar-heading {
            font-size: .75rem; 
            text-transform: uppercase;
            }

            /*
            * Utilities
            */

            .border-top { border-top: 1px solid #e5e5e5; }
            .border-bottom { border-bottom: 1px solid #e5e5e5; }
                
            /*==================SIDEBAR ENDS HERE ========================
            ========================================================*/


            /*========================================================
            ===================MAIN STARTS HERE ====================*/
                #main{
                    margin-top:10.125rem;                      
                    padding-right:130px;               
                }
                
                
                /*
                * Cards
                */
                .card-header{
                    height: 0.625rem;
                    padding: 0px;
                }
                
                .card-body p{
                    font-size: 1.125rem;
                    font-weight: 400;
                    margin: 0px;
                }

            /*==================MAIN ENDS HERE ========================
            ========================================================*/
            
            


            /*========================================================
            =================== STARTS HERE ====================*/
                
            /*================== ENDS HERE ========================
            ========================================================*/
                
                #nav{                    		
                    background:black;                    
                    color:#FFF;  
                } 
                
                
                #nav a{
                    color: currentColor;
                    text-decoration: none;		
                }
                
                #logo{
                    margin: 0.3rem;
                    /*font-size: 1.3rem;
                    font-weight:300;*/
                    color: #111;
                }
                
                
                #badge{
                    position:relative;
                    font-size:14px;		
                    top:-10px;                    
                    left:-15px;
                    background:#ff526f;
                    color:#FFF;
                    border-radius:1.25rem; 
                    border:1px solid #DDD;                              
                }

                
                #user{		
                 		
                }

                #user img{                
                    width:25px;
                    height:25px;
                    margin-right:10px;                
                }
                
                #logout{                    
                    margin-left:0.625rem;
                }  

                .modal#exampleModal{                           
                    margin-left:635px; 
                    margin-top:30px;                    
                    width:450px;
                }

                .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                    position:absolute;
                    margin-top:-11px;
                    margin-left:390px;
                    color:#FFF;                
                }            

                .notificationDetials{ 
                    float:left;                                  
                    border:1px solid #DDD;               
                    border-radius:10px;
                    margin:5px 10px;
                    padding:5px;
                }

                .notificationDetials .pic{                              
                    padding:5px;
                    float:left;                    
                }
                
                .notificationDetials .message{                                       
                    padding:5px;    
                    width:310px;
                    float:left;                                   
                }

                .mark-read{
                    float:right;
                    margin-right:20px;
                }                          
                
                                           
                
                
                /*================== NAV ENDS HERE ========================
                ========================================================*/

                .navBottom{
                    margin-top:65px;
                    background:#343a40;
                    color:#FFF;  
                    position:fixed;
                    width:102%;       
                    z-index:10;  
                    padding:5px 0px;          
                }

                .navBottom input{                    
                    color:#FFF;                     
                }


                .topRow{                    
                    padding:10px 0px ;
                    margin-top:-80px;
                    position:fixed;
                    width:74%;    
                    z-index:10;                
                }

                .topRow_1stcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .topRow_lastcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }


                /*========================================================
                =================== CONVERS_DIV STARTS HERE ====================*/
                                    

                .convers_div{
                    overflow-y:scroll;                    
                    background-size:contain;
                    background-position: center center;
                    background-repeat:no-repeat;
                    height:400px;
                    max-height:400px;
                    margin-top:20px;
                    padding-top:20px;
                    margin-bottom:10px;
                }


                .myMessage{
                    height:auto;                    
                    width:500px;               
                    float:right;                    
                    margin:2px;                    
                }

                .myMessageDetails{
                    max-width:500px; 
                    width:auto;
                    height:auto;                    
                    background:rgba(255, 255, 255, .8);
                    border-top-left-radius:20px;
                    border-top-right-radius:20px;
                    border-bottom-left-radius:20px;
                    border:1px solid #999;
                    padding:10px 10px;                    
                    float:right;
                    font-size:16px;
                }
                
                

                .adminReply{
                    height:auto;
                    width:570px;                    
                    float:left;
                    margin:2px;
                }                


                .adminReplyDetails{
                    max-width:500px;
                    width:auto;
                    height:auto;
                    background:rgba(255, 255, 255, .8);
                    border-radius:20px;
                    border:1px solid #999;
                    padding:10px 10px;
                    float:left;
                    font-size:16px;
                }                
                

                .imgHolder{
                    border: 1px solid #333;
                    border-radius:100%;
                    width:50px;
                    height:50px;
                    float:left;
                    padding:13px;
                    margin-right:10px;
                }

                .imgHolder .feather{
                    width:30px;
                    height:30px;
                }

                @keyframes feather{
                    0%{
                       transform:rotate(0deg);
                    }                                   
                    
                    100%{
                        transform:rotate(360deg);
                    }
                }
                
                
                .refresh.feather{                    	
                    animation-name: feather;
                    animation-duration: 5s;
                    animation-iteration-count: infinite;
                    animation-timing-function: linear;		
                    animation-direction: forwards;
                }


                .dpic{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:pink;
                    border-radius:100%;
                }

                .dpic2{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:#8af7ff;
                    border-radius:100%;
                }
                
                
                 /*================== CONVERS_DIV ENDS HERE ========================
                ========================================================*/
               






                /* Featurettes
                ------------------------- */

                .featurette-divider {
                margin: 5rem 0; /* Space out the Bootstrap <hr> more */
                }

                /* Thin out the marketing headings */
                .featurette-H {
                font-weight: 300;
                line-height: 1;
                letter-spacing: -.05rem;
                }





            /* ======================= @ MEDIA QUARIES ================================= */
            /* ========================================================================= */
                
                /* Small devices (landscape phones, 576px and up) */
                @media (max-width: 480px ) {
                    
                }
                
                /* Medium devices (tablets, 768px and up)*/
                @media (max-width: 768px ) {	
                    #nav{			
                        max-height: auto;                        
                    }    

                    .dropdown{	
                        margin-top: 1.25rem;
                        margin-left:1.25rem;
                    }
                    
                    .notify{
                                               	
                    }
                    
                    #user{	
                        margin-top: 10px;						
                        margin-left:1.25rem;		
                    }
                    
                    #logout{				
                        margin-top: 10px;						
                        margin-left:1.25rem;	
                    }

                    .modal#exampleModal{                           
                        margin-left:35px; 
                        margin-top:100px;                    
                        width:450px;
                    }

                    .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                        position:absolute;
                        margin-top:-11px;
                        margin-left:20px;
                        color:#FFF;                
                    }            

                    .notificationDetials{ 
                        float:left;                                  
                        border:1px solid #DDD;               
                        border-radius:10px;
                        margin:5px 10px;
                        padding:5px;
                    }

                    .notificationDetials .pic{                              
                        padding:5px;
                        float:left;                    
                    }
                    
                    .notificationDetials .message{                                       
                        padding:5px;    
                        width:310px;
                        float:left;                                   
                    }

                    .mark-read{
                        float:right;
                        margin-right:20px;
                    }

                    .navBottom{                        
                        width:108%;                                             
                    }

                    .searchDiv{
                        display:none;
                    }

                    .topRow{                    
                        padding:10px 0px ;
                        margin-top:-80px;
                        position:fixed;
                        width:74%;   
                        z-index:10;                 
                    }

                    .topRow_1stcol{
                        background:#FFF;
                        border-radius:5px;
                        padding:5px 20px;                        
                    }

                    .topRow_1stcol h3{
                        font-size:20px;
                    }

                    .topRow_lastcol{
                        display:none;
                    }

                    #main{                                            
                        padding-right:10px;               
                    }

                    .convers_div{
                        overflow-y:scroll;
                        overflow-x:hidden;                        
                        height:400px;
                        max-height:400px;
                        margin:0px;                        
                        padding:20px 0px;
                        margin-bottom:10px;
                        
                    }
                    .myMessage{
                        height:auto;                    
                        width:245px;               
                        float:right;                    
                        margin:2px;                                        
                    }

                    .myMessageDetails{
                        max-width:245px; 
                        width:auto;
                        height:auto;                    
                        background:rgba(255, 255, 255, .8);
                        border-top-left-radius:20px;
                        border-top-right-radius:20px;
                        border-bottom-left-radius:20px;
                        border:1px solid #999;
                        padding:5px 10px;                    
                        float:right;
                        font-size:16px;
                    }

                    .adminReply{
                        height:auto;
                        width:250px;  
                        float:left;
                        margin:2px;                          
                    }                


                    .adminReplyDetails{
                        max-width:220px;
                        width:auto;
                        height:auto;
                        background:rgba(255, 255, 255, .8);
                        border-radius:20px;
                        border:1px solid #999;
                        padding:6px 10px;
                        float:left;
                        font-size:16px;
                    }     

                    .imgHolder{
                        border: 1px solid #333;
                        border-radius:100%;
                        width:30px;
                        height:30px;
                        float:left;
                        padding:3px 7px;
                        margin-right:5px;
                    }

                    .imgHolder .feather{
                        width:16px;
                        height:16px;
                    }

                    
                    
                    
                }
                
                /* Large devices (desktops, 992px and up) */
                @media (max-width: 992px ) {
                    
                    
                }
                
                /* Extra large devices (large desktops, 1200px and up) */
                @media (max-width: 1200px ) {	}
@endsection
@section('page_main')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom topRow">
					
                    <div class="col-md-4 topRow_1stcol">
                        <h3 style="font-family:tahoma;">Messages</h3>
                    </div>
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4 topRow_lastcol">
                        <div class="btn-toolbar mb-3 float-right">
                            <div class="btn-group mr-2">
                                <button class="btn btn-sm btn-outline-dark">Share</button>
                                <button class="btn btn-sm btn-outline-dark">Export</button>
                            </div>
                            
                            <div class="btn btn-sm btn-outline-dark" id="timebutton">
                            <span data-feather="calendar"></span>
                            <?php echo date("D-m-y"); ?>
                            </div>
                        </div>
                    </div>

				</div>
				
				
				
                    <div class="col-md-10 offset-md-1 convers_div border rounded">
                        <div class="myMessage">
                            <div class="myMessageDetails featurette-H font-weight-normal">
                                Hey Goodday Admin !
                                <br>
                                <input class="btn btn-sm btn-outline-danger px-2 BTN mt-2" type="button" value="Ehidiame|10:56:23 GMT" onclick="document.getElementById('text_user').value = this.value; location.href = '#chats';" style="float:right;"> 
                            </div>                                                        
                        </div>
                        <div class="myMessage">
                            <div class="myMessageDetails featurette-H font-weight-normal">
                            
                                How are you Today. Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus, dignissimos animi aperiam minima quibusdam magnam placeat optio tempore non voluptas reiciendis voluptate? Officiis possimus laboriosam nemo recusandae quibusdam nobis dolorum!
                                <br>
                                <b style="float:right; margin-top:20px">45 seconds ago</b>
                                <br>
                                
                                
                                <br>
                                
                            </div>                                                        
                        </div>
                        <div class="myMessage">
                            <div class="myMessageDetails featurette-H font-weight-normal" style="border: {{'1px solid white;'}}">
                            <input class="btn btn-sm btn-info px-2 BTN mt-2" type="button" value="Ehidiame <==> Customer Manager" onclick="document.getElementById('text_user').value = this.value; location.href = '#chats';" style="float:right; margin-left:5px;"><a><input class="btn btn-sm btn-danger px-2 BTN mt-2" type="button" value="Delete Message" onclick="document.getElementById('text_user').value = this.value; location.href = '#chats';" style="float:right;"></a>  
                            </div>
                            </div>



                            <div class="myMessage">
                            <div class="myMessageDetails featurette-H font-weight-normal">
                            
                                <i>This message was deleted. . . . . . .</i>
                                <br>
                                <b style="float:right; margin-top:10px">45 seconds ago</b>
                                <br>
                                
                                
                                <br>
                                
                            </div>                                                        
                        </div>
                        <div class="myMessage" style="display:;">
                            <div class="myMessageDetails featurette-H font-weight-normal" style="border: {{'1px solid white;'}}">
                            <input class="btn btn-sm btn-info px-2 BTN mt-2" type="button" value="Ehidiame <==> Customer Manager" style="float:right; margin-left:5px;"><a href="{{route('prof')}}"><input class="btn btn-sm btn-danger px-2 BTN mt-2" type="button" value="Delete Message" onclick="document.getElementById('text_user').value = this.value; location.href = '#chats';" style="float:right;"></a>  
                            </div>
                            </div>

                            
                        <div class="myMessage">
                            <div class="myMessageDetails featurette-H font-weight-normal">
                                Are you there i really need your help 
                                <br>
                                <input class="btn btn-sm btn-outline-danger px-2 BTN mt-2" type="button" value="Ehidiame|10:56:23 GMT" onclick="document.getElementById('text_user').value = this.value; location.href = '#chats';" style="float:right;">                     
                            </div>                                    
                            
                        </div>

                        <div class="adminReply">
                            <div class="imgHolder"><span data-feather="user-check"></span></div>
                            <div class="adminReplyDetails featurette-H font-weight-normal">                                Hello there Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro cupiditate, illo autem aspernatur magni dicta rem at suscipit nulla repudiandae sit vitae assumenda nostrum possimus dignissimos fugiat distinctio magnam itaque? Hello there Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis, iure! Sequi quae provident asperiores earum culpa eius! Sit nostrum ut hic facilis blanditiis alias possimus magni aspernatur deserunt, earum necessitatibus!
                            <br>
                                <input class="btn btn-sm btn-outline-danger px-2 BTN mt-2" type="button" value="Ehidiame|10:56:23 GMT" onclick="document.getElementById('text_user').value = this.value; location.href = '#chats';" style="float:right;"> 
                            </div>
                            
                        </div>

                        <div class="adminReply">
                            <div class="imgHolder"><span data-feather="user-check"></span></div>
                            <div class="adminReplyDetails featurette-H font-weight-normal">                                How can i be of service to you
                                <br>
                                <input class="btn btn-sm btn-outline-danger px-2 BTN mt-2" type="button" value="Ehidiame|10:56:23 GMT" onclick="document.getElementById('text_user').value = this.value; location.href = '#chats';" style="float:right;"> 
                            </div>
                        </div>
                    </div>
                        
                    <div class="col-md-10 offset-md-1 mb-5 startComment">
                    <input type="text" name=""  id="text_user" class="form-control mb-3" placeholder="Enter Username">
                        <textarea name="message" id="" class="form-control" placeholder="Enter Message Here"></textarea>
                        <button class="btn btn-sm btn-outline-dark px-3 mt-4 BTN float-right"><span data-feather="send"></span> Send </button>
                    </div>                    
@endsection