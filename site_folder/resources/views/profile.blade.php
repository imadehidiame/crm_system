@extends('layouts.admin_dashboard')
@section('page_style')
body{
                padding:0;
                margin:0;
                font-size: .875rem;
                -webkit-font-smoothing:antialiased;
                text-rendering: optimizeLegibility;
                
            }
                .bl{
                    color:black;
                }
            .succ_div{
                    width:auto;
                    float:left;                    
                }
                
                .succ_details{
                    width:auto;
                    background:#5aeeb0;
                    color:#048d54;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }

                .err_div{
                    width:auto;
                    float:left;                    
                }
                
                .err_details{
                    width:auto;
                    background:#ff93a2;
                    color:#ff2a46;
                    padding:10px 10px;
                    margin:10px;
                    border-radius:5px;                    
                }


            .feather {
                width: 1rem;
                height: 1rem;        
                vertical-align:text-bottom;
            }

            .BTN{
                border-radius:20px;                
            }

            .BTN_bg{
                border-radius:20px;  
                background:#FFF;
            }

            a{
                text-decoration:none;
                color:currentcolor;
            }

            .INPUT{
                border-top-left-radius:15px;
                border-top-right-radius:15px;
            }


            /*========================================================
            ===================SIDEBAR STARTS HERE ====================*/
            /*
            * Sidebar
            */
                
                #sidebar{
                    margin-top:122px;
                    
                }

            .sidebar {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            z-index: 100; /* Behind the navbar */
            padding: 0;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar-sticky {
            position: -webkit-sticky;
            position: sticky;
            top: 48px; /* Height of navbar */
            height: calc(100vh - 48px);
            padding-top: .5rem;
            overflow-x: hidden;
            overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
            background:#FFF;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar .nav-link {
            font-weight: 500;
            color: #333;
            }

            .sidebar .nav-link .feather {
            margin-right: 4px;
            color: #999;
            }

            .sidebar .nav-link.active {
            color: #007bff;
            }

            .sidebar .nav-link:hover .feather,
            .sidebar .nav-link.active .feather {
            color: inherit;
            }

            .sidebar-heading {
            font-size: .75rem;
            text-transform: uppercase;
            }

            /*
            * Utilities
            */

            .border-top { border-top: 1px solid #e5e5e5; }
            .border-bottom { border-bottom: 1px solid #e5e5e5; }
                
            /*==================SIDEBAR ENDS HERE ========================
            ========================================================*/


            /*========================================================
            ===================MAIN STARTS HERE ====================*/
                #main{
                    margin-top:10.125rem;                      
                    padding-right:130px;               
                }
                
                
                /*
                * Cards
                */
                .card-header{
                    height: 0.625rem;
                    padding: 0px;
                }
                
                .card-body p{
                    font-size: 1.125rem;
                    font-weight: 400;
                    margin: 0px;
                }

            /*==================MAIN ENDS HERE ========================
            ========================================================*/
            
            


            /*========================================================
            =================== STARTS HERE ====================*/
                
            /*================== ENDS HERE ========================
            ========================================================*/
                
                #nav{                    		
                    background:black;                    
                    color:#FFF;  
                } 
                
                
                #nav a{
                    color: currentColor;
                    text-decoration: none;		
                }
                
                #logo{
                    margin: 0.3rem;
                    /*font-size: 1.3rem;
                    font-weight:300;*/
                    color: #111;
                }
                
                
                #badge{
                    position:relative;
                    font-size:14px;		
                    top:-10px;                    
                    left:-15px;
                    background:#ff526f;
                    color:#FFF;
                    border-radius:1.25rem; 
                    border:1px solid #DDD;                              
                }

                
                #user{		
                    		
                }

                #user img{                
                    width:25px;
                    height:25px;
                    margin-right:10px;                
                }
                

                
                #logout{                    
                    margin-left:0.625rem;
                }                
                
                .modal#exampleModal{                           
                    margin-left:635px; 
                    margin-top:30px;                    
                    width:450px;
                }

                .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                    position:absolute;
                    margin-top:-11px;
                    margin-left:390px;
                    color:#FFF;                
                }            

                .notificationDetials{ 
                    float:left;                                  
                    border:1px solid #DDD;               
                    border-radius:10px;
                    margin:5px 10px;
                    padding:5px;
                }

                .notificationDetials .pic{                              
                    padding:5px;
                    float:left;                    
                }
                
                .notificationDetials .message{                                       
                    padding:5px;    
                    width:310px;
                    float:left;                                   
                }

                .mark-read{
                    float:right;
                    margin-right:20px;
                }
                
                
                
                
                /*================== NAV ENDS HERE ========================
                ========================================================*/

                .navBottom{
                    margin-top:65px;
                    background:#343a40;
                    color:#FFF;  
                    position:fixed;
                    width:102%;   
                    z-index:10;     
                    padding:5px 0px;                   
                }

                .navBottom input{                    
                    color:#FFF;                     
                }


                .topRow{                    
                    padding:10px 0px ;
                    margin-top:-80px;
                    position:fixed;
                    width:74%;  
                    z-index:10;                  
                }

                .topRow_1stcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .topRow_lastcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .curr_conv_div{
                    position:relative;
                    height:400px;                                
                    box-shadow: -3px 5px 10px #bbb;
                    border-radius:10px;
                    overflow:hidden;  
                    margin-top:50px;
                }

                .curr_conv_div input#file{
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none; 
                    background:none; 
                    color:#000; 
                    font-size:12px;
                    font-weight: 400;
                    line-height: 1;
                    letter-spacing: -.05rem;
                }
                
                .curr_conv_div input#text, #email{
                    margin-top:5px;
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none; 
                    background:none; 
                    color:#000; 
                    font-size:14px;
                    font-weight: 400;
                    line-height: 1;
                    letter-spacing: -.05rem;

                }

                .curr_conv_img{
                    height:100px;           
                    position:absolute;                
                    left:0;
                    bottom:0;                
                    width:100%;                                               
                    background:url(images/bg.png);                
                    background-size:cover;
                    background-repeat:no-repeat;                
                }

                .imgHolder{
                    border: 3px dotted #999;
                    border-radius:100%;
                    width:70px;
                    height:70px;
                    float:left;
                    padding:2px;
                }

                .imgHolder img{
                    width:60px;
                    height:60px;
                }               
                
                @keyframes feather{
                    0%{
                       transform:rotate(0deg);
                    }                                   
                    
                    100%{
                        transform:rotate(360deg);
                    }
                }
                
                
                .refresh.feather{                    	
                    animation-name: feather;
                    animation-duration: 5s;
                    animation-iteration-count: infinite;
                    animation-timing-function: linear;		
                    animation-direction: forwards;
                }




                /*========================================================
                =================== PROFILE CARD STARTS HERE ====================*/

                .card{
                    border-color:#343a40;
                    margin-top:50px;
                }

                .card .card-header{
                    border-color:#343a40;
                    background:#343a40;
                    padding:10px 0px 30px 10px  ;
                    color:#FFF;
                    font-size:18px;
                    font-weight: 500;
                    line-height: 1;
                    letter-spacing: -.05rem;
                }

                .card-body input#username, #password, #confirmPassword, #country{
                    margin-top:5px;
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none; 
                    background:none; 
                    color:#000; 
                    font-size:14px;
                    font-weight: 400;
                    line-height: 1;
                    letter-spacing: -.05rem;
                }

                .input-group-text{
                    border-top:none; 
                    border-left:none; 
                    border-right:none;                  
                    background:none; 
                }

                .dpic{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:pink;
                    border-radius:100%;
                }

                .dpic2{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:#8af7ff;
                    border-radius:100%;
                }

                    
                /*================== PROFILE CARD ENDS HERE ========================
                ========================================================*/

        
                

                /* Featurettes
                ------------------------- */

                .featurette-divider {
                margin: 5rem 0; /* Space out the Bootstrap <hr> more */
                }

                /* Thin out the marketing headings */
                .featurette-H {
                font-weight: 300;
                line-height: 1;
                letter-spacing: -.05rem;
                }






            /* ======================= @ MEDIA QUARIES ================================= */
            /* ========================================================================= */
                
                /* Small devices (landscape phones, 576px and up) */
                @media (max-width: 480px ) {
                    
                }
                
                /* Medium devices (tablets, 768px and up)*/
                @media (max-width: 768px ) {	
                    #nav{			
                        max-height: auto;                        
                    }    

                    .dropdown{	
                        margin-top: 1.25rem;
                        margin-left:1.25rem;
                    }
                    
                    .notify{
                                               	
                    }
                    
                    #user{	
                        margin-top: 10px;						
                        margin-left:1.25rem;		
                    }
                    
                    #logout{				
                        margin-top: 10px;						
                        margin-left:1.25rem;	
                    }

                    .modal#exampleModal{                           
                        margin-left:35px; 
                        margin-top:100px;                    
                        width:450px;
                    }

                    .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                        position:absolute;
                        margin-top:-11px;
                        margin-left:20px;
                        color:#FFF;                
                    }            

                    .notificationDetials{ 
                        float:left;                                  
                        border:1px solid #DDD;               
                        border-radius:10px;
                        margin:5px 10px;
                        padding:5px;
                    }

                    .notificationDetials .pic{                              
                        padding:5px;
                        float:left;                    
                    }
                    
                    .notificationDetials .message{                                       
                        padding:5px;    
                        width:310px;
                        float:left;                                   
                    }

                    .mark-read{
                        float:right;
                        margin-right:20px;
                    }

                    .navBottom{                        
                        width:108%;                        
                    }

                    .searchDiv{
                        display:none;
                    }

                    .topRow{                    
                        padding:10px 0px ;
                        margin-top:-80px;
                        position:fixed;
                        width:74%;   
                        z-index:10;                 
                    }

                    .topRow_1stcol{
                        background:#FFF;
                        border-radius:5px;
                        padding:5px 20px;                        
                    }

                    .topRow_1stcol h3{
                        font-size:20px;                        
                    }

                    .topRow_lastcol{
                        display:none;
                    }

                    #main{                                            
                        padding-right:10px;               
                    }


                    .card{
                        margin-top:0px;
                    }

                    .curr_conv_div{
                        position:relative;
                        height:400px; 
                        box-shadow: -1px 1px 10px #bbb;
                        border-radius:10px;
                        overflow:hidden;  
                        margin-top:50px;                        
                    }

                    
                    
                }
                
                /* Large devices (desktops, 992px and up) */
                @media (max-width: 992px ) {
                    
                    
                }
                
                /* Extra large devices (large desktops, 1200px and up) */
                @media (max-width: 1200px ) {	}
            
@endsection
@section('page_main')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom topRow">

                @php
                /*$notification_number = 0;
                @if(is_int(session('notifications_adm'))==false):
                    @foreach(session('notifications_adm') as $note):
                    $notification_number+=1;
                    @endforeach
                @endif */
                $sup_ad = "";
                $displayss = 'block';
                $super_admin = "";
                $mobile = "";
                $em = "";
                $gender = "";
                $address = "";
                $display_fields = "";
                $state = "";
                $country = "";
                $reg_number = "";
                $reg_display = "";
                $web_em = "";
                $webm_display = "";
                $staffs = "";
                $staff_display = "";
                $package = "";
                $package_display = "";
                $sub = "";
                $sub_display = "";
                $button_label = "Update";
                $user = "";
                $upd_url = "";
                if($super_admin_check != true && $profile_check == false ){
                    $displayss = "none";
                    $upd_url = "/create_admin_profile";
                    $super_admin = session($_GET['rdr'])['data']->username;
                    $sup_ad = "";
                    $mobile = "";
                    $em = session($_GET['rdr'])['data']->email;
                    $gender = "";
                    $button_label = "Create Profile";
                    $user = session($_GET['rdr'])['data']->username;
                    $address = "";
                    $country = "";
                    $reg_display = "none";
                    $webm_display = "none";
                    $staff_display = "none";
                    $sub_display = "none";
                    $package_display = "none";
                   // $display_fields ="block";
                }else if($super_admin_check != true && $profile_check == true ){
                    $displayss = "none";
                    $upd_url = "/admin/update_personal";
                    $super_admin = session($_GET['rdr'])['data']->username;
                    $sup_ad = "";
                    $mobile = "";
                    $em = session($_GET['rdr'])['data']->email;
                    $gender = "";
                    $button_label = "Update Profile";
                    $user = session($_GET['rdr'])['data']->username;
                    $address = "";
                    $country = "";
                    $reg_display = "none";
                    $webm_display = "none";
                    $staff_display = "none";
                    $sub_display = "none";
                    $package_display = "none";
                   // $display_fields ="block";
                   //session($_GET['rdr'])['hash'];
                   $mobile = $profile_adm->phone_number;
                    $em = session($_GET['rdr'])['data']->email;
                    //$gender = $profile_adm->gender;
                    $address = $profile_adm->address;
                    $country = $profile_adm->country;
                    $state = $profile_adm->state_of_residence;
                }
                else if($super_admin_check == true){
                    $super_admin = ucwords(str_replace('_',' ', session($_GET['rdr'])['data']->username));
                    $sup_ad = $super_admin;
                    $upd_url = "/admin/update_personal";
                    $mobile = $company_registration->phone_number;
                    $em = $company_registration->email;
                    $reg_number = $company_registration->registration_number;
                    $gender = "";
                    $address = "";
                    //$country = ""
                    $display_fields = "none";
                    $web_em =  $company_registration->webmail;
                    $staffs =  $company_registration->number_of_staffs;
                    $sub =  $company_registration->subscription_status;
                    $package =  $company_registration->package_bundle;
                    $user = session($_GET['rdr'])['data']->username;
                }else if(session('profile_check') == true){
                    $upd_url = '/update_admin_profile';
                    $super_admin = $profile_adm->full_name;
                    $sup_ad = $super_admin;
                    $mobile = $profile_adm->mobile_number;
                    $em = $profile_adm->email;
                    $gender = $profile_adm->gender;
                    $address = $profile_adm->address;
                    $country = $profile_adm->country;
                    $state = $profile_adm->state_of_residence;
                    //$display_fields = "block";
                    $reg_display = "none";
                    $webm_display = "none";
                    $staff_display = "none";
                    $package_display = "none";
                    $sub_display = "none";
                    $button_label = "Update";
                    $user = session($_GET['rdr'])['data']->username;
                }
                @endphp
					
                    <div class="col-md-4 topRow_1stcol">
                        <h3 class="" style="font-family: tahoma;">Profile</h3>
                    </div>
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4 topRow_lastcol">
                        <div class="btn-toolbar mb-3 float-right">
                            <div class="btn-group mr-2">
                                <button class="btn btn-sm btn-outline-dark">Share</button>
                                <button class="btn btn-sm btn-outline-dark">Export</button>
                            </div>
                            
                            <div class="btn btn-sm btn-outline-dark" id="timebutton">
                            <span data-feather="calendar"></span>
                            <?php echo date("D-m-y"); ?>
                            </div>
                        </div>
                    </div>

				</div>
				
                
                
				
                <form action = '{{$upd_url}}' id="update_personal" method="post">
                                    @csrf
				<div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Personal Details</div>
                            {!!session('erro_div','')!!}
                            {!!session('erro','')!!}
                            <div class="card-body">
                                <div class="row">
                                                
                                    <div class="col-md-6 offset-md-3 pb-5">
                                        <label for="username">Full Name</label>
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="user"></span></span>
                                            </div>
                                            <input type="text" name="full_name" id="username" class="form-control" value="{{session($_GET['rdr'])['data']->full_name}}" placeholder="Full Name" disabled>
                                        </div>


                                    
                                        

                                        <label for="mobile">Phone Number</label>
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="smartphone"></span></span>
                                            </div>
                                            <input type="text" name="phone_number" id="username" class="form-control" value="{{session('phone_number','')}}" placeholder="{{empty($mobile)?'Phone Number':$mobile}}"  form="update_personal">
                                        
                                        <input type="hidden" name="ret_ul" value="{{session($_GET['rdr'])['hash']}}">
                                        </div>
                                        <p style="color:red;font-size:14px;">{{$errors->first('phone_number')}}</p>    
                                        <p style="color:red;font-size:14px;">{{$errors->has('phone_number')?'':session('phone_number_error','')}}</p>    
                                        <label for="address" style="color: black;display:{{$display_fields}}">Residential Address</label>
                                        <div class="input-group input-group-sm" style="display:{{$display_fields}}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="map-pin"></span></span>
                                            </div>
                                            <input type="text" name="address" id="username" class="form-control" value="{{session('address','')}}" placeholder="{{empty($address)?'Residential Address':$address}}"  form="update_personal">
                                        </div>
                                        <p style="color:red;font-size:14px;">{{$errors->first('address')}}</p>     
                                        

                                        <label for="address" style="display:{{$display_fields}}">Country</label>
                                        <div class="input-group input-group-sm" style="display:{{$display_fields}}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="map-pin"></span></span>
                                            </div>
                                            <input type="text" name="country" id="username" class="form-control" value="{{session('country','')}}" placeholder="{{empty($country)?'Country of residence':$country}}"  form="update_personal">
                                        </div>
                                        <p style="color:red;font-size:14px;">{{$errors->first('country')}}</p>

                                        <label for="address" style="display:{{$display_fields}}">State</label>
                                        <div class="input-group input-group-sm" style="display:{{$display_fields}}">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="map-pin"></span></span>
                                            </div>
                                            <input type="text" name="state_of_residence" id="username" class="form-control" value="{{session('state_of_residence')}}" placeholder="{{empty($state)?'State of residence':$state}}"  form="update_personal">
                                        </div>
                                        <p style="color:red;font-size:14px;">{{$errors->first('state_of_residence')}}</p>

                                        <label for="email" style="color:black;" class="featurette-H font-weight-normal">Email Address</label>
                                        <div class="input-group input-group-sm mb-3">                                        
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="mail"></span></span>
                                            </div>
                                            <input type="text" name="email" id="email" class="form-control" value="{{session('email','')}}" placeholder="{{empty($em)?'Email Address':$em}}" form="update_personal"> 
                                        </div>
                                        <p style="color:red;font-size:14px;">{{$errors->first('email')}}</p>

                                        <label for="webmail" class="featurette-H font-weight-normal bl" style="color:black;display:{{$webm_display}}">Webmail Address</label>
                                        <div class="input-group input-group-sm mb-3" style="display:{{$webm_display}}">                                        
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="mail"></span></span>
                                            </div>
                                            <input type="text" name="webmail" id="email" class="form-control" value="{{session('webmail','')}}" placeholder="{{empty($web_em)?'Webmail Address':$web_em}}" form="update_personal">
                                        </div>
                                        <p style="color:red;font-size:14px;">{{$errors->first('webmail')}}</p>

                                        <label for="email" class="featurette-H font-weight-normal bl" style="color:black;display:{{$reg_display}}">Company Registration Number</label>
                                        <div class="input-group input-group-sm mb-3" style="display:{{$reg_display}}">                                        
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="mail"></span></span>
                                            </div>
                                            <input type="text" name="registration_number" id="email" class="form-control" value="{{session('registration_number')}}" placeholder="{{empty($reg_number)?'Registration Number':$reg_number}}" form="update_personal">
                                        </div>
                                        <p style="color:red;font-size:14px;">{{$errors->first('registration_number')}}</p>

                                        <label for="email" class="featurette-H font-weight-normal bl" style="display:{{$staff_display}}">Average Number of Staffs</label>
                                        <div class="input-group input-group-sm mb-3" style="display:{{$staff_display}}">                                        
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="mail"></span></span>
                                            </div>
                                            <input type="text" name="number_of_staffs" id="email" class="form-control" value="{{session('number_of_staffs')}}" placeholder="{{empty($staffs)?'Number of staffs':$staffs}}" form="update_personal">
                                        </div>
                                        <p style="color:red;font-size:14px;">{{$errors->first('number_of_staffs')}}</p>


                                        <label for="email" class="featurette-H font-weight-normal bl" style="display:{{$package_display}}">Package Bundle</label>
                                        <div class="input-group input-group-sm mb-3" style="display:{{$package_display}}">                                        
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="mail"></span></span>
                                            </div>
                                            <input type="text" name="package" id="email" class="form-control" value="{{$package}}" disabled>
                                        </div>

                                        <label for="email" class="featurette-H font-weight-normal bl" style="display:{{$sub_display}}">Subscription Status</label>
                                        <div class="input-group input-group-sm mb-3" style="display:{{$sub_display}}">                                        
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="mail"></span></span>
                                            </div>
                                            <input type="text" name="registration" id="email" class="form-control" value="{{$sub}}" disabled>
                                        </div>

                                        <button type="submit" class="btn btn-sm btn-dark BTN float-right"><span data-feather="send" form="update_personal"></span> {{$button_label}}</button>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>

                    <div class="col-md-4">
                        <div class="curr_conv_div">
                            <div class="row p-2">
                                <div class="col-md-12">
                                    <h6 class="featurette-H">Login Details</h6>
                                    {!!session('err_div','')!!}
                                    <hr>
                                </div>
                                
                                
                                <div class="col-md-4 pl-5" style="display:none;">
                                    <div class="imgHolder">                                        
                                       <img src="images/black_beared_man.png" alt="">                                       
                                    </div>
                                </div>

                                <form action='/admin/update_login_details' method='POST'>
                                @csrf

                                <div class="col-md-12 px-5">
                                    <div class="input-group input-group-sm mt-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span data-feather="user"></span></span>
                                        </div>
                                        <input type="text" name="usee" id="text" class="form-control" value="{{$user}}" readonly>
                                    </div>
                                </div>
                                <input type="hidden" name="ret_ul" value="{{session($_GET['rdr'])['hash']}}">
                                <div class="col-md-12 px-5">
                                    <div class="input-group input-group-sm mt-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span data-feather="lock"></span></span>
                                        </div>
                                        <input type="password" name="passwordd" value="{{session('passwordd','')}}" id="text" class="form-control" placeholder="Password">
                                        
                                    </div>
                                    <span class="text-danger">{{$errors->first('passwordd')}}</span>
                                </div>

                                <div class="col-md-12 px-5">
                                    <div class="input-group input-group-sm mt-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span data-feather="check-circle"></span></span>
                                        </div>
                                        <input type="password" name="confirm_passwordd" id="text" class="form-control" placeholder="Re-type Password" value="{{session('confirm_passwordd','')}}">
                                        
                                    </div>
                                    <span class="text-danger">{{$errors->first('confirm_passwordd')}}</span>
                                    <button class="btn btn-sm btn-dark BTN float-right mt-3" type="submit" style="float:right;"><span data-feather="send"></span> Update</button>
                                </div>
                                </form>
                                

                            </div>

                            
                        </div> 
                    </div>
                </div>

                           
@endsection