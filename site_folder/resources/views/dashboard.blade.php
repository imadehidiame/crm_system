@extends('layouts.admin_dashboard')
@section('page_style')
body{
                padding:0;
                margin:0;
                font-size: .875rem;
                -webkit-font-smoothing:antialiased;
                text-rendering: optimizeLegibility;
                
            }


            .feather {
                width: 1rem;
                height: 1rem;        
                vertical-align:text-bottom;
            }

            .BTN{
                border-radius:20px;                
            }

            .BTN_bg{
                border-radius:20px;  
                background:#FFF;
            }

            a{
                text-decoration:none;
                color:currentcolor;
            }

            a.BTN .feather:hover{                
                color:#000;
            }

            .INPUT{
                border-top-left-radius:15px;
                border-top-right-radius:15px;
            }


            /*========================================================
            ===================SIDEBAR STARTS HERE ====================*/
            /*
            * Sidebar
            */
                
                #sidebar{
                    margin-top:122px;
                    
                }

            .sidebar {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            z-index: 100; /* Behind the navbar */
            padding: 0;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar-sticky {
            position: -webkit-sticky;
            position: sticky;
            top: 48px; /* Height of navbar */
            height: calc(100vh - 48px);
            padding-top: .5rem;
            overflow-x: hidden;
            overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
            background:#FFF;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar .nav-link {
            font-weight: 500;
            color: #333;
            }

            .sidebar .nav-link .feather {
            margin-right: 4px;
            color: #999;
            }

            .sidebar .nav-link.active {
            color: #007bff;
            }

            .sidebar .nav-link:hover .feather,
            .sidebar .nav-link.active .feather {
            color: inherit;
            }

            .sidebar-heading {
            font-size: .75rem;
            text-transform: uppercase;
            }

            /*
            * Utilities
            */

            .border-top { border-top: 1px solid #e5e5e5; }
            .border-bottom { border-bottom: 1px solid #e5e5e5; }
                
            /*==================SIDEBAR ENDS HERE ========================
            ========================================================*/


            /*========================================================
            ===================MAIN STARTS HERE ====================*/
                #main{
                    margin-top:10.125rem;    
                    padding-right:110px;                    
                }
                
                
                /*
                * Cards
                */
                .card-header{
                    height: 0.625rem;
                    padding: 0px;
                }
                
                .card-body p{
                    font-size: 1.125rem;
                    font-weight: 400;
                    margin: 0px;
                }

            /*==================MAIN ENDS HERE ========================
            ========================================================*/
            
            


            /*========================================================
            =================== STARTS HERE ====================*/
                
            /*================== ENDS HERE ========================
            ========================================================*/
                
                #nav{                    		
                    background:black;                    
                    color:#FFF;  
                } 
                
                
                #nav a{
                    color: currentColor;
                    text-decoration: none;		
                }
                
                #logo{
                    margin: 0.3rem;
                    /*font-size: 1.3rem;
                    font-weight:300;*/
                    color: #111;
                }
                
                
                #badge{
                    position:relative;
                    font-size:14px;		
                    top:-10px;                    
                    left:-15px;
                    background:#ff526f;
                    color:#FFF;
                    border-radius:1.25rem; 
                    border:1px solid #DDD;                    
                }

                
                #user{		
                    		
                }

                #user img{                
                    width:25px;
                    height:25px;
                    margin-right:10px;                
                }
                

                
                #logout{                    
                    margin-left:0.625rem;
                }                         
                
                .searchDiv .form-control{
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none; 
                    background:none; 
                    color:#FFF;
                    display:none;
                }

                .searchDiv button{
                    border-left:none; 
                    border-color:#DDD;
                    display:none;
                }
                
                /*================== NAV ENDS HERE ========================
                ========================================================*/
                
                .modal#exampleModal{                           
                    margin-left:635px; 
                    margin-top:30px;                    
                    width:450px;
                }

                .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                    position:absolute;
                    margin-top:-11px;
                    margin-left:390px;
                    color:#FFF;                
                }            

                .notificationDetials{ 
                    float:left;                                  
                    border:1px solid #DDD;               
                    border-radius:10px;
                    margin:5px 10px;
                    padding:5px;
                }

                .notificationDetials .pic{                              
                    padding:5px;
                    float:left;                    
                }
                
                .notificationDetials .message{                                       
                    padding:5px;    
                    width:310px;
                    float:left;                                   
                }

                .mark-read{
                    float:right;
                    margin-right:20px;
                }

                .navBottom{
                    margin-top:65px;
                    background:#343a40;
                    color:#FFF;   
                    position:fixed;
                    width:102%; 
                    z-index:10;     
                    padding:5px 0px;                 
                }


                .topRow{                    
                    padding:10px 0px ;
                    margin-top:-80px;
                    position:fixed;
                    width:74%;   
                    z-index:10;                 
                }

                .topRow_1stcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .topRow_lastcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .curr_conv_div{
                    position:relative;
                    height:390px;                
                    box-shadow: -1px 3px 10px #bbb;
                    border-radius:10px;
                    overflow:hidden;                      
                }

                .curr_conv_img{
                    height:100px;           
                    position:absolute;                
                    left:0;
                    bottom:0;                
                    width:100%;                                               
                    background:url(images/bg.png);                
                    background-size:cover;
                    background-repeat:no-repeat;                
                }

                .bars{
                    margin-top:20px;
                }

                .space{                    
                    margin: 10px 5px;
                    border-radius:5px;
                    padding: 10px 50px;
                }

                .space .btn{                    
                    box-shadow:none;
                }

                .progress{
                    height:25px;
                    box-shadow: -2px 3px 10px #BBB;   
                    background:#FFF;    
                    clear:both;                         
                }

                .crdAcc input#Enteramount {                    
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none;                     
                }

                .input-group-text.t2{
                    border-top:none; 
                    border-left:none; 
                    border-right:none;                  
                    background:none; 
                }

                @keyframes feather{
                    0%{
                       transform:rotate(0deg);
                    }                                   
                    
                    100%{
                        transform:rotate(360deg);
                    }
                }
                
                
                .refresh.feather{                    	
                    animation-name: feather;
                    animation-duration: 5s;
                    animation-iteration-count: infinite;
                    animation-timing-function: linear;		
                    animation-direction: forwards;
                }

                .modal#updatemodal{                     
                    margin-top:50px;                    
                    
                }

                .modal#updatemodal input#minimumSale, #exchangeRate, #amount{
                    margin-top:5px;                    
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none; 
                    background:none; 
                    color:#000; 
                    font-size:14px;
                    font-weight: 400;
                    line-height: 1;
                    letter-spacing: -.05rem;
                }

                .input-group-text{
                    border-top:none; 
                    border-left:none; 
                    border-right:none;                  
                    background:none; 
                }

                .succ_div{
                    width:auto;
                    float:left;                    
                }
                
                .succ_details{
                    width:auto;
                    background:#5aeeb0;
                    color:#048d54;
                    padding:10px 40px;
                    margin:10px;
                    border-radius:5px;                    
                }

                .dpic{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:pink;
                    border-radius:100%;
                }

                .dpic2{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:#8af7ff;
                    border-radius:100%;
                }


            
                

                /* Featurettes
                ------------------------- */

                .featurette-divider {
                margin: 5rem 0; /* Space out the Bootstrap <hr> more */
                }

                /* Thin out the marketing headings */
                .featurette-H {
                font-weight: 300;
                line-height: 1;
                letter-spacing: -.05rem;
                }






                /* ======================= @ MEDIA QUARIES ================================= */
                /* ========================================================================= */
                
                /* Small devices (landscape phones, 576px and up) */
                @media (max-width: 480px ) {
                    
                }
                
                /* Medium devices (tablets, 768px and up)*/
                @media (max-width: 768px ) {	
                    #nav{			
                        max-height: auto;                        
                    }    

                    .dropdown{	
                        margin-top: 1.25rem;
                        margin-left:1.25rem;
                    }

                    .notify{
                        margin-left:20px;
                    }
                    
                    #user{	
                        margin-top: 10px;						
                        margin-left:1.25rem;		
                    }
                    
                    #logout{				
                        margin-top: 10px;						
                        margin-left:1.25rem;	
                    }


                    .modal#exampleModal{                           
                        margin-left:35px; 
                        margin-top:100px;                    
                        width:450px;
                    }

                    .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                        position:absolute;
                        margin-top:-11px;
                        margin-left:20px;
                        color:#FFF;                
                    }            

                    .notificationDetials{ 
                        float:left;                                  
                        border:1px solid #DDD;               
                        border-radius:10px;
                        margin:5px 10px;
                        padding:5px;
                    }

                    .notificationDetials .pic{                              
                        padding:5px;
                        float:left;                    
                    }
                    
                    .notificationDetials .message{                                       
                        padding:5px;    
                        width:310px;
                        float:left;                                   
                    }

                    .mark-read{
                        float:right;
                        margin-right:20px;
                    }


                    .navBottom{                        
                        width:108%;    
                        background:#343a40;
                        margin-top:65px;                        
                        color:#FFF;   
                        position:fixed;                        
                        z-index:10;                  
                    }

                    .searchDiv{
                        display:none;
                    }

                    .topRow{                    
                        padding:10px 0px ;
                        margin-top:-80px;
                        position:fixed;
                        width:74%;   
                        z-index:10;                 
                    }

                    .topRow_1stcol{
                        background:#FFF;
                        border-radius:5px;
                        padding:5px 20px;                        
                    }

                    .topRow_1stcol h3{
                        font-size:20px;                        
                    }

                    .topRow_lastcol{
                        display:none;
                    }

                    #main{                           
                        padding-right:15px;                    
                    }

                    .bars{
                        margin-top:0px;                                           
                    }

                    .space{
                        background:#fEfEfe;
                        box-shadow: 1px 1px 3px #DDD;
                        margin: 10px 0px;
                        border-radius:5px;
                        padding: 10px 10px;
                    }

                    .curr_conv_div{
                        position:relative;
                        height:350px;                
                        box-shadow: -1px 1px 10px #bbb;
                        border-radius:10px;
                        overflow:hidden;  
                        margin-top:50px;                    
                    }
                    
                    
                }
                
                /* Large devices (desktops, 992px and up) */
                @media (max-width: 992px ) {
                    
                    
                }
                
                /* Extra large devices (large desktops, 1200px and up) */
                @media (max-width: 1200px ) {	}
@endsection
@section('page_main')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom topRow">
					
                    <div class="col-md-4 topRow_1stcol">
                        <h3 style="font-family: tahoma;">Dashboard</h3>
                    </div>
                    <div class="col-md-4 d-sm-none d-md-block d-lg-block">

                    </div>
                    <div class="col-md-4 topRow_lastcol d-sm-none d-md-block d-lg-block">
                        <div class="btn-toolbar mb-3 float-right">
                            <div class="btn-group mr-2">
                                <button class="btn btn-sm btn-outline-dark">Share</button>
                                <button class="btn btn-sm btn-outline-dark">Export</button>
                            </div>
                            
                            <div class="btn btn-sm btn-outline-dark" id="timebutton">
                            <span data-feather="calendar"></span>
                            <?php echo date("D-m-y"); ?>
                            </div>
                        </div>
                    </div>

				</div>
				
				
				<div class="row bars">

                    <div class="col-md-8">
                        <h4 class="featurette-H"> Company Information </h4>

                        <!-- Modal -->
                        
                        <div class="row">
                            <div class="col-md-12 space">
                                <h5 class="featurette-H font-weight-normal float-left">Super Administrator </h5>                            
                                <!-- Button trigger modal -->

                                @php
                                $super_admin_width = $pack->super_admin; 
                                if(strtolower($package_benefits['Super Administrator'])=='unlimited'){
                                $super_admin_width = ($super_admin_width/($super_admin_width*100) )*100;
                                }else{
                        $super_admin_width = intval(($super_admin_width/$package_benefits['Super Administrator']*100));
                                }
                                @endphp
                                
                                <h6 class="featurette-H font-weight-normal float-right"> {{$pack->super_admin}}/{{$package_benefits['Super Administrator']}} </h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="{{$super_admin_width}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$super_admin_width==0?1:$super_admin_width}}%;"></div>
                                </div>                                
                            </div>


                        <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> Administrators </h5>
                                <!-- Button trigger modal -->

                                @php
                                $admin_width = $pack->admin == 0 ? 1 : $pack->admin; 
                                if(strtolower($package_benefits['Administrator'])=='unlimited'){
                                $admin_width = ($admin_width/($admin_width*100) )*100;
                                }else{
                                $admin_width = intval(($admin_width/$package_benefits['Administrator'] )*100);
                                }
                                @endphp

                                <h6 class="featurette-H font-weight-normal float-right"> {{$pack->admin}}/{{$package_benefits['Administrator']}}</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="{{$admin_width}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$admin_width}}%;"></div>
                                </div>                                
                            </div>


                            <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> Customer Managers </h5>
                                <!-- Button trigger modal -->
                                
                                @php
                                $customer_manager_width = $pack->customer_manager == 0 ? 1 : $pack->customer_manager; 
                                if(strtolower($package_benefits['Customer Manager'])=='unlimited'){
                                $customer_manager_width = ($customer_manager_width/($customer_manager_width*100) )*100;
                                }else{
                                $customer_width = intval(($customer_manager_width/$package_benefits['Customer Manager'] )*100);
                                }
                                @endphp


                                <h6 class="featurette-H font-weight-normal float-right"> {{$pack->customer_manager}}/{{$package_benefits['Customer Manager']}} </h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="{{$customer_manager_width}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$customer_manager_width}}%;"></div>
                                </div>                                
                            </div>
                            <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> Customers </h5>
                                <!-- Button trigger modal -->
                                @php
                                $customer_width = $pack->customer == 0 ? 1 : $pack->customer; 
                                if(strtolower($package_benefits['Customer'])=='unlimited'){
                                $customer_width = ($customer_width/($customer_width*100) )*100;
                                }else{
                                $customer_width = intval(($customer_width/$package_benefits['Customer'] )*100);
                                }
                                @endphp
                                <h6 class="featurette-H font-weight-normal float-right"> {{$pack->customer}}/{{$package_benefits['Customer']}}</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-dark" role="progressbar" aria-valuenow="{{$customer_width}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$customer_width}}%;"></div>
                                </div>                                
                            </div>
 
                            

                            
                        </div> 
                    </div>

                    <div class="col-md-4">
                        <div class="curr_conv_div" id="curr_conv_div">
                            <div class="p-2">
                                <h5 class="featurette-H font-weight-normal">{{$pack->package}} Bundle</h5>
                                <hr>
                                <div class="col-md-12  mb-3 from">
                                    <h6 class="featurette-H font-weight-normal">Package Benefits</h6>
                                    <hr>
                                    
                                    @foreach($package_benefits as $packs => $value)
                                    @if(strtolower($value) == "unlimited")
                                    <p><b>*</b> No limit to the number of {{str_plural($packs)}}</p>
                                    @else
                                    <p><b>*</b> {{$value }} {{$value > 1?str_plural($packs):$packs}}</p>
                                    @endif
                                    @endforeach
                                    <p><b>*</b>Forum chat support between customers and customer care reps</p>
                                    <div class="input-group text-white bg-dark">                                        
                                        <input type="text" class="form-control text-white bg-dark" placeholder="Amount" value="Amount">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-transparent border-left-0 border-right-0 border-top"><span data-feather="dollar-sign" class="text-white bg-dark"></span></span>
                                        </div>
                                        <input type="text" class="form-control text-white border-left-0  bg-dark" value="0.00/mon">                                        
                                    </div>                                        
                                </div>

                            </div>

                            <div class="curr_conv_img" style="display:none;"></div>
                        </div> 
                    </div>
                </div>

                
@endsection