<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/glyphicon.css')}}">
        <link href="{{URL::asset('css/font-awesome.min.css')}}" rel="stylesheet">
        <style>
            /* GLOBAL STYLES
            -------------------------------------------------- */
            /* Padding below the footer and lighter body text */
            body{
                padding:0;
                margin:0;
                font-size: .875rem;
                -webkit-font-smoothing:antialiased;
                text-rendering: optimizeLegibility;
                
            }


            .feather {
                width: 1rem;
                height: 1rem;        
                vertical-align:text-bottom;
            }

            .BTN{
                border-radius:20px;                
            }

            .BTN_bg{
                border-radius:20px;  
                background:#FFF;
            }

            a{
                text-decoration:none;
                color:currentcolor;
            }

            a.BTN .feather:hover{                
                color:#000;
            }

            .INPUT{
                border-top-left-radius:15px;
                border-top-right-radius:15px;
            }


            /*========================================================
            ===================SIDEBAR STARTS HERE ====================*/
            /*
            * Sidebar
            */
                
                #sidebar{
                    margin-top:122px;
                    
                }

            .sidebar {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            z-index: 100; /* Behind the navbar */
            padding: 0;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar-sticky {
            position: -webkit-sticky;
            position: sticky;
            top: 48px; /* Height of navbar */
            height: calc(100vh - 48px);
            padding-top: .5rem;
            overflow-x: hidden;
            overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
            background:#FFF;
            box-shadow: inset -3px 0 3px rgba(0, 0, 0, .1);
            }

            .sidebar .nav-link {
            font-weight: 500;
            color: #333;
            }

            .sidebar .nav-link .feather {
            margin-right: 4px;
            color: #999;
            }

            .sidebar .nav-link.active {
            color: #007bff;
            }

            .sidebar .nav-link:hover .feather,
            .sidebar .nav-link.active .feather {
            color: inherit;
            }

            .sidebar-heading {
            font-size: .75rem;
            text-transform: uppercase;
            }

            /*
            * Utilities
            */

            .border-top { border-top: 1px solid #e5e5e5; }
            .border-bottom { border-bottom: 1px solid #e5e5e5; }
                
            /*==================SIDEBAR ENDS HERE ========================
            ========================================================*/


            /*========================================================
            ===================MAIN STARTS HERE ====================*/
                #main{
                    margin-top:10.125rem;    
                    padding-right:110px;                    
                }
                
                
                /*
                * Cards
                */
                .card-header{
                    height: 0.625rem;
                    padding: 0px;
                }
                
                .card-body p{
                    font-size: 1.125rem;
                    font-weight: 400;
                    margin: 0px;
                }

            /*==================MAIN ENDS HERE ========================
            ========================================================*/
            
            


            /*========================================================
            =================== STARTS HERE ====================*/
                
            /*================== ENDS HERE ========================
            ========================================================*/
                
                #nav{                    		
                    background:black;                    
                    color:#FFF;  
                } 
                
                
                #nav a{
                    color: currentColor;
                    text-decoration: none;		
                }
                
                #logo{
                    margin: 0.3rem;
                    /*font-size: 1.3rem;
                    font-weight:300;*/
                    color: #111;
                }
                
                
                #badge{
                    position:relative;
                    font-size:14px;		
                    top:-10px;                    
                    left:-15px;
                    background:#ff526f;
                    color:#FFF;
                    border-radius:1.25rem; 
                    border:1px solid #DDD;                    
                }

                
                #user{		
                    		
                }

                #user img{                
                    width:25px;
                    height:25px;
                    margin-right:10px;                
                }
                

                
                #logout{                    
                    margin-left:0.625rem;
                }                         
                
                .searchDiv .form-control{
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none; 
                    background:none; 
                    color:#FFF;
                    display:none;
                }

                .searchDiv button{
                    border-left:none; 
                    border-color:#DDD;
                    display:none;
                }
                
                /*================== NAV ENDS HERE ========================
                ========================================================*/
                
                .modal#exampleModal{                           
                    margin-left:635px; 
                    margin-top:30px;                    
                    width:450px;
                }

                .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                    position:absolute;
                    margin-top:-11px;
                    margin-left:390px;
                    color:#FFF;                
                }            

                .notificationDetials{ 
                    float:left;                                  
                    border:1px solid #DDD;               
                    border-radius:10px;
                    margin:5px 10px;
                    padding:5px;
                }

                .notificationDetials .pic{                              
                    padding:5px;
                    float:left;                    
                }
                
                .notificationDetials .message{                                       
                    padding:5px;    
                    width:310px;
                    float:left;                                   
                }

                .mark-read{
                    float:right;
                    margin-right:20px;
                }

                .navBottom{
                    margin-top:65px;
                    background:#343a40;
                    color:#FFF;   
                    position:fixed;
                    width:102%; 
                    z-index:10;     
                    padding:5px 0px;                 
                }


                .topRow{                    
                    padding:10px 0px ;
                    margin-top:-80px;
                    position:fixed;
                    width:74%;   
                    z-index:10;                 
                }

                .topRow_1stcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .topRow_lastcol{
                    background:#FFF;
                    border-radius:5px;
                    padding:10px 20px;
                }

                .curr_conv_div{
                    position:relative;
                    height:450px;                
                    box-shadow: -1px 3px 10px #bbb;
                    border-radius:10px;
                    overflow:hidden;                      
                }

                .curr_conv_img{
                    height:100px;           
                    position:absolute;                
                    left:0;
                    bottom:0;                
                    width:100%;                                               
                    background:url(images/bg.png);                
                    background-size:cover;
                    background-repeat:no-repeat;                
                }

                .bars{
                    margin-top:20px;
                }

                .space{                    
                    margin: 10px 5px;
                    border-radius:5px;
                    padding: 10px 50px;
                }

                .space .btn{                    
                    box-shadow:none;
                }

                .progress{
                    height:25px;
                    box-shadow: -2px 3px 10px #BBB;   
                    background:#FFF;    
                    clear:both;                         
                }

                .crdAcc input#Enteramount {                    
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none;                     
                }

                .input-group-text.t2{
                    border-top:none; 
                    border-left:none; 
                    border-right:none;                  
                    background:none; 
                }

                @keyframes feather{
                    0%{
                       transform:rotate(0deg);
                    }                                   
                    
                    100%{
                        transform:rotate(360deg);
                    }
                }
                
                
                .refresh.feather{                    	
                    animation-name: feather;
                    animation-duration: 5s;
                    animation-iteration-count: infinite;
                    animation-timing-function: linear;		
                    animation-direction: forwards;
                }

                .modal#updatemodal{                     
                    margin-top:50px;                    
                    
                }

                .modal#updatemodal input#minimumSale, #exchangeRate, #amount{
                    margin-top:5px;                    
                    border-top:none; 
                    border-left:none; 
                    border-right:none; 
                    box-shadow:none; 
                    background:none; 
                    color:#000; 
                    font-size:14px;
                    font-weight: 400;
                    line-height: 1;
                    letter-spacing: -.05rem;
                }

                .input-group-text{
                    border-top:none; 
                    border-left:none; 
                    border-right:none;                  
                    background:none; 
                }

                .succ_div{
                    width:auto;
                    float:left;                    
                }
                
                .succ_details{
                    width:auto;
                    background:#5aeeb0;
                    color:#048d54;
                    padding:10px 40px;
                    margin:10px;
                    border-radius:5px;                    
                }

                .dpic{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:pink;
                    border-radius:100%;
                }

                .dpic2{
                    height:25px;
                    width:25px;
                    border:1px solid #999;
                    background:#8af7ff;
                    border-radius:100%;
                }


            
                

                /* Featurettes
                ------------------------- */

                .featurette-divider {
                margin: 5rem 0; /* Space out the Bootstrap <hr> more */
                }

                /* Thin out the marketing headings */
                .featurette-H {
                font-weight: 300;
                line-height: 1;
                letter-spacing: -.05rem;
                }






                /* ======================= @ MEDIA QUARIES ================================= */
                /* ========================================================================= */
                
                /* Small devices (landscape phones, 576px and up) */
                @media (max-width: 480px ) {
                    
                }
                
                /* Medium devices (tablets, 768px and up)*/
                @media (max-width: 768px ) {	
                    #nav{			
                        max-height: auto;                        
                    }    

                    .dropdown{	
                        margin-top: 1.25rem;
                        margin-left:1.25rem;
                    }

                    .notify{
                        margin-left:20px;
                    }
                    
                    #user{	
                        margin-top: 10px;						
                        margin-left:1.25rem;		
                    }
                    
                    #logout{				
                        margin-top: 10px;						
                        margin-left:1.25rem;	
                    }


                    .modal#exampleModal{                           
                        margin-left:35px; 
                        margin-top:100px;                    
                        width:450px;
                    }

                    .modal#exampleModal .glyphicon.glyphicon-triangle-top{
                        position:absolute;
                        margin-top:-11px;
                        margin-left:20px;
                        color:#FFF;                
                    }            

                    .notificationDetials{ 
                        float:left;                                  
                        border:1px solid #DDD;               
                        border-radius:10px;
                        margin:5px 10px;
                        padding:5px;
                    }

                    .notificationDetials .pic{                              
                        padding:5px;
                        float:left;                    
                    }
                    
                    .notificationDetials .message{                                       
                        padding:5px;    
                        width:310px;
                        float:left;                                   
                    }

                    .mark-read{
                        float:right;
                        margin-right:20px;
                    }


                    .navBottom{                        
                        width:108%;    
                        background:#343a40;
                        margin-top:65px;                        
                        color:#FFF;   
                        position:fixed;                        
                        z-index:10;                  
                    }

                    .searchDiv{
                        display:none;
                    }

                    .topRow{                    
                        padding:10px 0px ;
                        margin-top:-80px;
                        position:fixed;
                        width:74%;   
                        z-index:10;                 
                    }

                    .topRow_1stcol{
                        background:#FFF;
                        border-radius:5px;
                        padding:5px 20px;                        
                    }

                    .topRow_1stcol h3{
                        font-size:20px;                        
                    }

                    .topRow_lastcol{
                        display:none;
                    }

                    #main{                           
                        padding-right:15px;                    
                    }

                    .bars{
                        margin-top:0px;                                           
                    }

                    .space{
                        background:#fEfEfe;
                        box-shadow: 1px 1px 3px #DDD;
                        margin: 10px 0px;
                        border-radius:5px;
                        padding: 10px 10px;
                    }

                    .curr_conv_div{
                        position:relative;
                        height:450px;                
                        box-shadow: -1px 1px 10px #bbb;
                        border-radius:10px;
                        overflow:hidden;  
                        margin-top:50px;                    
                    }
                    
                    
                }
                
                /* Large devices (desktops, 992px and up) */
                @media (max-width: 992px ) {
                    
                    
                }
                
                /* Extra large devices (large desktops, 1200px and up) */
                @media (max-width: 1200px ) {	}
                
              
                
                
            /*===========================================================
            ===========================================================*/
                
                                       
        </style>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- NAV starts here -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top px-4 bg-dark" id="nav">	  
            <div class="container-fluid">
            <!-- LOGO -->
            <a class="navbar-brand" id="logo">
                <img src="{{URL::asset('svg/nutshell_logo.png')}}" class="">UTSHELL
                
            </a>
        
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto"></ul>
                            
                
                <ul class="navbar-nav">                    
                    <li class="nav-item">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-sm btn-outline-light BTN px-3 notify" data-toggle="modal" data-target="#exampleModal">
                            <span data-feather="bell"></span>
                        </button>
                        <div id="badge" class="badge featurette-H">02</div>                        
                    </li>                    
                    
                    <li><div id="user" class="featurette-H"><button class="btn btn-sm btn-outline-light BTN px-3 "><img src="{{URL::asset('svg/ave.png')}}" alt="" class="float-left"> Kool Aid Incorporations </button></div></li>
                    <li><div id="logout"><button class="btn btn-sm btn-outline-light BTN px-3"><span data-feather="power"></span></button></div></li>
                </ul>		
                
                
                <div class="d-sm-block d-md-none d-lg-none">                    
                    
                                        
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Saved reports</span>
                        <a class="d-flex align-items-center text-muted" href="#">
                        <span data-feather="plus-circle"></span>
                        </a>
                    </h6>

                    <ul class="nav flex-column mb-2">						
                        <li class="nav-item">
                            <a class="nav-link" href="profile.php">
                            <span data-feather="users"></span>
                            Profile
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="chat.php">
                            <span data-feather="message-circle"></span>
                            Chat with Admin
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="buy.php">
                            <span data-feather="shopping-bag"></span>
                            Buy
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="sell.php">
                            <span data-feather="shopping-cart"></span>
                            Sell
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="bids.php">
                            <span data-feather="target"></span>
                            Bids
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="ongoingtransaction.php">
                            <span class="refresh" data-feather="refresh-cw"></span>
                            On-Going Transactions
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="history.php">
                            <span data-feather="inbox"></span>
                            Transaction History
                            </a>
                        </li>
                        												
                    </ul>	
                </div>
                
            </div>     
            </div>   
        </nav>
        <!-- NAV ends here -->


        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                <span class="glyphicon glyphicon-triangle-top"></span>

                <div class="modal-header">
                    <h5 class="featurette-H font-weight-normal"> Notifications</h5>
                    <button class="btn btn-sm bg-transparent BTN mark-read" data-toggle="tooltip" data-placement="top" title="Mark All Read" ><span data-feather="check-circle"></span></button>                    
                </div>
                <div class="modal-body">                    
                    <div class="notificationDetials">
                        <div class="pic"><button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic mr-1">AE </div></button></div>
                        <div class="message">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt nulla dicta vel corporis quod at a aperiam est excepturi impedit, This User made a bid on your Currency</div>
                    </div>

                    <div class="notificationDetials">
                        <div class="pic"><button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic2 mr-1">FL </div></button></div>
                        <div class="message">This User made a bid on your Currency</div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <a href="notification.php" class="btn btn-sm btn-outline-warning BTN px-3 featurette-H" role="button"> View All</a>
                    <button type="button" class="btn btn-sm bg-transparent BTN featurette-H" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal"> <span data-feather="x-circle"></span></button>
                </div>
                </div>
            </div>
        </div>

        
        
        <div class="row navBottom pr-4">                       
            <div class="col-md-6">
                <h6 class="featurette-H font-weight-normal"><a class="nav-link active mt-2" style="color:currentcolor;" href="loginsuccess.php"> <span data-feather="home"></span> Dashboard </a></h6>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4 searchDiv">
                <div class="input-group input-group-sm mb-3">
                    <input type="text" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-outline-light" type="button"> <span data-feather="search"></span></button>
                    </div>
                </div>
            </div>
        </div>

        <!--- CONTAINER STARTS HERE -->
        <div class="container">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar" id="sidebar">
                    <div class="sidebar-sticky">                        
                                            
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Navigation Links</span>
                            <a class="d-flex align-items-center text-muted" href="#">
                            <span data-feather="plus-circle"></span>
                            </a>
                        </h6>

                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link" href="profile.php">
                                <span data-feather="users"></span>
                                Profile
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="chat.php">
                                <span data-feather="message-circle"></span>
                                Chat with Admin
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="buy.php">
                                <span data-feather="shopping-bag"></span>
                                Buy
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="sell.php">
                                <span data-feather="shopping-cart"></span>
                                Sell
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="bids.php">
                                <span data-feather="target"></span>
                                Bids
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="ongoingtransaction.php">
                                <span class="refresh" data-feather="refresh-cw"></span>
                                On-Going Transactions
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="history.php">
                                <span data-feather="inbox"></span>
                                Transaction History
                                </a>
                            </li>	
                            			
                        </ul>					
                        
                    </div>
                </nav>
            </div>
        </div>

        <!-- MAIN SECTION STARTS HERE -->
			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3" id="main">
				<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom topRow">
					
                    <div class="col-md-4 topRow_1stcol">
                        <h3 style="font-family: tahoma;">Dashboard</h3>
                    </div>
                    <div class="col-md-4 d-sm-none d-md-block d-lg-block">

                    </div>
                    <div class="col-md-4 topRow_lastcol d-sm-none d-md-block d-lg-block">
                        <div class="btn-toolbar mb-3 float-right">
                            <div class="btn-group mr-2">
                                <button class="btn btn-sm btn-outline-success">Share</button>
                                <button class="btn btn-sm btn-outline-success">Export</button>
                            </div>
                            
                            <div class="btn btn-sm btn-outline-success" id="timebutton">
                            <span data-feather="calendar"></span>
                            <?php echo date("D-m-y"); ?>
                            </div>
                        </div>
                    </div>

				</div>
				
				
				<div class="row bars">

                    <div class="col-md-8">
                        <h4 class="featurette-H"> Hightlights of your Current Ballance </h4>

                        <!-- Modal -->
                        <div class="modal fade" id="updatemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog " role="document">
                                <div class="modal-content">                

                                <div class="modal-header">
                                    <h5 class="featurette-H font-weight-normal"> Update E-Currency</h5>                    
                                </div>
                                <div class="modal-body">   

                                    <div class="col-md-12 d-flex justify-content-center">
                                        <div class="succ_div ">
                                            <div class="succ_details text-center featurette-H font-weight-bold"> You Successfully updated currency ! 
                                            <br><span class="font-weight-normal">Changes would reflect in the currency</span></div>
                                        </div>
                                    </div>

                                    <div class="editBody col-md-10 offset-md-1 pt-3">
                                        <label for="amount"> Amount :</label>
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="dollar-sign"></span></span>
                                            </div>
                                            <input type="text" name="" id="amount" class="form-control" required>
                                        </div>

                                        <label for="exchangeRate"> Exchange Rate :</label>
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="dollar-sign"></span></span>
                                            </div>
                                            <input type="text" name="" id="exchangeRate" class="form-control" required>
                                        </div>

                                        <label for="minimumSale"> Minimum Sale :</label>
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><span data-feather="dollar-sign"></span></span>
                                            </div>
                                            <input type="text" name="" id="minimumSale" class="form-control" required>
                                        </div>                                                                                                                        

                                        <button class="btn btn-sm btn-success BTN float-right my-3"><span data-feather="send"></span> Update</button>
                                    </div>                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-outline-secondary BTN px-3 featurette-H" data-dismiss="modal"> Close</button>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 space">
                                <h5 class="featurette-H font-weight-normal float-left"> Paypal </h5>                            
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-info bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 55.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width:15%;"></div>
                                </div>                                
                            </div>
                            <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> Bitcoin </h5>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-warning bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 680.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width:3%;"></div>
                                </div>                                
                            </div>
                            <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> Webmoney </h5>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-dark bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 990.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-dark" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%;"></div>
                                </div>                                
                            </div>

                            <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> Payza </h5>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-success bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 145.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width:33%;"></div>
                                </div>                                
                            </div>

                            <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> Ok Pay </h5>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-warning bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 55.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width:15%;"></div>
                                </div>                                
                            </div>
                        
                            <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> Skrill </h5>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-secondary bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 680.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-secondary" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width:3%;"></div>
                                </div>                                
                            </div>

                            <div class="col-md-12 space">
                                <h5 class="featurette-H font-weight-normal float-left"> Neteller </h5>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-success bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 990.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%;"></div>
                                </div>                                
                            </div>

                            <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> Perfect Money </h5>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-danger bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 145.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width:33%;"></div>
                                </div>                                
                            </div>

                            <div class="col-md-12 space">
                                <h5 class="featurette-H font-weight-normal float-left"> Payeer </h5>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-info bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 55.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width:15%;"></div>
                                </div>                                
                            </div>
                        
                            <div class="col-md-12 space ">
                                <h5 class="featurette-H font-weight-normal float-left"> FIAT </h5>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-sm text-danger bg-transparent BTN float-right px-3" data-toggle="modal" data-target="#updatemodal">
                                    <span data-feather="edit-3"></span> Update
                                </button>
                                <h6 class="featurette-H font-weight-normal float-right"> Total <span data-feather="dollar-sign"> </span> 680.00</h6> 
                                <div class="progress mb-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width:3%;"></div>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="col-md-4">
                        <div class="curr_conv_div" id="curr_conv_div">
                            <div class="p-2">
                                <h5 class="featurette-H font-weight-normal">CURRENT KOODIPAY BALANCE</h5>
                                <hr>
                                <div class="col-md-12  mb-3 from">
                                    <h6 class="featurette-H font-weight-normal">Cleard Total</h6>
                                    <div class="input-group">                                        
                                        <input type="text" class="form-control" placeholder="Amount">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-transparent border-left-0 border-right-0 border-top"><span data-feather="dollar-sign"></span></span>
                                        </div>
                                        <input type="text" class="form-control border-left-0" value="500.00">                                        
                                    </div>                                        
                                </div>

                                <div class="col-md-6  mb-3 from">
                                    <h6 class="featurette-H font-weight-normal">Uncleared Total</h6>
                                    <div class="input-group">                                                                                
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-dark text-white border-right-0 border-top"><span data-feather="dollar-sign"></span></span>
                                        </div>
                                        <input type="text" class="form-control text-white border-left-0  bg-dark" value="200.00" readonly>
                                    </div>                                        
                                </div>

                                <div class="col-md-12 crdAcc">
                                    <form action="creditaccount.php" id="creditaccount" method="post"></form>
                                    <h5 class="featurette-H font-weight-normal">Credit Account</h5>
                                    <div class="input-group input-group mt-1">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text t2"><span data-feather="dollar-sign"></span></span>
                                        </div>                                
                                        <input type="text" name="" placeholder="Enter Amount " class="form-control float-left" id="Enteramount">
                                    </div>                                  
                                    
                                    <button class=" btn btn-sm btn-outline-success BTN float-right my-2" type="submit" form="creditaccount"><span data-feather="dollar-sign"></span> Credit</button>
                                </div>
                            </div>

                            <div class="curr_conv_img"></div>
                        </div> 
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 mt-5">
                        <p class="featurette-H font-weight-normal float-right"><span data-feather="globe"></span> Powered by: NARDEY</p>
                        <p class="featurette-H text-center font-weight-normal">
                            &copy; <?php echo date("Y") ; ?> Koodimat, Inc &middot;                            
                        </p>
                    </div>
                </div>

			</main><!-- MAIN SECTION ENDS HERE -->

        <script src="{{URL::asset('js/jquery.js')}}"></script>
               
        <script src="{{URL::asset('js/popper.js')}}"></script>
        <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>        
                   
        <script src="{{URL::asset('js/feather.min.js')}}"></script> 

        <script>
            feather.replace()

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false;

        </script>   

        
             
    </body>
</html>