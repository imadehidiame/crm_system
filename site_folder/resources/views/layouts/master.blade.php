<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="X-CSRF-TOKEN" content="{{csrf_token()}}">
        <meta name="X-XSRF-TOKEN" content="{{$_COOKIE['XSRF-TOKEN']}}">
        <title> @yield('title')</title> 
        <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/grid.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/font-awesome.min.css')}}" rel="stylesheet">
        <style>@yield('page_css')</style>
    </head>
    <body class="@yield('body_class')">
    <script type="text/javascript" src="{{URL::asset('js/jquery.js') }}"></script>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="{{route('home')}}"><img class="mb-1" src="{{URL::asset('svg/nutshell_logo.png')}}">UTSHELL</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
        
          <li class="nav-item active" style="display:{{$hm}};">
            <a class="nav-link" href="{{route('home')}}">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item" style="display:{{$hm}};">
            <a class="nav-link" href="{{route('home')}}">Pricing</a>
          </li>
          
        </ul>
        <form class="form-inline mt-2 mt-md-0" style="display:{{$hm}};">
          <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </nav>
        @section('page')
            
        @show

    
    
    <div class="row" style="display:@yield('footer_display')">
                <div class="col-md-12 mt-5">
                <p class="featurette-heading font-weight-normal float-right"><span data-feather="globe"></span> Powered by: NUTSHELL</p>
                    <p class="featurette-heading text-center font-weight-normal">
                        &copy; <?php echo date("Y") ; ?> NUTSHELL, Inc. &middot; 
                        <a href="#" style="text-decoration:none;">Privacy</a> &middot; 
                        <a href="#" style="text-decoration:none;">Terms</a>
                    </p>
                </div>
            </div>
       

    <script type="text/javascript" src="{{URL::asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popper.js') }}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/feather.min.js')}}"></script>
    <!--<script type="text/javascript" src="/public/js/test.js"></script>  -->
    </body>
</html>