<html>
    <head>
        <title> @yield('title')</title>
        <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/grid.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('css/font-awesome.min.css')}}" rel="stylesheet">
        
    </head>
    <body>
        @section('sidebar')
            This is the master sidebar.
        @show

        <div class="container">
            @yield('content')
        </div>
    
    <script type="text/javascript" src="{{URL::asset('js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{URL::asset('js/popper.js') }}"></script>
    <script type="text/javascript" src="{{URL::asset('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/feather.min.js')}}"></script>
    <!--<script type="text/javascript" src="/public/js/test.js"></script>  -->
    </body>
</html>