<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NUTSHELL</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}"> 
        <link rel="stylesheet" href="{{URL::asset('css/glyphicon.css')}}">
        <link href="{{URL::asset('css/font-awesome.min.css')}}" rel="stylesheet">
        <style>
        @yield('page_style')
        </style>
        </head>
        
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top px-4 bg-dark" id="nav">	  
            <div class="container-fluid">
            <!-- LOGO -->
            <a class="navbar-brand mb-2" id="logo">
                <img src="{{URL::asset('svg/nutshell_logo.png')}}" class="">UTSHELL
                
            </a>
                <?php 
                $notification_number = 0;
                if(!is_int($super_notifications)):
                    for($count = 0;$count<count($super_notifications);$count++):
                        $notification_number+=1;
                    endfor;
                endif;
                ?>
                
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" {{$display_assets == "none"?"disabled":""}}>
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto"></ul>
                            
                

                <ul class="navbar-nav">                    
                    <li class="nav-item">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-sm btn-outline-light BTN px-3 notify" data-toggle="modal" data-target="#exampleModal"{{ $display_assets === "none"? " disabled":"" }}>
                            <span data-feather="bell"></span>
                        </button>
                        <div id="badge" class="badge featurette-H">{{$notification_number}}</div>                        
                    </li>                    
                    
                    <li><div id="user" class="featurette-H"><button class="btn btn-sm btn-outline-light BTN px-3 "><img src="{{URL::asset('svg/ave.png')}}" alt="" class="float-left"> {{session($_GET['rdr'])['data']->full_name}} </button></div></li>
                    <li><div id="logout"><a href="{{route('log',['rdr'=>$_GET['rdr']])}}"><button class="btn btn-sm btn-outline-light BTN px-3"><span data-feather="power"></span></button></a></div></li>
                </ul>		
                
                
                <div class="d-sm-block d-md-none d-lg-none">                    
                    
                                        
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Saved reports</span>
                        <a class="d-flex align-items-center text-muted" href="#">
                        <span data-feather="plus-circle"></span>
                        </a>
                    </h6>

                    <ul class="nav flex-column mb-2">						
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('dash_customer',['rdr'=>$_GET['rdr']])}}">
                            <span data-feather="users"></span>
                            {{ $display_assets == "none" ? "Create " : ""}}Profile
                            </a>
                        </li>
                        
                        
                        
                        
                        
                        


                        <li class="nav-item" style="display:{{$display_assets}}">
                            <a class="nav-link" href="{{route('customer_message',['rdr'=>$_GET['rdr']])}}">
                            <span data-feather="message-circle"></span>
                            Messaging
                            </a>
                        </li>

                        <li class="nav-item" style="display:{{$display_assets}}">
                            <a class="nav-link" href="{{route('customer_invoice',['rdr'=>$_GET['rdr']])}}">
                            <span data-feather="target"></span>
                            Invoice
                            </a>
                        </li>

                        <li class="nav-item" style="display:{{$display_assets}}">
                            <a class="nav-link" href="{{route('customer_meeting',['rdr'=>$_GET['rdr']])}}">
                            <span class="refresh" data-feather="refresh-cw"></span>
                            Meetings
                            </a>
                        </li>
                        
                        <li class="nav-item" style="display:{{$display_assets}}">
                            <a class="nav-link" href="#">
                            <span data-feather="inbox"></span>
                            Notifications
                            </a>
                        </li>
                        												
                    </ul>	
                </div>
                
            </div>     
            </div>   
        </nav>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                <span class="glyphicon glyphicon-triangle-top"></span>

                <div class="modal-header">
                    <h5 class="featurette-H font-weight-normal">{{$notification_number==0?"No New ":""}} Notifications</h5>
                    <button class="btn btn-sm bg-transparent BTN mark-read" data-toggle="tooltip" data-placement="top" title="Mark All Read" style="display:{{$notification_number==0?"none;":"block;"}}"><span data-feather="check-circle"></span></button>                    
                </div>
                @if($notification_number > 0)
                @for($c = 0; $c<count($super_notifications); $c++)
                <div class="modal-body">                    
                    <div class="notificationDetials">
                        <div class="pic"><button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic mr-1">{{strtoupper(substr(session($_GET['rdr'])['data']->username,0,1))}} </div></button></div>
                        <div class="message">{{$super_notifications[$c]['message']}}
                        <br><br>
                        <b style="float:right;">{{$super_notifications[$c]['time']}}</b>
                        </div>
                    </div>

                                        
                </div>
                @endfor
                @endif

                <div class="modal-footer">
                    <a href="notification.php" class="btn btn-sm btn-outline-warning BTN px-3 featurette-H" role="button"> View {{ $notification_number==0?" Previous Notifications":" All"}}</a>
                    <button type="button" class="btn btn-sm bg-transparent BTN featurette-H" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal"> <span data-feather="x-circle"></span></button>
                </div>
                </div>
            </div>
        </div>

          <div class="row navBottom pr-4">                       
            <div class="col-md-6">
                <h6 class="featurette-H font-weight-normal"><a class="nav-link active mt-2" style="color:currentcolor;" href="{{route('dash_customer',['rdr'=>$_GET['rdr']])}}"> <span data-feather="home"></span> Dashboard </a></h6>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4 searchDiv" style="display:none;">
                <div class="input-group input-group-sm mb-3">
                    <input type="text" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-outline-light" type="button"> <span data-feather="search"></span></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar" id="sidebar">
                    <div class="sidebar-sticky">                        
                                            
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Navigation Links</span>
                            <a class="d-flex align-items-center text-muted" href="#">
                            <span data-feather="plus-circle"></span>
                            </a>
                        </h6>

                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('dash_customer',['rdr'=>$_GET['rdr']])}}">
                                <span data-feather="users"></span>
                                {{ $display_assets == "none"? "Create ": "" }}Profile
                                </a>
                            </li>

                            

                                                        
                            <li class="nav-item" style="display:{{$display_assets}}">
                                <a class="nav-link" href="{{route('customer_message',['rdr'=>$_GET['rdr']])}}">
                                <span data-feather="message-circle"></span>
                                Messaging
                                </a>
                            </li>
                            
                            

                            <li class="nav-item" style="display:{{$display_assets}}">
                                <a class="nav-link" href="{{route('customer_invoice',['rdr'=>$_GET['rdr']])}}">
                                <span data-feather="target"></span>
                                Invoice
                                </a>
                            </li>

                            <li class="nav-item" style="display:{{$display_assets}}">
                                <a class="nav-link" href="{{route('customer_meeting',['rdr'=>$_GET['rdr']])}}">
                                <span class="refresh" data-feather="refresh-cw"></span>
                                Meetings
                                </a>
                            </li>
                            
                            <li class="nav-item" style="display:{{$display_assets}}">
                                <a class="nav-link" href="#">
                                <span data-feather="inbox"></span>
                                Notifications
                                </a>
                            </li>	
                            			
                        </ul>					
                        
                    </div>
                </nav>
            </div>
        </div>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3" id="main">
            @yield('page_main')
            <div class="row">
                    <div class="col-md-12 mt-5">
                        <p class="featurette-H font-weight-normal float-right"><span data-feather="globe"></span> Powered by: NARDEY</p>
                        <p class="featurette-H text-center font-weight-normal">
                            &copy; <?php echo date("Y") ; ?> NUTSHELL, Inc &middot;                            
                        </p>
                    </div>
                </div>
        </main>
        <div class="row">
                    
        <script src="{{URL::asset('js/jquery.js')}}"></script>
               
        <script src="{{URL::asset('js/popper.js')}}"></script>
        <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>        
                   
        <script src="{{URL::asset('js/feather.min.js')}}"></script> 

        <script>
            feather.replace()

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false;

        </script>   

        
             
    </body>
</html>