@section('navbar')
<nav class="navbar navbar-expand-lg navbar-dark fixed-top px-4 bg-dark" id="nav">	  
            <div class="container-fluid">
            <!-- LOGO -->
            <a class="navbar-brand" id="logo">
                <img src="{{URL::asset('svg/nutshell_logo.png')}}" class="">UTSHELL
                
            </a>
        
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto"></ul>
                            
                
                <ul class="navbar-nav">                    
                    <li class="nav-item">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-sm btn-outline-light BTN px-3 notify" data-toggle="modal" data-target="#exampleModal">
                            <span data-feather="bell"></span>
                        </button>
                        <div id="badge" class="badge featurette-H">02</div>                        
                    </li>                    
                    
                    <li><div id="user" class="featurette-H"><button class="btn btn-sm btn-outline-light BTN px-3 "><img src="{{URL::asset('svg/ave.png')}}" alt="" class="float-left"> Kool Aid Incorporations </button></div></li>
                    <li><div id="logout"><button class="btn btn-sm btn-outline-light BTN px-3"><span data-feather="power"></span></button></div></li>
                </ul>		
                
                
                <div class="d-sm-block d-md-none d-lg-none">                    
                    
                                        
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Saved reports</span>
                        <a class="d-flex align-items-center text-muted" href="#">
                        <span data-feather="plus-circle"></span>
                        </a>
                    </h6>

                    <ul class="nav flex-column mb-2">						
                        <li class="nav-item">
                            <a class="nav-link" href="profile.php">
                            <span data-feather="users"></span>
                            Profile
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="chat.php">
                            <span data-feather="message-circle"></span>
                            Chat with Admin
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="buy.php">
                            <span data-feather="shopping-bag"></span>
                            Buy
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="sell.php">
                            <span data-feather="shopping-cart"></span>
                            Sell
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="bids.php">
                            <span data-feather="target"></span>
                            Bids
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="ongoingtransaction.php">
                            <span class="refresh" data-feather="refresh-cw"></span>
                            On-Going Transactions
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="history.php">
                            <span data-feather="inbox"></span>
                            Transaction History
                            </a>
                        </li>
                        												
                    </ul>	
                </div>
                
            </div>     
            </div>   
        </nav>
@show