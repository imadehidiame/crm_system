<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NUTSHELL</title>
        <meta name="description" content="">
        <meta name="X-CSRF-TOKEN" content="{{csrf_token()}}">
        <meta name="X-S-V" content="{{session($_GET['rdr'])['hash']}}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/glyphicon.css')}}">
        <link href="{{URL::asset('css/font-awesome.min.css')}}" rel="stylesheet">
        <style>
        @yield('page_style')
        </style>
        </head>
        
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top px-4 bg-dark" id="nav">	  
            <div class="container-fluid">
            <!-- LOGO -->
            <a class="navbar-brand mb-2" id="logo">
                <img src="{{URL::asset('svg/nutshell_logo.png')}}" class="">UTSHELL
                
            </a>
                <?php 
                $notification_number = 0;
                if(!is_int($super_notifications)):
                    for($count = 0;$count<count($super_notifications);$count++):
                        $notification_number+=1;
                    endfor;
                endif;
                ?>
                @php
                /*$notification_number = 0;
                @if(is_int(session('notifications_adm'))==false):
                    @foreach(session('notifications_adm') as $note):
                    $notification_number+=1;
                    @endforeach
                @endif */
                $displayss = $display_assets;
                $super_admin = ""; 
                //$display_adm = 'none';
                $display_adm = $display_assets;
                if($super_admin_check == true){
                    $super_admin = ucwords(str_replace('_',' ', session($_GET['rdr'])['data']->username));
                }else{
                    $super_admin = ucwords(str_replace('_',' ', session($_GET['rdr'])['data']->full_name));
                }
                /*if(session('adm_login')->super_admin_flag == "true"){
                    $display_adm = 'block';
                }*/
                /*if(session('adm_login')->super_admin_flag != "true" && session('profile_check') == false ){
                    $displayss = "none";
                    $super_admin = session('adm_login')->username;
                
                }*/
                //else if(session('adm_login')->super_admin_flag == true){
                    //$super_admin = ucwords(str_replace('_',' ', session('adm_login')->username));
                //}
                if($profile_check == true){
                    //$super_admin = session('profile_adm')->full_name;
                }
                @endphp

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" {{$displayss == "none"?"disabled":""}}>
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto"></ul>
                            
                

                <ul class="navbar-nav">                    
                    <li class="nav-item">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-sm btn-outline-light BTN px-3 notify" data-toggle="modal" data-target="#exampleModal"{{ $displayss === "none"? " disabled":"" }}>
                            <span data-feather="bell"></span>
                        </button>
                        <div id="badge" class="badge featurette-H">{{$notification_number}}</div>                        
                    </li>                    
                    
                    <li><div id="user" class="featurette-H"><button class="btn btn-sm btn-outline-light BTN px-3 "><img src="{{URL::asset('svg/ave.png')}}" alt="" class="float-left"> {{$super_admin}} </button></div></li>
                    <li><div id="logout"><a href="{{route('log',['rdr'=>$_GET['rdr']])}}"><button class="btn btn-sm btn-outline-light BTN px-3"><span data-feather="power"></span></button></a></div></li>
                </ul>		
                
                
                <div class="d-sm-block d-md-none d-lg-none">                    
                    
                                        
                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Saved reports</span>
                        <a class="d-flex align-items-center text-muted" href="#">
                        <span data-feather="plus-circle"></span>
                        </a>
                    </h6>

                    <ul class="nav flex-column mb-2">						
                        <li class="nav-item{{!empty($prof_nav_link)?$prof_nav_link:''}}">
                            <a class="nav-link{{!empty($prof_nav_link)?$prof_nav_link:''}}" href="{{route('prof',['rdr'=>$_GET['rdr']])}}">
                            <span data-feather="users"></span>
                            {{ $displayss == "none" ? "Create " : ""}}Profile
                            </a>
                        </li>
                        
                        
                        
                        <li class="nav-item" style="display:{{$displayss}}">
                            <a class="nav-link" href="" name="route" data-url="/dashboard/customer_managers?rdr={{$_GET['rdr']}}">
                            <span data-feather="users"></span>
                            Customer Managers
                            </a>
                        </li>
                        
                        <li class="nav-item" style="display:{{$display_admins}}">
                            <a class="nav-link" href="{{route('adm',['rdr'=>$_GET['rdr']])}}">
                            <span data-feather="users"></span>
                            Administrators
                            </a>
                        </li>


                        <li class="nav-item" style="display:{{$displayss}}">
                            <a class="nav-link" href="{{route('adm_message',['rdr'=>$_GET['rdr']])}}">
                            <span data-feather="message-circle"></span>
                            Chat with Admin
                            </a>
                        </li>

                        <li class="nav-item" style="display:{{$displayss}}">
                            <a class="nav-link" href="{{route('adm_invoice',['rdr'=>$_GET['rdr']])}}">
                            <span data-feather="target"></span>
                            Invoice
                            </a>
                        </li>

                        <li class="nav-item" style="display:{{$displayss}}">
                            <a class="nav-link" href="{{route('adm_meeting',['rdr'=>$_GET['rdr']])}}">
                            <span class="refresh" data-feather="refresh-cw"></span>
                            Meetings
                            </a>
                        </li>
                        
                        <li class="nav-item" style="display:{{$displayss}}">
                            <a class="nav-link" href="history.php">
                            <span data-feather="inbox"></span>
                            Transaction History
                            </a>
                        </li>
                        												
                    </ul>	
                </div>
                
            </div>     
            </div>   
        </nav>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                <span class="glyphicon glyphicon-triangle-top"></span>

                <div class="modal-header">
                    <h5 class="featurette-H font-weight-normal">{{$notification_number==0?"No New ":""}} Notifications</h5>
                    <button class="btn btn-sm bg-transparent BTN mark-read" data-toggle="tooltip" data-placement="top" title="Mark All Read" style="display:{{$notification_number==0?"none;":"block;"}}"><span data-feather="check-circle"></span></button>                    
                </div>
                @if($notification_number > 0)
                @for($c = 0; $c<count($super_notifications); $c++)
                <div class="modal-body">                    
                    <div class="notificationDetials"> 
                        <div class="pic"><button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic mr-1">{{strtoupper(substr(session($_GET['rdr'])['data']->username,0,1))}} </div></button></div>
                        <div class="message">{{$super_notifications[$c]['message']}}
                        <br><br>
                        <button type="button" style="float:left;" class="btn btn-sm bg-danger BTN featurette-H del_notice" data-toggle="tooltip" data-placement="bottom" title="Delete notification"> <span data-feather="x-circle"></span></button>
                        <b style="float:right;">{{$super_notifications[$c]['time']}}</b>
                        </div>
                    </div>

                                        
                </div>
                @endfor
                @endif

                <div class="modal-footer">
                    <a href="#" class="btn btn-sm btn-outline-warning BTN px-3 featurette-H" role="button"> View {{ $notification_number==0?" Previous Notifications":" All"}}</a>
                    <button type="button" class="btn btn-sm bg-transparent BTN featurette-H" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal"> <span data-feather="x-circle"></span></button>
                </div>
                </div>
            </div>
        </div>

          <div class="row navBottom pr-4">                       
            <div class="col-md-6">
                <h6 class="featurette-H font-weight-normal"><a class="nav-link active mt-2" style="color:currentcolor;" href="{{route('dash',['rdr'=>$_GET['rdr']])}}"> <span data-feather="home"></span> Dashboard </a></h6>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4 searchDiv" style="display:none;">
                <div class="input-group input-group-sm mb-3">
                    <input type="text" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-outline-light" type="button"> <span data-feather="search"></span></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar" id="sidebar">
                    <div class="sidebar-sticky">                        
                                            
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Navigation Links</span>
                            <a class="d-flex align-items-center text-muted" href="#">
                            <span data-feather="plus-circle"></span>
                            </a>
                        </h6>

                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('prof',['rdr'=>$_GET['rdr']])}}">
                                <span data-feather="users"></span>
                                {{ $displayss == "none"? "Create ": "" }}Profile
                                </a>
                            </li>

                            <li class="nav-item" style="display:{{$displayss}}">
                                <a class="nav-link" href="" name="route" data-url="/dashboard/customer_managers?rdr={{$_GET['rdr']}}">
                                <span data-feather="users"></span>
                                Customer Managers
                                </a>
                            </li>
                            
                            <li class="nav-item" style="display:{{$display_admins}}">
                                <a class="nav-link" href="{{route('adm',['rdr'=>$_GET['rdr']])}}">
                                <span data-feather="users"></span>
                                Administrators
                                </a>
                            </li>
                            
                            <li class="nav-item" style="display:{{$displayss}}">
                                <a class="nav-link" href="{{route('adm_message',['rdr'=>$_GET['rdr']])}}">
                                <span data-feather="message-circle"></span>
                                Chat with Admin
                                </a>
                            </li>
                            
                            

                            <li class="nav-item" style="display:{{$displayss}}">
                                <a class="nav-link" href="{{route('adm_invoice',['rdr'=>$_GET['rdr']])}}">
                                <span data-feather="target"></span>
                                Invoice
                                </a>
                            </li>

                            <li class="nav-item" style="display:{{$displayss}}">
                                <a class="nav-link" href="{{route('adm_meeting',['rdr'=>$_GET['rdr']])}}">
                                <span class="refresh" data-feather="refresh-cw"></span>
                                Meetings
                                </a>
                            </li>
                            
                            <li class="nav-item" style="display:{{$displayss}}">
                                <a class="nav-link" href="#">
                                <span data-feather="inbox"></span>
                                Notifications
                                </a>
                            </li>	
                            			
                        </ul>					
                        
                    </div>
                </nav>
            </div>
        </div>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3" id="main">
            @yield('page_main')
            <div class="row">
                    <div class="col-md-12 mt-5">
                        <p class="featurette-H font-weight-normal float-right"><span data-feather="globe"></span> Powered by: NARDEY</p>
                        <p class="featurette-H text-center font-weight-normal">
                            &copy; <?php echo date("Y") ; ?> NUTSHELL, Inc &middot;                            
                        </p>
                    </div>
                </div>
        </main>
        <div class="row">
                    
        <script src="{{URL::asset('js/jquery.js')}}"></script>
               
        <script src="{{URL::asset('js/popper.js')}}"></script>
        <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>        
                   
        <script src="{{URL::asset('js/feather.min.js')}}"></script> 
        <script src="{{URL::asset('js/del_notice.js')}}"></script>

        <script>
            feather.replace()

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })

            Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false;

        </script>   

        
             
    </body>
</html>