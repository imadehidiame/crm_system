@section('container_links')
<div class="container">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar" id="sidebar">
                    <div class="sidebar-sticky">                        
                                            
                        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                            <span>Navigation Links</span>
                            <a class="d-flex align-items-center text-muted" href="#">
                            <span data-feather="plus-circle"></span>
                            </a>
                        </h6>

                        <ul class="nav flex-column mb-2">
                            <li class="nav-item">
                                <a class="nav-link" href="profile.php">
                                <span data-feather="users"></span>
                                Profile
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="chat.php">
                                <span data-feather="message-circle"></span>
                                Chat with Admin
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="buy.php">
                                <span data-feather="shopping-bag"></span>
                                Buy
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="sell.php">
                                <span data-feather="shopping-cart"></span>
                                Sell
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="bids.php">
                                <span data-feather="target"></span>
                                Bids
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="ongoingtransaction.php">
                                <span class="refresh" data-feather="refresh-cw"></span>
                                On-Going Transactions
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="history.php">
                                <span data-feather="inbox"></span>
                                Transaction History
                                </a>
                            </li>	
                            			
                        </ul>					
                        
                    </div>
                </nav>
            </div>
        </div>
@ahow