@section('navbar_links')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                <span class="glyphicon glyphicon-triangle-top"></span>

                <div class="modal-header">
                    <h5 class="featurette-H font-weight-normal"> Notifications</h5>
                    <button class="btn btn-sm bg-transparent BTN mark-read" data-toggle="tooltip" data-placement="top" title="Mark All Read" ><span data-feather="check-circle"></span></button>                    
                </div>
                <div class="modal-body">                    
                    <div class="notificationDetials">
                        <div class="pic"><button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic mr-1">AE </div></button></div>
                        <div class="message">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt nulla dicta vel corporis quod at a aperiam est excepturi impedit, This User made a bid on your Currency</div>
                    </div>

                    <div class="notificationDetials">
                        <div class="pic"><button class="btn btn-sm bg-transparent font-weight-normal p-0 BTN " type="button"><div class="float-left dpic2 mr-1">FL </div></button></div>
                        <div class="message">This User made a bid on your Currency</div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <a href="notification.php" class="btn btn-sm btn-outline-warning BTN px-3 featurette-H" role="button"> View All</a>
                    <button type="button" class="btn btn-sm bg-transparent BTN featurette-H" data-toggle="tooltip" data-placement="top" title="Close" data-dismiss="modal"> <span data-feather="x-circle"></span></button>
                </div>
                </div>
            </div>
        </div>

          <div class="row navBottom pr-4">                       
            <div class="col-md-6">
                <h6 class="featurette-H font-weight-normal"><a class="nav-link active mt-2" style="color:currentcolor;" href="loginsuccess.php"> <span data-feather="home"></span> Dashboard </a></h6>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4 searchDiv">
                <div class="input-group input-group-sm mb-3">
                    <input type="text" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-outline-light" type="button"> <span data-feather="search"></span></button>
                    </div>
                </div>
            </div>
        </div>
@endsection